1. [ ] People Ops: Once the termination has taken place (whether voluntary or involuntary), as soon as possible, create a **confidential** issue called 'Offboarding (NAME), per (DATE, please follow yyyy-mm-dd)' in in the [People Ops Employment Issue Tracker](https://gitlab.com/gitlab-com/people-group/employment/issues) with relevant lines of the master offboarding checklist.
1. [ ] People Ops: Enter the former team member's GitLab email address and handle below.

|||
| ----------------- | ---------------------- |
| GitLab E-mail     | `{email}` |
| GitLab.com Handle | `{@handle}`|

## All GitLab Team Members

1. [ ] For this offboarding, the manager is `__MANAGER_HANDLE__`, and People Ops is handled by `__PEOPLE_EXPERIENCE_HANDLE__`. Assign this issue to them (this should be done automatically, but worth checking).
1. [ ] People Ops: Add a due date to this issue exactly 30 days from the last day of their employment. We will use this as a reminder to delete their GSuite account.

## BambooHR and Okta

<summary>People Ops</summary>

1. [ ] People Ops: In a private message with the People Business Partner in charge of the termination, confirm the Termination Type, Termination Reason and if the team member is Eligible for Rehire.
1. [ ] People Ops: At the far right corner of the team member's profile, click on the gear icon, choose `Terminate Employee`. Use the date mentioned in this issue as final date of employment / contract. Enter Termination Type, Termination Reason and Eligible for Rehire per the answers received from the People Partner.
1. [ ] People Ops: If a team member is terminated before their Probationary Period has completed, delete the "Active" line dated in the future in the Employment Status section of BambooHR.
1. [ ] People Ops : Okta entitlements are driven based on BambooHR status. Therefore to remove the user in Okta, the best way to set off that workflow is to force an Import from BambooHR into Okta. To do this, log into the [Okta dashboard](https://gitlab-admin.okta.com/) and select `Admin` in the upper-right hand side of the page. Once on the Okta Admin dashboard, select `Applications`, search and select `BambooHR Admin`, go to the `Import` tab and click the `Import Now` button. This will force an import, and you should see a message that a user has been removed after processing completes.

_Note: This will also trigger any deprovisioning workflows for Applications that Okta is configured to perform Deprovisioning on, as well as disable the user's Okta account._



## Google account

<summary>People Ops</summary>

1. [ ] People Ops: Log on to the [Google Admin console](https://admin.google.com/gitlab.com/AdminHome?pli=1&fral=1#UserList:org=49bxu3w3zb11yx) and find the team member's profile.
1. [ ] People Ops: Click the `More` button (left hand side of screen) and then click `Suspend User` and click it again to confirm. This will ensure that the user can no longer access their account.
1. [ ] People Ops: Rename the team member's email from `username@gitlab.com` to `former_username@gitlab.com`
1. [ ] People Ops: Select the dropdown icon `ˇ` in the `User information` section and delete the email alias associated with the user.
1. [ ] People Ops: Following the [instructions in the handbook](https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/#g-suite), set an automatic rejection rule. This sends an auto-response to the sender notifying them that the team member is no longer with GitLab and who to contact instead.



## GitLab Accounts


<summary>Manager</summary>

1. [ ] Manager: Review all open merge requests and issues assigned to the team member and reassign them to you or another team member as per your handover agreement within the team.




<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/itops : Check if former GitLab Team Member had a gold account on GitLab, if so contact support to deactivate it.
      - You can do this by searching [dotcom-interal](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=GitLab%20Gold%20Request) for Closed `~"GitLab Gold Request"`s. You can use the `author` filter to narrow it down.
1. [ ] IT Ops @gitlab-com/business-ops/itops : Remove former GitLab Team Member's' GitLab.com account from the [gitlab-com group](https://gitlab.com/groups/gitlab-com/group_members).
   1. [ ] IT Ops @gitlab-com/business-ops/itops : Block former GitLab Team Member, remove from all company Groups and Projects, and then unblock. Remove GitLab job role if visible on the profile. Make sure to change their associated email address to their personal email address.
   1. [ ] IT Ops @gitlab-com/business-ops/itops : Remove former GitLabber's admin account, if applicable.
1. [ ] IT Ops @gitlab-com/business-ops/itops : Block former GitLab Team Member's [dev.GitLab.org account](https://dev.gitlab.org/admin/users) and remove from [gitlab group](https://dev.gitlab.org/groups/gitlab/group_members).
1. [ ] IT Ops @gitlab-com/business-ops/itops : Block former GitLab Team Member's [staging.GitLab.com account](https://staging.gitlab.com/admin/users) and remove from [gitlab group](https://staging.gitlab.com/groups/gitlab/group_members).



## Slack


<summary>People Ops</summary>

1. [ ] People Ops: [Check if the team member has created any Slack bots](https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/) before disabling account. [Link](https://gitlab.slack.com/apps/manage?utm_source=in-prod&utm_medium=inprod-apps_link-slack_menu-click) to Apps menu.
1. [ ] People Ops: Disable team member in [Slack](https://gitlab.slack.com/admin). If the team member is an Admin ping Erich or Sid to remove them.
1. [ ] People Ops: If a voluntary termination, add team member as a single channel guest to `gitlab-alumni` with the personal email found in BambooHR.




## 1Password



<summary>People Ops</summary>

1. [ ] People Ops: Log into [1Password](https://1password.com/) and click on "People" in the right-hand sidebar. Search for the team member's name. Click "More Actions" under their name and choose "Suspend" to remove access to 1Password. Take a screenshot of the user's permissions and post it as a comment in this offboarding issue.




<summary>Manager</summary>

1. [ ] Coordinate with IT Ops @gitlab-com/business-ops/itops to change any shared passwords, with the intent to be gated behind Okta when possible, in particular;
    1. [ ] Review if team member had sysadmin access passwords for GitLab.com Infrastructure (ssh, chef user/key, others). Identify if any can be moved to Okta.
    1. [ ] Review what 1Password vaults former team member had access to, and identify any shared passwords to be changed and moved to Okta.




## Calendars & Agenda



<summary>People Ops</summary>

1. [ ] People Ops: Remove team member from [Breakout Rooms spreadsheet](https://docs.google.com/spreadsheets/d/1M-6NjK8_gAqcU1BehQVkKuDa1Ua2hTGBy8TuEDXHh3g/edit#gid=0).




<summary>Manager</summary>

1. [ ] Manager: In the #team-member-updates on Slack, announce departure of team member: with the ['X is no longer with GitLab template'](https://about.gitlab.com/handbook/offboarding/#departures-are-unpleasant). Do not say anything about reason for termination for involuntary terminations.
1. [ ] Manager: Remove team member from team meeting invitations.
1. [ ] Manager: Cancel weekly 1:1 meetings with team member.




## Notifications



<summary>People Ops</summary>

1. [ ] People Ops: Notify Security of the offboarding (@jurbanc).
1. [ ] People Ops: Notify CFO (@pmachle) and Sr Stock Administrator (@tdominique)of offboarding. If this team member is on garden leave, please ensure the exact dates and process is shared with both the CFO and Sr Stock Administrator.
1. [ ] People Ops: Notify Marketing (@jjcordz) to remove from social/Twitter/Tweekdeck (GitHostIO, gitlabstatus, GitLabSupport) if applicable.




<summary>IT Ops</summary>

1. [ ] IT Ops: Reach out to former team member to identify and retrieve any company supplies/equipment. @gitlab-com/business-ops/itops See the [Offboarding page](https://about.gitlab.com/handbook/offboarding/) for further details on that process.
1. [ ] IT Ops @gitlab-com/business-ops/itops : Comment in this issue when equipment has been returned/recycled. Ping hiring manager, Security and `@llamb`.
1. [ ] IT Ops @gitlab-com/business-ops/itops : Inform Controller / Accounting if any items in former GitLab Team Member's possession will not be returning, so that they can be removed from asset tracking.


<summary>Manager</summary>

1. [ ] Manager: For VP and above terminations, announce in #e-group-confidential chat channel, prior to the #team-member-update announcement, including brief context for the termination.
1. [ ] Manager: Announce in #team-member-updates chat channel as soon as the chat and Google Accounts are blocked: 'As of today, XX is no longer with GitLab. I would like to take this opportunity to thank XX for their contributions and wish them all the best for the future. If you have questions about tasks or projects that need to be picked up, please let me know. If you have concerns, please bring them up with your manager.' Note, for VP and above positions, that manager will provide brief context for the termination.
1. [ ] Manager: if the team member previously made an announcement about leaving, copy the link to the message/mention in the Team Agenda or refer them to the actual team call announcement itself. It is very important to send this message as soon as possible so people know that they can rely on official communication channels and not have to find out through the grapevine. Delays in announcing it are not acceptable. The policy of not commenting on circumstances is in force indefinitely, even if the termination is voluntary. It is unpleasant, but it is the right thing to do. If people press for answers say you don't want to suggest that underperformance was a reason for this exit but remind them that:
1. [ ] Manager: Organize smooth hand over of any work or tasks from former team member. Offer option to send a message to everyone in the company (i.e. forwarded by the manager), for a farewell message and/or to transmit their personal email address in case people wish to stay in touch.




<summary>Accounting & Finance</summary>

1. [ ] Accounting (@llamb): Do not approve any submitted or in progress Expensify expenses until there is confirmation of return of laptop.
1. [ ] Accounting (@llamb): Cancel company American Express card if applicable.
1. [ ] Accounting (@nprecilla): Remove team member from Expensify.
1. [ ] Finance (@wwright): Remove team member from [Rolling 4 Quarter GitLab Team Members](https://docs.google.com/spreadsheets/d/12mNijMwA8hIG5h3zV5JJ0oxsVh6xzk6-si0eT7L9mvM/edit#gid=828135525) spreadsheet.



## Handbook & Team Page


<summary>Manager</summary>

1. [ ] Manager: Edit team member's [team page](https://about.gitlab.com/team) entry to display as a vacancy if applicable. If position is not to be backfilled, delete the entire entry.




<summary>People Ops</summary>

1. [ ] People Ops: Remove team member's profile picture on the [team page](https://about.gitlab.com/team) by going to `source` --> `images` --> `team` and finding the team member's photo.
**Important:** Before deleting the image, make sure that you edit the team.yml entry for the team member with the picture line as `picture: ../gitlab-logo-extra-whitespace.png`. Please note that **only** the team member's profile picture line should be removed and updated with the text above, the rest of their information in team.yml should remain. Both the team.yml edit and the profile picutre removal should be in the same MR.
1. [ ] People Ops: If the team member is a people manager at GitLab, be sure to remove their slug from their direct reports profiles on the team page as well. If a new manager is not yet assigned, please replace the manager slug with the departing GitLab team member's direct manager. (I.E. If the departing team member with the title Manager, GitLab reports to Director, GitLab then Director, GitLab should be listed as the manager for Manager, GitLab's direct reports)
1. [ ] People Ops: If applicable, remove team member's pet(s) from [team pets page](https://about.gitlab.com/team-pets). Don't forget to remove the picture(s).
_Tip: Once on the [team pets page](https://about.gitlab.com/team-pets), you can check to see if the team member has a pet listed by right clicking, selecting `View Page Source`, and searching the team member's name._
1. [ ] People Ops: Remove hard-coded reference links of the team member from our documentation and handbook (i.e. links to the team page) by following the [instructions on the handbook](https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/)
1. [ ] People Ops: Paste the links to the MRs in the comments of the offboarding issue for visibility.


## Other Systems and Tools


<summary>People Ops</summary>

1. [ ] People Ops: Okta should now deprovision Zoom as part of Okta Deactivation, please verify it was deactivated. If not, please ping the team in the #okta slack channel
1. [ ] People Ops: Delete account in [Moo](https://moo.com).
1. [ ] People Ops (Analyst Coordinator @julie.samson): Remove team member from compensation calculator
1. [ ] People Ops (Analyst Coordinator @julie.samson): Ensure there is no outstanding tuition reimbursement that would need to be refunded to GitLab.
1. [ ] People Ops (@ewegscheider): Deactivate former team member from [Greenhouse](https://app2.greenhouse.io/dashboard#).

<summary>Manager</summary>

1. [ ] Manager: Reach out to [TripActions Support](https://app.tripactions.com/app/user/search) to request future (non-personal) trips booked are canceled.


<summary>Business Systems Analyst</summary>

1. [ ] Business Systems Analyst (@lisvinueza): Review and confirm if team member is a system admin/provisioner.
1. [ ] Business Systems Analyst (@lisvinueza): If team member is a system admin/provisioner, open and complete an [Update Tech Stack Provisioner issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Update%20Tech%20Stack%20Provisioner).



## Tech Stack System Deprovisioning

#### System Admins

@tech-stack-provisioner
Review the table of applications listed below that is included in our [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0). For applications that you Own/Admin, please review and ensure the former team member does **not** have access to your system(s).

**If the user DOES have access to a system you administer access to:**
- Review and remove the former team member's access.
- Select the applicable checkbox(es) once access has been removed from the impacted system.

**If the user DOES NOT have access to a system you administer access to:**
- After confirming the former team member does not have access to the system, select the checkbox indicating access has been deprovisioned.


<summary>Alex Tkach</summary>

1. [ ] GovWin IQ
1. [ ] LicenseApp
1. [ ] Sertifi


<summary>Amber Stahn</summary>

1. [ ] Datafox
1. [ ] Salesforce
1. [ ] Clari




<summary>Ann Tiseo/Jenny Nguyen</summary>

1. [ ] ADP




<summary>Anthony Carella</summary>

1. [ ] Tenable.IO




<summary>Christie Lenneville, Taurie Davis, Sarah O'Donnell</summary>

1. [ ] Optimal Workshop




<summary>Cindy Nunez</summary>

1. [ ] Blackline
1. [ ] Tipalti




<summary>David Sakamoto</summary>

1. [ ] LucidChart




<summary>David Smith, Anthony Sandoval, Brent Caldwell</summary>

1. [ ] AWS (production)
1. [ ] AWS (support/staging)
1. [ ] AWS (gitter)
1. [ ] AWS (gov cloud)
1. [ ] AWS (gitlab.io)
1. [ ] Azure
1. [ ] Elastic Cloud
1. [ ] Fastly CDN
1. [ ] GitLab.com Prod/staging rails and db console (ssh)
1. [ ] customers. -gitlab.com (ssh)
1. [ ] forum. -gitlab.com (ssh)
1. [ ] license. -gitlab.com (ssh)
1. [ ] version. -gitlab.com (ssh)
1. [ ] ops.gitlab.net
1. [ ] Google Cloud Platform
1. [ ] Grafana (dashboards.gitlab.net)
1. [ ] Sentry (sentry.gitlab.net)
1. [ ] PackageCloud
1. [ ] Rackspace
1. [ ] Status - IO




<summary>Erich Wegscheider</summary>

1. [ ] ContactOut - Sourcing Team only
1. [ ] DocuSign
1. [ ] LinkedIn Recruiter




<summary>Jessica Mitchell/Jacie Bandur</summary>

1. [ ] Will Learning




<summary>JJ Cordz</summary>

1. [ ] Bizible
1. [ ] Cookiebot
1. [ ] DiscoverOrg
1. [ ] Disqus
1. [ ] Drift
1. [ ] Eventbrite
1. [ ] Facebook Ad Platform
1. [ ] FunnelCake
1. [ ] Google Adwords
1. [ ] Google Analytics
1. [ ] Google Tag Manager
1. [ ] Mandrill
1. [ ] Marketo
1. [ ] Mailchimp
1. [ ] Meetup
1. [ ] Moz Pro
1. [ ] Shopify
1. [ ] Sigstr
1. [ ] Tweetdeck
1. [ ] WebEx
1. [ ] YouTube
1. [ ] Chorus
1. [ ] LeanData
1. [ ] Google Search Console
1. [ ] MailGun
1. [ ] Sprout Social
1. [ ] LinkedIn Sales Navigator
1. [ ] Outreach




<summary>Liam McAndrew</summary>

1. [ ] CrowdIn - Lower the team member's permissions on [Crowdin](https://translate.gitlab.com/project/gitlab/settings#members) to translator




<summary>Lis Vinueza</summary>

1. [ ] Zendesk Light Agent




<summary>Lukas Eipert</summary>

1. [ ] JetBrains - Revoke JetBrains licenses. Go to the [user management](https://account.jetbrains.com/organization/3530772/users) and search for the team member, revoke their licenses.




<summary>Matt Benzaquen, Swetha Kashyap, Wilson Lau</summary>

1. [ ] Xactly




<summary>Nichole LaRue, JJ Cordz</summary>

1. [ ]	PathFactory




<summary>Paul Machle</summary>

1. [ ]	Carta




<summary>People Ops</summary>

1. [ ]	Hello Sign




<summary>Robert Nalen</summary>

1. [ ]	Conga Contracts
1. [ ]	ContractWorks
1. [ ]	Visual Compliance




<summary>Sarah O’Donnell</summary>

1. [ ]	Mural
1. [ ]	Qualtrics




<summary>Paul Harrison</summary>

1. [ ]	TheHive




<summary>Shaun McCann</summary>

1. [ ]	Unbabel




<summary>Taylor Murphy, Justin Stark, Kathleen Tam</summary>

1. [ ]	Periscope
1. [ ]	Snowflake
1. [ ]	Stitch
1. [ ]  Fivetran




<summary>Wilson Lau, Cristine Marquardt</summary>

1. [ ]	Avalara
1. [ ]	Stripe
1. [ ]	Zuora

---


### FOR CORE TEAM MEMBERS ONLY

<summary>IT Ops</summary>

1.  [ ] IT Ops @gitlab-com/business-ops/itops : Remove developer access for [gitlab-org](https://gitlab.com/groups/gitlab-org).
1.  [ ] IT Ops @gitlab-com/business-ops/itops : Ping the [Code Contributor Program Manager](https://about.gitlab.com/handbook/marketing/community-relations/#who-we-are) to remove the former Core Team member as a `developer` in the [Core Team group](https://gitlab.com/groups/gitlab-core-team/-/group_members) and add the person to the `alumni.yml` file.



/confidential

/label ~offboarding ~"laptop request" ~"LaptopOffboarding::To Do"
