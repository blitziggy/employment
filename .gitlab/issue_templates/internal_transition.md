Welcome to your new role! The onboarding issue is broken out into what your ("Team Member") responsibilities are, what your previous reporting manager's ("Previous Reporting Manager) responsibilities are, what your new reporting manager's ("New Reporting Manager"), and what our People Experience team's ("People Experience") responsibilities are. Just focus on "Team Member," and if you can't move forward with your onboarding tasks because People Experience, your manager, or your buddy (if applicable) haven't checked tasks they are responsible for, don't hesitate to send them a reminder!
Please assign this issue to the Team Member, Previous Reporting Manager, New Reporting Manager and the People Experience Associate handling the issue and label `internal-transition`.

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Once the People Experience Associates have been notified by the People Operations Analyst in the Promotion/Transfer Tracker spreadsheet that a transition is forthcoming, create a **confidential** issue called 'Transitioning (NAME), per (DATE, please follow yyyy-mm-dd) as (TITLE)' in in the [People Ops Employment Issue Tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues) with relevant sections of this checklist, be it Promotion, Demotion or Internal Transition.
1. [ ] People Experience: Add a due date of two weeks from the transition effective date.
1. [ ] People Experience: Previous Manager is `__MANAGER_HANDLE__`, new Manager is `__MANAGER_HANDLE__`, and People Experience is tackled by `__PEOPLE_EXPERIENCE_HANDLE__`. Assign this issue to the People Experience team member, the transitioning team member and the hiring managers.

</details>

### Promotion

<details>
<summary>Previous Reporting Manager</summary>

1. [ ] Previous Reporting Manager: Review the team member's current access and submit an [Access Change Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Access%20Change%20Request) issue. Please work with the New Reporting Manager to determine if a team member's access will still be needed in the new role or if access will need to be updated to a higher/lower access role. Please cc the new manager.
1. [ ] If they will become or will no longer be a provisioner of a tool, open an issue to [Update the Tech Stack Provisioner.](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Update%20Tech%20Stack%20Provisioner) Please cc the new manager.

</details>

<details>
<summary>New Reporting Manager</summary>

1. [ ] Manager: Review the team member's current access and submit an [Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) or choose from a role based template in the [new issue dropdown](https://gitlab.com/gitlab-com/access-requests/issues).
1. [ ] Manager: Select an onboarding buddy on the new employee's team.  Please try to select someone in a similar time zone, and someone who has been at GitLab for at least 3 months.
1. [ ] Manager: Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the onboarding issue.
1. [ ] Manager: Schedule a video call using Zoom with the new team member at the start of their first day to welcome them to the team and set expectations. 
1. [ ] Manager: Organize smooth onboarding with clear starting tasks / pathway for new team member.
1. [ ] Calendars & Agenda
   1. [ ] Manager: Invite team member to recurring team meetings.
   1. [ ] Manager: Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.
1. [ ] Manager: Set new GitLab team members' project-level permissions as-needed.
1. [ ] Manager: Add the new team member to the required groups in 1Password if you have access to the Admin Console in 1Password. If you do not have access please ping @gitlab-com/business-ops/itops with which vaults the new team member should be added to. Note: if it is necessary to add an individual to a vault (instead of to a group), make sure that the permissions are set to _not allow_ exporting items.
1. [ ] Manager: Introduce your new team member to your team at your next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.

</details>

<details>
<summary>Buddy</summary>

1. [ ] Buddy: Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/general-onboarding/onboarding-buddies/) to get familiar with your role.
1. [ ] Buddy: Schedule a zoom call for their first day to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc.
1. [ ] Buddy: Schedule at least one follow-up chat on day four or five. During that chat, offer to help them with updating the team page. Check with the new team member if they will need help setting up their SSH key and installing a Shell ready for using Git locally. You may need to set up an additional meeting for this step.

</details>

<details>
<summary>Team Member</summary>

1. [ ] New Team Member: Update team page entry.

</details>

---

### Demotion

<details>
<summary>Previous Reporting Manager</summary>

1. [ ] Previous Reporting Manager: Review the team member's current access and submit an [Access Change Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Access%20Change%20Request) issue. Please work with the New Reporting Manager to determine if a team member's access will still be needed in the new role or if access will need to be updated to a higher/lower access role. Please cc the new manager.
1. [ ] If they will become or will no longer be a provisioner of a tool, open an issue to [Update the Tech Stack Provisioner.](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Update%20Tech%20Stack%20Provisioner) Please cc the new manager.

</details>

<details>
<summary>New Reporting Manager</summary>

1. [ ] Manager: Review the team member's current access and submit an [Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) or choose from a role based template in the [new issue dropdown](https://gitlab.com/gitlab-com/access-requests/issues).
1. [ ] Manager: Select an onboarding buddy on the new employee's team.  Please try to select someone in a similar time zone, and someone who has been at GitLab for at least 3 months.
1. [ ] Manager: Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the onboarding issue.
1. [ ] Manager: Schedule a video call using Zoom with the new team member at the start of their first day to welcome them to the team and set expectations. 
1. [ ] Manager: Organize smooth onboarding with clear starting tasks / pathway for new team member.
1. [ ] Calendars & Agenda
   1. [ ] Manager: Invite team member to recurring team meetings.
   1. [ ] Manager: Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.
1. [ ] Manager: Set new GitLab team members' project-level permissions as-needed.
1. [ ] Manager: Introduce your new team member to your team at your next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.

</details>

<details>
<summary>Buddy</summary>

1. [ ] Buddy: Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/general-onboarding/onboarding-buddies/) to get familiar with your role.
1. [ ] Buddy: Schedule a zoom call for their first day to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc.
1. [ ] Buddy: Schedule at least one follow-up chat on day four or five. During that chat, offer to help them with updating the team page. Check with the new team member if they will need help setting up their SSH key and installing a Shell ready for using Git locally. You may need to set up an additional meeting for this step.

</details>

<details>
<summary>Team Member</summary>

1. [ ] New Team Member: Update team page entry.

</details>

---

### Internal Transition

<details>
<summary>Previous Reporting Manager</summary>

1. [ ] Previous Reporting Manager: Review the team member's current access and submit an [Access Change Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Access%20Change%20Request) issue. Please work with the New Reporting Manager to determine if a team member's access will still be needed in the new role or if access will need to be updated to a higher/lower access role. Please cc the new manager.
1. [ ] If they will become or will no longer be a provisioner of a tool, open an issue to [Update the Tech Stack Provisioner.](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Update%20Tech%20Stack%20Provisioner) Please cc the new manager.

</details>

<details>
<summary>New Reporting Manager</summary>

1. [ ] Manager: Review the team member's current access and submit an [Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) or choose from a role based template in the [new issue dropdown](https://gitlab.com/gitlab-com/access-requests/issues).
1. [ ] Manager: Select an onboarding buddy on the new employee's team.  Please try to select someone in a similar time zone, and someone who has been at GitLab for at least 3 months.
1. [ ] Manager: Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the onboarding issue.
1. [ ] Manager: Schedule a video call using Zoom with the new team member at the start of their first day to welcome them to the team and set expectations. 
1. [ ] Manager: Organize smooth onboarding with clear starting tasks / pathway for new team member.
1. [ ] Calendars & Agenda
   1. [ ] Manager: Invite team member to recurring team meetings.
   1. [ ] Manager: Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.
1. [ ] Manager: Set new GitLab team members' project-level permissions as-needed.
1. [ ] Manager: Add the new team member to the required groups in 1Password if you have access to the Admin Console in 1Password. If you do not have access please ping @gitlab-com/business-ops/itops with which vaults the new team member should be added to. Note: if it is necessary to add an individual to a vault (instead of to a group), make sure that the permissions are set to _not allow_ exporting items.
1. [ ] Manager: Introduce your new team member to your team at your next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.

</details>

<details>
<summary>Buddy</summary>

1. [ ] Buddy: Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/general-onboarding/onboarding-buddies/) to get familiar with your role.
1. [ ] Buddy: Schedule a zoom call for their first day to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc.
1. [ ] Buddy: Schedule at least one follow-up chat on day four or five. During that chat, offer to help them with updating the team page. Check with the new team member if they will need help setting up their SSH key and installing a Shell ready for using Git locally. You may need to set up an additional meeting for this step.

</details>

<details>
<summary>Team Member</summary>

1. [ ] New Team Member: Update team page entry.
2. [ ] Update your Slack profile to include your new role
3. [ ] Update your GitLab profile to include your new role
4. [ ] Update your Gmail signature to include your new role

</details>

---

### Role-specific tasks

#### People Managers

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Review [Git page update](https://about.gitlab.com/handbook/git-page-update/) to learn more about contributing and using GitLab.
1. [ ] New team member: Make a meaningful [update to the handbook](https://about.gitlab.com/handbook/handbook-usage/) and assign the merge request to your manager.
1. [ ] New team member: If applicable, review the [Vacancy Creation Process](https://about.gitlab.com/handbook/hiring/vacancies/), to learn how to open a new position.
1. [ ] New team member: Review the [Leadership handbook page](https://about.gitlab.com/handbook/leadership/), particularly the recommended [articles](https://about.gitlab.com/handbook/leadership/#articles) and [books](https://about.gitlab.com/handbook/leadership/#books).

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Give member `Maintainer` access on [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com).
1. [ ] Manager: Create a new [training issue](https://gitlab.com/gitlab-com/people-ops/Training/issues) using the [new manager enablement template](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/new-manager-enablement.md) and assign it to the new team member.  Provide a link to the issue in a comment below this onboarding checklist.
1. [ ] Manager:  Before the new hire's first day, update the reporting structure in BambooHR by completing the following steps:
   * Login into BambooHR and click the employees tab
   * Select the applicable employee(s)
   * If you cannot see the employees in that view, search for them in the Search bar
   * Once in the employee profile, on the upper right hand side, click the request a change drop down.
   * Select job information, and complete the updated fields, including the 'reports to' field.
   * People Ops will process the request and it will be updated shortly.
1. [ ] Manager: Before the first day, update the reporting structure on the team page.
1. [ ] Manager: Comment in the issue if the new team member needs an Interview Training Issue and/or additional permissions in Greenhouse.
1. [ ] Manager: Submit an Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=slack_googlegroup_1Passwordgroupvault) to have the team member added to the Manager Google Group.

</detail>

<details>
<summary>People Experience</summary>

1. [ ] People Experience : If applicable, create an [interviewing training issue](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/interview_training.md) in the [People Ops Training project](https://gitlab.com/gitlab-com/people-ops/Training/issues) and assign it to the new team member.
1. [ ] People Ops (@ewegscheider) : If applicable, [upgrade new team member in Greenhouse to either "Interviewer" or "Job Admin: Hiring Manager"](https://about.gitlab.com/handbook/hiring/greenhouse/#access-levels-and-permissions) for any roles they will be interviewing for or a hiring manager for. If they will be opening new vacancies in the future, they will need to have the permission "Can create new jobs and request approvals" enabled. If they are an executive, ping Recruiting in order to add them to the Greenhouse vacancy and offer approval flow for their division.

</details>

/label ~"internal-transition" 
