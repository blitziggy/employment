> In addition to general onboarding, you may have department- or role-specific tasks to complete. See below main section for details.

### Before Starting at GitLab

1. [ ] People Ops: Once the offer letter or contract is signed, create a **confidential** issue called 'Onboarding (NAME), starting (DATE), as Consultant' in the [People Ops Employment issue tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues) with relevant lines of the master checklist, and choose the `onboarding` label. /cc @brittanyr, @tknudsen
1. [ ] GitLab contact is (FILL IN WITH @ HANDLE),  People Ops is tackled by (FILL IN WITH @ HANDLE). Assign this issue to the People Ops team member and the GitLab contact.
1. [ ] People Ops: If the consultant is outside of the US, confirm that the new team member is not on any known [Denied Party List](https://www.export.gov/csl-search) add their name to the "Denied Parties List" Google Doc.
1. [ ] Google account
   1. [ ] People Ops: Log on to the [Google Admin console](https://admin.google.com/gitlab.com/AdminHome?pli=1&fral=1#UserList:org=49bxu3w3zb11yx) and verify `first initial last name[at]gitlab.com` is not conflicting with a current account. Click the + sign to create an account for the consultant, and email the instructions to consultant's private email address.
   1. [ ] People Ops: Add new Google user to the ['Disable 2FA' group](https://admin.google.com/gitlab.com/AdminHome?groupId=new-users@gitlab.com&chromeless=1#OGX:Group) under the Groups tab to disable 2FA for them.
   1. [ ] People Ops: Add new user to proper regional group through the admin console.
   1. [ ] People Ops: Inform the GitLab contact that the Google account has been created by commenting in this issue with the new email address.
1. [ ] People Ops (@chloe): Invite the consultant's GitLab email address to the [gitlab-com group](https://gitlab.com/groups/gitlab-com/group_members) as a `Developer`.
1. [ ] People Ops (@chloe): Invite the consultant's GitLab email address to the [gitlab-org group](https://gitlab.com/groups/gitlab-org/group_members) as a `Developer`.
1. [ ] GitLab contact: In the ["Email, Slack, and GitLab Groups and Aliases" doc](https://docs.google.com/document/d/1rrVgD2QVixuAf07roYws7Z8rUF2gJKrrmTE4Z3vtENo/edit#), determine which email aliases the consultant should be added to, and add a comment to the onboarding issue with the aliases and groups they need access to.
1. [ ] GitLab contact: Determine if consultant will need access to the `dev` server, which allows for access to version.gitlab.com and license.gitlab.com. If so, make [new dev.GitLab.org account](https://dev.gitlab.org/admin/users/new) and invite to the [gitlab group](https://dev.gitlab.org/groups/gitlab/group_members) as a developer. The dev server is only viewable to GitLab team members, and is used by the Development team (amongst others). Per our [communication guidelines](https://about.gitlab.com/handbook/communication/#external-communication) point 9, everything that can be public should be out in the open. To grant access, create an issue in the [Access-Requests project](https://gitlab.com/gitlab-com/access-requests/issues) using the `New Access Request` issue template for this and other related system access requests.
   1. [ ] GitLab contact: Let the new team member know their handle for dev.gitlab.org by mentioning it here.
1. [ ] GitLab contact: Set new GitLab Team Member's project-level permissions as-needed.
1. [ ] People Ops: Add to [Slack](https://gitlab.slack.com/admin).
1. [ ] People Ops Information
   1. [ ] People Ops: File signed contract with PIAA or NDA+IP agreements in BambooHR profile under "Documents"-->"Signed Contract/Offer Letter". Set the uploaded documents to "shared" so that the team member can also view them. For GitLab Inc. contractor agreements, also file the W9. If the contract has any exceptions to the IP agreement, check the applicable box under the Jobs tab.
1. [ ] People Ops: Add to phishing test platform. To do so, log into [Knowbe4](https://training.knowbe4.com/), click on "Users" at the top, "Import Users", add the new team member's GitLab email, and click "Import". Go to Users page, click the dropdown arrow next to their email, click edit, add their first and last names, and click save.
1. [ ] People Ops: Create a row for the new user on the [1Password spreadsheet](https://docs.google.com/spreadsheets/d/1dSlIRbD1b8XisIBbANDK5NGe55LvVSTsWM7aHcWCOVU/edit#gid=0).
1. [ ] People Ops: [Add blank entry to team page](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#blank-entry) (only the start date and position, use "logo-extra-whitespace.png" for the picture) so the team can see when new people are joining.
1. [ ] People Ops: Send brief welcome email to their personal address that directs the consultant to their GitLab email and their onboarding issue, using the [onboarding email template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding_email.md). Copy their GitLab email address and People Ops. When clicking send you will receive prompt; choose "send without sharing", as their GitLab accounts already have the appropriate access.
1. [ ] Consultant: Assign the onboarding issue to yourself.
1. [ ] Consultant: Google does not support 2FA in some countries ([e.g. Nigeria](https://productforums.google.com/forum/#!topic/gmail/3_wcd8tAqdc) ); if that is the case for you, reach out to People Ops to get the initial authentication code sent to a phone in  supported country, after which 2FA will work as normal.
1. [ ] Consultant: 1Password. We recommend that you set up your 1Password account early in the onboarding process since you will need it to hold all of the new passwords that you will create for your accounts. This is an awesome tool that GitLab uses to store all passwords for each department. You will also have your personal vault where you can store all the passwords that you are creating for GitLab.
   1. [ ] Consultant: Register on 1Password by clicking on the link in the ["Link to 1Password Sign-Up"](https://docs.google.com/document/d/15BdqpNhSi_5bc2b9KXo_w5f0gC-3gmuw-aIgzRcbnbk/edit) Google Doc and then make a comment below in this issue to confirm your account. You can find this (and other) shared Google Docs by using [`Search for files shared with gitlab.com`](https://support.google.com/a/answer/3187967?hl=en) in Google Drive.
   1. [ ] Consultant: Install the 1Password app on your computer, and link it to your team account, as described on the [security best practices page](https://about.gitlab.com/handbook/security/). Please let People Ops or your manager know if you need any assistance.
   1. [ ] Consultant: Change your password on GitLab.com to use 1Password if you had an account before joining the company.
   1. [ ] Consultant: If you created any accounts while onboarding before being added to 1Password, reset your passwords for them to use 1Password, including your GitLab email.
   1. [ ] Consultant: Set up [secure passwords per the handbook](https://about.gitlab.com/handbook/security/).
   1. [ ] Consultant: Once you are registered on 1Password, post a comment below to ask the manager to add you to the 1Password groups required for your role.
   1. [ ] Consultant: Review with your GitLab contact that you are using 1Password in accordance with [Security Best Practices](https://about.gitlab.com/handbook/security/).
   1. [ ] GitLab contact: Add the new team member to the required groups in 1Password if you have access to the Admin Console in 1Password. If you do not have access please ping email peopleops with which vaults the consultant should be added to. Note: if it is necessary to add an individual to a vault (instead of to a group), make sure that the permissions are set to _not allow_ exporting items.


#### Getting Setup

1. [ ] Slack
   1. [ ] Consultant: Pick your [Slack username](https://gitlab.slack.com/account/settings#username) to be the same as your GitLab email handle, for consistency and ease of use.
   1. [ ] Consultant: Fill in your [Slack profile](https://gitlab.slack.com/account/profile)
      [ ] Consultant:  consider changing your ["display name"](https://get.slack.help/hc/en-us/articles/216360827-Change-your-display-name) if you prefer to be addressed by a nickname
1. [ ] Consultant: Set up [Zoom account](https://about.gitlab.com/handbook/tools-and-tips/#zoom) **with your GitLab email address**, and fill in your [profile](https://zoom.us/profile): Name and Profile Picture.
   1. [ ] Consultant: Set up the Zoom Scheduler plugin for [Chrome](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle/related?hl=en) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/zoom-new-scheduler/) - this will allow you to automatically add Zoom links to Google Calendar events.
1. [ ] People Ops: Remove user from the [Google 'New Users' group](https://admin.google.com/gitlab.com/AdminHome?groupId=new-users@gitlab.com&chromeless=1#OGX:Group). This causes 2FA enforcement for the user.

   

1. [ ] Consultant: Please read the [anti-harassment policy](https://about.gitlab.com/handbook/anti-harassment/) and in particular any country specific requirements that relate to your location.
1. [ ] Consultant: Don't forget to comply with the contract you signed, and make sure you understand [Intellectual Property](https://about.gitlab.com/handbook/people-operations/code-of-conduct/#intellectual-property-and-protecting-ip).
1. [ ] Consultant: Set-up and familiarize yourself with our apps: [Gmail](https://mail.google.com/), [Google Calendar](https://www.google.com/calendar/), [Slack](https://gitlab.slack.com/messages/general/) and [Google Drive](https://www.google.com/drive/) where you can [download](https://tools.google.com/dlpage/drive/index.html?hl=en) to work offline. Be aware your Google Calendar (tied to your GitLab Google account) is internally viewable by default. If you have private meetings often, you might want to [change this](https://support.google.com/calendar/answer/34580?co=GENIE.Platform%3DDesktop&hl=en) in your calendar settings.
1. [ ] Consultant: Review [GitLab's internal acceptable use policy](https://about.gitlab.com/handbook/people-operations/acceptable-use-policy/) 

Many of tools used by GitLab including tips on how to use them can be found in the [Tools and Tips section of the handbook](https://about.gitlab.com/handbook/tools-and-tips/).

### Day 2: Security

1. [ ] Consultant: Read the [security best practices](https://about.gitlab.com/handbook/security/), and please ask questions to make sure it is all clear to you.
1. [ ] GitLab asks you to enable 2FA (2-Step Verification, also known as two-factor authentication), because it adds an extra layer of security to your account. You sign in with something you know (your password) and something you have (a code sent to your phone). If you have problems with 2FA just let People Ops know! We can disable it for you to then set it up again. If the problem persists, then we can direct you to one of our super admins.
   1. [ ] Consultant: Enable 2FA on your GitLab email account (this should have been an option to do when you first received the invitation to the account).
   1. [ ] Consultant: If you authenticated your GitLab.com account with Google, GitHub, etc. you should either disconnect them or make sure they use two-factor authentication.
   1. [ ] Consultant: Enable [two-factor authentication](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) on your GitLab.com account.
1. [ ] Consultant: Make sure your notifications for GitLab.com aren't sent to a personal email. Send them to your `@gitlab.com` email. GitLab.com is viewable to anyone with an account, and we ask you to set up your GitLab.com account with your GitLab email for additional security.



#### For Engineering, such as Developers, Build, Infrastructure, etc. Only

1. [ ] Consultant: Add the [Engineering Rhythm calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_aqtv5ahnpp8kmm4q7briauc2to%40group.calendar.google.com) to your Google Calendar.
1. [ ] Consultant: (For Engineering) Read the [Developer Onboarding](https://about.gitlab.com/handbook/developer-onboarding/)
1. [ ] Consultant: Purchase and expense for yourself a [YubiKey 4 Series](https://www.yubico.com/product/yubikey-4-series/) or greater.
1. [ ] Consultant: If you need access to Staging, if not already granted, please follow the instructions on the [Staging section](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#staging) in the engineering handbook.
1. [ ] GitLab contact: Provide access to PagerDuty (if applicable).
1. [ ] GitLab contact: (For Distribution) Provide access to the [gitlab-pivotal group](https://gitlab.com/gitlab-pivotal)
1. [ ] GitLab contact: (For Distribution) Provide access to the [gitlab-pivotal GCE project](https://gitlab.com/gitlab-pivotal/gitlab-ee-bosh-release/blob/master/docs/getting-started/README.md#prerequisites)
1. [ ] GitLab contact: (For Distribution) Provide access to the [dev-server](https://gitlab.com/gitlab-com/infrastructure/issues/1592)

#### For Production And Database Engineering Only

1. [ ] GitLab contact: Create a new [Production Engineering onboarding checklist](https://gitlab.com/gitlab-com/gl-infra/infrastructure/blob/master/.gitlab/issue_templates/onboarding_template.md).
1. [ ] People Ops: [Upgrade consultant's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] Consultant: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.

#### For Database Engineering Only

1. [ ] Consultant: Make sure you can log in to the staging and production databases using `sudo gitlab-psql gitlabhq_production`.
1. [ ] Consutlant: Join the `#database` channel on Slack.

#### For Support Only

1. [ ] GitLab contact: Open a new [support onboarding boot camp issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues) using the appropriate issue template for [Support Engineer](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Support%20Engineering%20Bootcamp.md) or [Support Agent](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Service%20Support%20Agent%20Bootcamp.md), and provide the link in a comment below this onboarding checklist.
1. [ ] Zendesk:
   1. [ ] GitLab contact: [Add new team member](https://support.zendesk.com/hc/en-us/articles/203661986-Adding-end-users-agents-and-administrators#topic_h43_2k2_yg) as an agent in [GitLab ZenDesk](https://gitlab.zendesk.com); you may need to [purchase a new license](https://about.gitlab.com/handbook/support/workflows/shared/zendesk/zendesk_admin.html#adding--removing-agents-in-zendesk)
   1. [ ] GitLab contact: Add agent to required [support groups](https://support.zendesk.com/hc/en-us/articles/203661766-About-organizations-and-groups) in [GitLab ZenDesk](https://gitlab.zendesk.com).
1. [ ] People Ops: [Upgrade consultant's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] Consultant: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.

#### For Community Advocates Only

1. [ ] Manager: Open a new community advocacy onboarding bootcamp [issue](https://gitlab.com/gitlab-com/marketing/general/issues) using the community advocacy [onboarding checklist](https://about.gitlab.com/handbook/marketing/developer-relations/community-advocacy/onboarding/checklist/), and provide the link in a comment below this onboarding checklist.
1. [ ] Shopify
   1. [ ] Manager: [Add the new member](https://help.shopify.com/manual/your-account/staff-accounts/create-staff-accounts) as a staff user on our Shopify instance
   1. [ ] New team member: Update your [Shopify account](https://help.shopify.com/manual/your-account/staff-accounts/edit-staff-accounts#update-staff-account-details)
1. [ ] Printfection
   1. [ ] Manager: [Add the new member](https://help.printfection.com/hc/en-us/articles/114094526173-Managing-Users-in-Printfection) as a user on our Printfection account
   1. [ ] New team member: Update your Printfection account
1. [ ] Zendesk
   1. [ ] Manager: [Add new team member](https://support.zendesk.com/hc/en-us/articles/203661986-Adding-end-users-agents-and-administrators#topic_h43_2k2_yg) as an agent in [GitLab Community ZenDesk](https://gitlab-community.zendesk.com); you may need to [purchase a new license](https://about.gitlab.com/handbook/support/workflows/shared/zendesk/zendesk_admin.html#adding--removing-agents-in-zendeskk).
   1. [ ] Manager: Add agent to required [support groups](https://support.zendesk.com/hc/en-us/articles/203661766-About-organizations-and-groups) in [GitLab Community ZenDesk](https://gitlab-community.zendesk.com).
   1. [ ] New team member: Update your [Zendesk account](https://support.zendesk.com/hc/en-us/articles/203690996-Viewing-your-user-profile-in-Zendesk-Support)
1. [ ] New team member: Create a [Hacker News](https://news.ycombinator.com/news) account if you don't have one already, make sure to specify in your user bio that you're a Community Advocate at GitLab, Hacker News requires that we be transparent about any conflicts of interest.
1. [ ] TweetDeck
   1. [ ] Manager: Add team member to [TweetDeck](https://tweetdeck.twitter.com/).
   1. [ ] New team member: Enable "Confirmation Step" for all GitLab accounts to prevent accidental tweeting.
1. [ ] Disqus
   1. [ ] New team member: Create a Disqus account, connect it to your `@gitlab.com` Google account and comment your username in the issue.
   1. [ ] Manager: Give the user the ability to moderate Disqus comments on the blog.
1. [ ] Community Forum
   1. [ ] New team member: Create new account for the [GitLab community forum](https://forum.gitlab.com/) using the sign in with GitLab option and comment your username in the issue.
   1. [ ] Manager: Add new team member to "moderators" group on the [GitLab community forum](https://forum.gitlab.com/).
1. [ ] New team member: Create an account on [Stack Overflow](http://stackoverflow.com/) and comment your username in the issue.

#### For UX Designers

1. [ ] New team member: Join the [#ux](https://gitlab.slack.com/messages/ux/) channel on Slack.
1. [ ] New team member: Familiarize yourself with the [UX Designer Onboarding](https://about.gitlab.com/handbook/engineering/ux/uxdesigner-onboarding/) page and [relevant pages](https://about.gitlab.com/handbook/engineering/ux/uxdesigner-onboarding/#relevant-links) linked from there.
1. [ ] People Ops: Add new team member to the `@uxers` User Group on Slack.
1. [ ] Manager: Give new team member `Developer` access to the [GitLab Design](https://gitlab.com/gitlab-org/gitlab-design) project on GitLab.com.
1. [ ] Manager: Give new team member `Developer` access to the [UX Research](https://gitlab.com/gitlab-org/ux-research) project on GitLab.com.
1. [ ] Manager: Give new team member `Maintainer` access to the [GitLab UX](https://gitlab.com/gitlab-com/gitlab-ux) group on GitLab.com.
1. [ ] Manager: Share the UX calendar with new team member
1. [ ] Manager: Add new team member to the [GitLab Dribbble team](https://dribbble.com/gitlab).
1. [ ] Manager: Add new team member to the group Sketch License and provide them with the key.
1. [ ] Manager: Add new team member to UX weekly calls and UX/FE monthly calls.
1. [ ] People Ops: Add new member to Engineering Google group and update [Google doc](https://docs.google.com/document/d/1rrVgD2QVixuAf07roYws7Z8rUF2gJKrrmTE4Z3vtENo/edit#)

#### For Frontend Engineers Only

1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] New team member: Familiarize yourself with the [frontend development guidelines](https://docs.gitlab.com/ce/development/fe_guide/).
1. [ ] New team member: Familiarize yourself with the [Vue documentation](https://vuejs.org/v2/guide/).
1. [ ] New team member: Familiarize yourself with [GitLab's Design System](http://design.gitlab.com).
1. [ ] Your specific Frontend Team Buddy is: XXX
1. [ ] Manager: Grant new team member edit access to the frontend calendar
1. [ ] Manager: Add new team member to [@gl-frontend](https://gitlab.com/groups/gl-frontend/-/group_members) group
1. [ ] FE Buddy: Schedule an introduction [coffee break call](https://about.gitlab.com/company/culture/all-remote/tips/#coffee-chats) in advance for the first 2 days.
1. [ ] FE Buddy: Do an intro session about the GDK and specifically about our FE Tools (for example best way to run Tests, etc.).
1. [ ] FE Buddy: Show one of your current deliverables and explain based on that: What you have actually done, How did you find the actual relevant files, reproducing steps, How our process works, How to submit an MR, When to involve reviewers etc.
1. [ ] FE Buddy: Search for 3 easy issues to start with (if none found, create one based on existing code) which is a simple refactoring, removal of ESLint exceptions, replace Modals or Icons.
1. [ ] New Team Member: Ensure that you've added yourself as a [reviewer for gitlab-foss and gitlab](https://about.gitlab.com/handbook/engineering/projects/#gitlab-foss). You can do this by adding the appropriate entries to the `data/team.yml` with which you added yourself to the team page.
1. [ ] New Team Member: Start off with these issues and check in with your buddy every second day on your progress, what you have done and your final MR.
1. [ ] New Team Member: Put on the agenda in your first week a small FE intro. What have you done before specifically? Favorite Frameworks? Your Code Editor? etc.
1. [ ] New Team Member: Check in with your respective manager on which areas you will start working so that you can take a look at the functionality and read the documentation about it.
1. [ ] People Ops: Add new member to Engineering Google group and update [Google doc](https://docs.google.com/document/d/1rrVgD2QVixuAf07roYws7Z8rUF2gJKrrmTE4Z3vtENo/edit#)

#### For Product Management Only

1. [ ] People Ops: Add new team member to the `@product-team` User Group on Slack.
1. [ ] Manager: Update [mapping of product categories to PMs](https://about.gitlab.com/handbook/product/categories/).
1. [ ] New team member: Review the [important dates PMs should keep in mind](https://about.gitlab.com/handbook/product/#important-dates-pms-should-keep-in-mind) and the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline).
1. [ ] New team member: Schedule introductory calls with your Core Team as listed in the [Product Categories page] (https://about.gitlab.com/handbook/product/categories/).
    1. Scheduled call with _____ EM
    1. Scheduled call with _____ UX
    1. Scheduled call with _____ PMM
1. [ ] New team member: Familiarize yourself with the [product handbook](https://about.gitlab.com/handbook/product) and relevant pages linked from there.
1. [ ] New team member: Familiarize yourself with [GitLab's Design System](http://design.gitlab.com).
1. [ ] New team member: If you need access to Staging, if not already granted, please follow the instructions on the [Staging section](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#staging) in the engineering handbook.
1. [ ] Make an update to the [Product Management section of the onboarding issue template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-product-management-only)

#### For Marketing Design Only

1. [ ] New team member: Join the following slack channels:
    1. Marketing - This is the general marketing channel. Don't know where to ask a question? Start here.
    1. Marketing-Design - All things marketing design are discussed here. Have questions? Need feedback? Stuck on an idea?
1. [ ] Read the Marketing Handbook
1. [ ] Manager: Provide [SketchApp](http://www.sketchapp.com/) license by adding a seat to the group license. This is administered by the UX department.
1. [ ] People Ops: Provide access to [Adobe Creative Cloud](https://www.adobe.com/creativecloud.html) using the shared credential in the Secretarial vault. If a UX Designer feels that they need CC Photoshop and/or CC Illustrator they can request it.
1. [ ] New Team Member: Review the Marketing [OKRs](https://about.gitlab.com/handbook/marketing/#okrs).

#### For Security Only

1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] People Ops: Add new team member to `@sec-team` Slack group.
1. [ ] Manager: Add new team member to `security-gl-team@gitlab.com`.
1. [ ] Manager: Invite new team member to the private security team Slack channels.
1. [ ] Manager: Add new team member to the [gl-security](https://gitlab.com/gl-security) GitLab group.
1. [ ] People Ops: Add new member to Engineering Google group and update [Google doc](https://docs.google.com/document/d/1rrVgD2QVixuAf07roYws7Z8rUF2gJKrrmTE4Z3vtENo/edit#)

#### For Finance Only

1. [ ] Finance: Add to Comerica (as user or viewer only if in Finance).
1. [ ] Finance: Add *Employee Record* in NetSuite with department classification.
1. [ ] Finance: Add to Carta with department classification.

#### For People Ops Only

1. [ ] People Ops: Add team member to BambooHR as an admin.
1. [ ] Manager: Add team member to TriNet and HR Savvy as an admin (if applicable).
1. [ ] People Ops: Add team member to Greenhouse as "Job Admin: People Ops".
1. [ ] People Ops: Invite team member to People Ops Confidential channel in Slack.

#### For Recruiting Only

1. [ ] New team member: Read each of the subpages of the [hiring section in the handbook](https://about.gitlab.com/handbook/hiring/).
1. [ ] New team member: Read through the [recruiting onboarding guide](https://gitlab.com/gitlab-com/people-ops/recruiting/tree/master/recruiting-onboarding).
1. [ ] People Ops: [Upgrade new team member's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] People Ops: Add team member to `@hiringteam` group on Slack.
1. [ ] People Ops: Add team member to `hiring@gitlab.com` and `jobs@gitlab.com` email groups.
1. [ ] Manager: Add team member to Greenhouse as `Job Admin: Recruiting`.
1. [ ] Manager: Add team member to the [`gl-hiring`](https://gitlab.com/groups/gl-hiring/-/group_members) GitLab group.
1. [ ] Manager: Add team member to the shared Google Calendar `Interview Calendar` and give read/write acces.
1. [ ] Manager: Add team member to the weekly Recruiting and People Ops team meetings.

#### For Core Team Members Only

1. [ ] People Ops: Have member sign [NDA](https://about.gitlab.com/handbook/contracts/#core-team-nda) before any of the following steps are taken.
2. [ ] People Ops: Ping the [Code Contributor Program Manager](https://about.gitlab.com/handbook/marketing/community-relations/#who-we-are) to add member as a `developer` in the [Core Team group](https://gitlab.com/groups/gitlab-core-team/-/group_members)
1. [ ] People Ops: Give member access to all Slack channels.
1. [ ] People Ops: Give member developer access to [gitlab-org](https://gitlab.com/groups/gitlab-org/group_members).
1. [ ] Core Team Member: Add yourself to the team page by following the instructions in [the handbook](https://about.gitlab.com/handbook/git-page-update/#11-add-yourself-to-the-team-page), and make sure to include your nickname when you do so if you prefer to be called anything other than your first name. Most of the information including your role & department should have been filled out already, but please add your `story`.
1. [ ] Core Team Member: To support you in your contributions, you are able to [request a license for a JetBrains product](https://about.gitlab.com/handbook/tools-and-tips/#jetbrains).

#### For Technical Writers Only

1. [ ] Manager: Request that new team member be added to docs@gitlab.com email group and `@docsteam` user group on Slack. (They will then automatically be added to `#docs`, `#docs-team`, and `#docs-comments`.)
1. [ ] Manager: Add the new team member to the [gl-docsteam group](https://gitlab.com/groups/gl-docsteam/-/group_members) on gitlab.com.
1. [ ] New team member: Read through the following to become familiar with how technical writing is done at GitLab:
   1. [ ] [Technical Writing Handbook](https://about.gitlab.com/handbook/product/technical-writing/).
   1. [ ] [GitLab Documentation guidelines](https://docs.gitlab.com/ce/development/documentation/index.html). In particular, the [processes](https://docs.gitlab.com/ce/development/documentation/workflow.html).
   1. [ ] Other GitLab documentation-related guidelines, including:
      - [Documentation style guidelines](https://docs.gitlab.com/ce/development/documentation/styleguide.html).
      - Markdown guides, for:
        - [GitLab documentation](https://docs.gitlab.com/ce/user/markdown.html).
        - [Company website](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/).
      - [Company writing style guidelines](https://about.gitlab.com/handbook/communication/#writing-style-guidelines).
      - Because our documentation is part of the codebase and we sometimes need to port it: [Cherry-picking from CE to EE](https://docs.gitlab.com/ce/development/documentation/index.html#cherry-picking-from-ce-to-ee).
   1. [ ] New team member: [GitLab Release Posts process](https://about.gitlab.com/handbook/marketing/blog/release-posts/).
1. [ ] New team member: Familiarize yourself with the following documentation-related project structures:
   - [`gitlab-docs`](https://gitlab.com/gitlab-com/gitlab-docs)
   - [`doc`](https://gitlab.com/gitlab-org/gitlab-foss/tree/master/doc) directory in [`gitlab-foss`]
   - [`doc`](https://gitlab.com/gitlab-org/gitlab/tree/master/doc) directory in [`gitlab`]
   - [`docs`](https://gitlab.com/gitlab-org/gitlab-runner/tree/master/docs) directory in [`gitlab-runner`]
   - [`doc`](https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master/doc) directory in [`omnibus-gitlab`]

<!-- begin marketing section -->

#### For Marketing (non-SDR/BDR) Only

### Department Tasks

1. [ ] Marketing (@evhoffmann): Add to Tweetdeck.
1. [ ] Marketing Ops (@jjcordz): Add as 'Marketing User' in Marketo.
2. [ ] Sales Operations (@francisaquino): Add as 'Listener' in Chorus, if needed.

### Week 1: General Onboarding

1. [ ] Complete the first half of this onboarding issue as part of your general onboarding.
1. [ ] Read the [Cluetrain Manifesto](http://www.cluetrain.com/) and share with your manager how you believe it applies to marketing and communication.

### Week 2: Marketing Onboarding

#### Day 1

##### GitLab Buyer

**Goal:** Understand the people that buy and use GitLab

1. [ ] New team member: Watch [Understanding the GitLab Buyer: Personas and Pain Points](https://youtu.be/-UITZi0mXeU)
1. [ ] New team member: Watch [Persona: Chief Architect](https://www.youtube.com/watch?v=qyELotxsQzY)
1. [ ] New team member: Watch [Persona: Head of IT](https://www.youtube.com/watch?v=LUh5eevH3F4)
1. [ ] New team member: Watch [Persona: DevOps Director/Lead](https://www.youtube.com/watch?v=5_D4brnjwTg)
1. [ ] New team member: Discuss the three major Personas and their Pain Points with your manager: who are they, what are their common titles, what are their main responsibilities, what are their top 3 pain points
1. [ ] New team member: Submit video: Describe in your own words the components, attributes and pain points of Chief Architect
1. [ ] New team member: Submit video: Describe in your own words the components, attributes and pain points of Head of IT
1. [ ] New team member: Submit video: Describe in your own words the components, attributes and pain points of DevOps Director

**Assessment**
1. [ ] New team member: Take Quiz: [The GitLab Buyer](https://goo.gl/forms/mGEjlhAIqU56lXKm1)
1. [ ] Manager: Sign-off of completed section

#### Day 2

##### GitLab Industry

**Goal:** Understand the industry in which GitLab competes

1. [ ] New team member: Watch [Understand the Industry where GitLab Competes](https://www.youtube.com/watch?v=qQ0CL3J08lI)
1. [ ] New team member: Watch [What is DevOps?](https://www.youtube.com/watch?v=_I94-tJlovg)
1. [ ] New team member: Review [The State of DevOps 2017](https://www.bmc.com/content/dam/bmc/migration/pdf/Interop+ITX+State+of+DevOps_Final2-002-V1.pdf)
1. [ ] New team member: Review [Understanding the Software Development Lifecycle](https://about.gitlab.com/sdlc/)
1. [ ] New team member: Bookmark the [Glossary of Terms](https://docs.gitlab.com/ce/university/glossary/)
1. [ ] New team member: Watch [Understanding GitLab's Competitors in the Industry](https://www.youtube.com/watch?v=GDqGO5cv1Mk)
1. [ ] New team member: Review [Comparison](https://about.gitlab.com/comparison/)
1. [ ] New team member: Review [Direction](https://about.gitlab.com/direction/#single-application)
1. [ ] New team member: Review and subscribe to these blogs
   1. [ ] [Hacker News](https://news.ycombinator.com/)
   1. [ ] [Martin Fowler](https://martinfowler.com/)
   1. [ ] [New Stack](https://thenewstack.io/)
1. [ ] New team member: Review and subscribe to the [GitLab Blog](https://about.gitlab.com/blog/)
1. [ ] New team member: Review 3 keynotes from [this conference](https://www.youtube.com/playlist?list=PLvk9Yh_MWYuyq_rn0H-AP3ORoQ1QM0s1L)
1. [ ] New team member: Watch the first 30 minutes of the [AWS conference keynote](https://www.youtube.com/watch?v=1IxDLeFQKPk)

**Assessment**
1. [ ] New team member: Take Quiz: [The Industry where GitLab Competes](https://goo.gl/forms/u6XZNcMDBBNu5f4b2)
1. [ ] Manager: Sign-off of the completed section

#### Days 3 & 4

##### The GitLab Solution

**Goal:** Understand how the GitLab solution solves the pain points for the GitLab buyer

1. [ ] New team member: Watch [The GitLab Value Proposition 2018](https://www.youtube.com/watch?v=6Dsdd1LSf40)
1. [ ] New team member: Watch [Where GitLab Fits in Within Current Tool Stack](https://www.youtube.com/watch?v=YHznYB275Mg&feature=youtu.be)
1. [ ] New team member: Review [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq/)
1. [ ] New team member: Review [Customer Case Studies](https://about.gitlab.com/customers/)

**Assessment**
1. [ ] Manager: Sign-off of completed section

#### Day 5

##### Messaging

**Goal:** Understand how to connect the GitLab buyer pain points to your marketing

1. [ ] New team member: Review [why people choose GitLab over GitHub](https://medium.com/@Brickblock/why-we-use-gitlab-8829a7712c3b)
1. [ ] New team member: Read the [GitLab Elevator Pitch](https://about.gitlab.com/handbook/marketing/product-marketing/#elevator-pitch)
1. [ ] New team member: Watch [Messaging that has Worked Well for GitLab](https://www.youtube.com/watch?v=8LDlcvOgn9w)
1. [ ] New team member: Review [this article on moving from CE -> EE](https://about.gitlab.com/handbook/positioning-faq/#were-already-using-gitlab-ce-for-free-why-should-we-upgrade)
1. [ ] New team member: Watch [GitLab Sales Training - Security Products](https://www.youtube.com/watch?v=O1XFOlnywDI&feature=youtu.be)
1. [ ] New team member: Watch [Why Auto DevOps Really Matters](https://www.youtube.com/watch?v=56chpCftwJQ&feature=youtu.be)

**Assessment**
1. [ ] Manager: Sign-off of completed section

### Week 3: Community Empathy

#### Days 1 - 5

1. [ ] New team member: Join the [#advocate-for-a-day Slack channel](https://gitlab.slack.com/messages/CB16DMSLC).
1. [ ] New team member: Read ["Advocate for a Day" tips](https://docs.google.com/document/d/1wOLjPpJvwblPObvFV3WPxagPMfV0lxYyz_SAVB_iNr8/edit).
1. [ ] New team member: Coordinate with Community Advocate Manager on volunteering for Community Advocate shifts where you will respond to people on Twitter & HackerNews.
1. [ ] New team member: Read this [user story](https://about.gitlab.com/2018/06/21/leah-petersen-user-spotlight/) and watch the accompanying video.

<!-- end marketing section -->

<!-- Begin BDR Section (department tasks & bootcamp); SDR, Sales, & Customer Success sections are below -->

#### For Inbound BDRs Only

1. [ ] People Ops: [Upgrade new team member's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] Manager: Invite to marketing meeting.
1. [ ] Manager: Invite to sales meeting.
1. [ ] Manager: Invite to weekly inbound BDR meeting.
1. [ ] Manager: Schedule weekly 1:1 meeting.
1. [ ] Business Operations (@msnow1): Add to Salesforce.
1. [ ] Business Operations (@msnow1): Add to Clearbit.
2. [ ] Sales Operations (@francisaquino): Add to Chorus as Listener.
3. [ ] Sales Operations (@francisaquino): Ensure integration between Chorus and Outreach.
1. [ ] Marketing (@jjcordz): Give the new team member an Outreach license.
1. [ ] Marketing (@jjcordz): Add to LinkedIn SalesNav.
1. [ ] Inbound Sales (@mollybeth): Give the new team member an Drift license.
1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs.
1. [ ] New team member: In the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit) and [Competition](https://about.gitlab.com/comparison/)
1. [ ] New team member: Familiarize yourself with the [Support](https://about.gitlab.com/handbook/support/) process.

<!-- add `## General Onboarding` and a new line to the first 2 lines of this issue if including a bootcamp -->

## BDR Specific Onboarding

When a GitLab BDR starts they will need to complete a 1-month intensive business development onboarding program.

**Daily Assessments**

Throughout your onboarding, assessments are given to test your knowledge over the topics learned that day. The assessment results are sent to your team lead upon completion and will be reviewed in your weekly 1:1s throughout the duration of your onboarding.

### Week 1

#### General Onboarding

1. [ ] New team member: Complete the first half of this onboarding issue as part of your general onboarding.

#### BDR Team Culture

**Goal:** Understand what it means to be a BDR at GitLab
1. [ ] New team member: Watch [Welcome to the Business Development Team](https://www.youtube.com/watch?v=dGbyVPSz9qY)
1. [ ] New team member: Watch [The GitLab BDR team Mission, Expectations for Your Performance and Our Vision for Success]()
1. [ ] New team member: Review the [BDR Overview](https://about.gitlab.com/marketing/marketing-sales-development/sdr/#bdr-standards)
1. [ ] New team member: Review the [Sales Accepted Opportunity Criteria](https://about.gitlab.com/handbook/business-ops/#criteria-for-sales-accepted-opportunity-sao)
1. [ ] New team member: Discuss your understanding of the BDR Team culture, expectations and your goals with your manager
1. [ ] New team member: Submit video: Describe the culture of the GitLab BDR team, the expectations on you as a BDR at GitLab, and your goals for the role

**Assessment**
1. [ ] New team member: Take Quiz: [BDR Culture, Overview, Expectations](https://goo.gl/forms/EsMXdBoitp3DQouG2)
1. [ ] Manager: Sign-off of completed section

### Week 2

#### Day 1

##### GitLab Buyer

**Goal:** Understand the people that buy and use GitLab

1. [ ] New team member: Watch [Understanding the GitLab Buyer: Personas and Pain Points](https://youtu.be/-UITZi0mXeU)
1. [ ] New team member: Watch [Persona: Chief Architect/Head of DevOps](https://www.youtube.com/watch?v=qyELotxsQzY)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): pages 115 and 120
1. [ ] New team member: Watch [Persona: Head of IT](https://www.youtube.com/watch?v=LUh5eevH3F4)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): pages 116 and 121
1. [ ] New team member: Watch [Persona: DevOps Director/Lead](https://www.youtube.com/watch?v=5_D4brnjwTg)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): pages 117 and 122
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): pages 118 and 123
1. [ ] New team member: Discuss the four major Personas and their Pain Points with your manager: who are they, what are their common titles, what are their main responsibilities, what are their top 3 pain points
1. [ ] New team member: Submit video: Describe in your own words the components, attributes and pain points of Head of DevOps
1. [ ] New team member: Submit video: Describe in your own words the components, attributes and pain points of Head of IT
1. [ ] New team member: Submit video: Describe in your own words the components, attributes and pain points of DevOps Director

**Assessment**
1. [ ] New team member: Take Quiz: [The GitLab Buyer](https://goo.gl/forms/mGEjlhAIqU56lXKm1)
1. [ ] Manager: Sign-off of completed section

#### Day 2

##### GitLab Industry

**Goal:** Understand the industry in which GitLab competes

1. [ ] New team member: Watch [Understand the Industry where GitLab Competes](https://www.youtube.com/watch?v=qQ0CL3J08lI)
1. [ ] New team member: Watch [What is DevOps?](https://www.youtube.com/watch?v=_I94-tJlovg)
1. [ ] New team member: Review [The State of DevOps 2017](https://www.bmc.com/content/dam/bmc/migration/pdf/Interop+ITX+State+of+DevOps_Final2-002-V1.pdf)
1. [ ] New team member: Review [Understanding the Software Development Lifecycle](https://about.gitlab.com/sdlc/)
1. [ ] New team member: Bookmark the [Glossary of Terms](https://docs.gitlab.com/ce/university/glossary/)
1. [ ] New team member: Watch [Understanding GitLab's Competitors in the Industry](https://www.youtube.com/watch?v=GDqGO5cv1Mk)
1. [ ] New team member: Review [Comparison](https://about.gitlab.com/comparison/)
1. [ ] New team member: Review [Direction](https://about.gitlab.com/direction/#single-application)
1. [ ] New team member: Review and subscribe to these blogs
   1. [ ] [Hacker News](https://news.ycombinator.com/)
   1. [ ] [Martin Fowler](https://martinfowler.com/)
   1. [ ] [New Stack](https://thenewstack.io/)
1. [ ] New team member: Review and subscribe to the [GitLab Blog](https://about.gitlab.com/blog/)
1. [ ] New team member: Review 3 keynotes from [this conference](https://www.youtube.com/playlist?list=PLvk9Yh_MWYuyq_rn0H-AP3ORoQ1QM0s1L)
1. [ ] New team member: Watch the first 30 minutes of the [AWS conference keynote](https://www.youtube.com/watch?v=1IxDLeFQKPk)

**Assessment**
1. [ ] New team member: Take Quiz: [The Industry where GitLab Competes](https://goo.gl/forms/u6XZNcMDBBNu5f4b2)
1. [ ] Manager: Sign-off of completed section

#### Days 3 & 4

##### The GitLab Solution

**Goal:** Understand how the GitLab solution solves the pain points for the GitLab buyer

1. [ ] New team member: Watch [The GitLab Value Proposition 2018](https://www.youtube.com/watch?v=6Dsdd1LSf40)
1. [ ] New team member: Watch [Where GitLab Fits in Within Current Tool Stack](https://www.youtube.com/watch?v=YHznYB275Mg&feature=youtu.be)
1. [ ] New team member: Review [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq/)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Value Proposition for Head of DevOps, page 120
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Value Proposition for Head of Engineering, page 121
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Value Proposition for DevOps Engineer, page 122
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Value Proposition Generator, page 12-16
1. [ ] New team member: [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Create Value Propositions for these Personas: Chief Architect, Head of IT, DevOps Director, pages 17-19
1. [ ] New team member: Discuss the Value Propositions created with your manager
1. [ ] New team member: Submit video: The GitLab Value Proposition for a Chief Architect
1. [ ] New team member: Submit video: The GitLab Value Proposition for a Head of IT
1. [ ] New team member: Submit video: The GitLab Value Proposition for a DevOps Director
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): GitLab Ideal Customer Profile, page 22
1. [ ] New team member: Review [Customer Case Studies](https://about.gitlab.com/customers/)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Comcast, Delta and Ticketmaster Customer Stories in, pages 125-129
1. [ ] New team member: Discuss the Comcast, Delta and Ticketmaster customer stories with your manager
1. [ ] New team member: Submit video: Tell the Comcast customer story in your own words
1. [ ] New team member: Submit video: Tell the Delta customer story in your own words
1. [ ] New team member: Submit video: Tell the Ticketmaster customer story in your own words

**Assessment**
1. [ ] New team member: Take Quiz: [The GitLab Solution](https://goo.gl/forms/EwiIZqHAtXoG7yFU2)
1. [ ] Manager: Sign-off of completed section

#### Day 5

##### Messaging

**Goal:** Understand how to connect the GitLab buyer pain points to your calls, email messaging & social strategies

1. [ ] New team member: Read the [GitLab Elevator Pitch](https://about.gitlab.com/handbook/marketing/product-marketing/#elevator-pitch)
1. [ ] New team member: Watch [Messaging that has Worked Well for GitLab](https://www.youtube.com/watch?v=8LDlcvOgn9w)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Research Steps, pages 33-35
1. [ ] New team member: Review [this](https://about.gitlab.com/handbook/sales-training/#6-questions-sales-reps-should-but-dont-ask-themselves-before-sending-an-email-)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Messaging Research Steps, pages 33-35
1. [ ] New team member: Review [this article & webinar recording](https://www.saleshacker.com/how-to-write-the-perfect-sales-email/) on writing effective emails
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Email Builder, pages 64-66
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Social Builder, page 67
1. [ ] New team member: Review [this article](https://blog.hubspot.com/sales/sales-voicemail-tips-that-guarantee-callbacks) on leaving voicemails
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Voicemail Builder, pages 62-63
1. [ ] New team member: Watch [Converting Free to Paid](https://www.youtube.com/watch?v=56eRRl6zFuA&feature=youtu.be)
1. [ ] New team member: Review [this article on moving from CE -> EE](https://about.gitlab.com/handbook/positioning-faq/#were-already-using-gitlab-ce-for-free-why-should-we-upgrade)
1. [ ] New team member: Watch [GitLab Sales Training - Security Products](https://www.youtube.com/watch?v=O1XFOlnywDI&feature=youtu.be)
1. [ ] New team member: Watch [Why Auto DevOps Really Matters](https://www.youtube.com/watch?v=56chpCftwJQ&feature=youtu.be)

**Assessment**
1. [ ] New team member: Take Quiz: [GitLab Messaging](https://goo.gl/forms/T2cIYCPmpROBFgIh1)
1. [ ] Manager: Sign-off of completed section

### Week 3

#### Days 1 & 2

##### Tools

**Goal:** Deep dive tools GitLab BDRs use everyday

1. [ ] New team member: Watch Using Salesforce at GitLab - waiting on video
1. [ ] New team member: Bookmark [Salesforce Training Trailhead](https://www.salesforce.com/services/training/overview/)
1. [ ] New team member: Watch [Using Outreach at GitLab](https://www.youtube.com/watch?v=Sg0huunn6vo)
1. [ ] New team member: Bookmark [Outreach University](https://university.outreach.io/)
1. [ ] New team member: Watch [Killer Content & Using Sequences](https://university.outreach.io/live-training/80859)
1. [ ] New team member: Watch [Executing Tasks with Precision](https://university.outreach.io/live-training/80896)
1. [ ] New team member: Watch [Lead Management & Research](https://www.youtube.com/watch?v=dGbyVPSz9qY)
1. [ ] New team member: Watch Lead/Contact Management - Disposition Status - need link
1. [ ] New team member: Review [LinkedIn Sales Navigator Tutuorial](https://youtu.be/jxPw_pXqd2s)
1. [ ] New team member: Discuss any questions on tools with manager
1. [ ] New team member: Submit video demonstrating basic understanding of Salesforce
1. [ ] New team member: Submit video demonstrating basic understanding of Outreach
1. [ ] New team member: Submit video demonstrating basic understanding of LinkedIn Sales Navigator

**Assessment**
1. [ ] Manager: Sign-off of completed section

#### Days 3 & 4

##### GitLab Sales Skills

**Goal:** Gain an understanding of basic sales skills

1. [ ] New team member: Watch [Best Practices for Opening Calls](https://www.youtube.com/watch?v=jR1RGSBHo5Q&feature=youtu.be)
1. [ ] New team member: Watch [Best Practices for Asking Questions](https://www.youtube.com/watch?v=Lv4nozScm-M)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Asking Questions, pages 92-93
1. [ ] New team member: Watch [Best Practices for Closing for Appointments](https://www.youtube.com/watch?v=FfUfmguwki4&feature=youtu.be)
1. [ ] New team member: Watch [Objection Handling](https://www.youtube.com/watch?v=Q_RnXedP0JI)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Objection Handling, pages 90, 95
1. [ ] New team member: Watch [Objection Handling Techniques](https://youtu.be/yEwunIvRXlY), start at 27 minute mark
1. [ ] New team member: Watch [Overcoming Call Reluctance](https://www.youtube.com/watch?v=M1skw4LXPlk)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Phone Contact Best Practices, pages 83-84
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Pivot Best Practices, pages 85-86
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Dealing with Gatekeepers, page 87
1. [ ] New team member: Review [Account Mapping](http://predictablerevenue.com/blog/mapping-calls-vs-are-we-a-fit-calls-key-differences)
1. [ ] New team member: Watch  [Time Management](https://youtu.be/bA_xdjwAfZM)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Touch Pattern Stages, pages 36-42
1. [ ] New team member: Review [How to Create an Opportunity/Handoff Process](https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#how-to-create-an-opportunity)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Qualification Stages, pages 43-49
1. [ ] New team member: Discuss the Sales Skills reviewed with your manager and review any questions
1. [ ] New team member: Submit video: review best practices for opening calls
1. [ ] New team member: Submit video: review best practices for asking questions on calls
1. [ ] New team member: Submit video: review best practices for objection handling
1. [ ] New team member: Submit video: review best practices for overcoming call reluctance

**Assessment**
1. [ ] New team member: Take Quiz: [Sales Skills](https://goo.gl/forms/qdVuoz4eVEZrD8Ye2)
1. [ ] Manager: Sign-off of completed section

#### Day 5

##### Business Acumen

**Goal:** Gain an understanding of basic business acumen as it applies to the GitLab buyer

1. [ ] New team member: Watch [How to Boost Your Business Acumen](https://www.youtube.com/watch?v=-h6XciVfraI)
1. [ ] New team member: Watch [Business Acumen for Sales Professionals](https://www.youtube.com/watch?v=XRt9RIvxUTA)
1. [ ] New team member: Review [this article](http://content.intland.com/blog/agile/devops/how-does-devops-help-your-business-to-grow) about DevOps and IT Operations
1. [ ] New team member: Review [this article](https://www.quickstart.com/blog/post/how-devops-can-help-your-business/) about how DevOps can help your business
1. [ ] New team member: Review [this article](http://www.zdnet.com/article/devops-what-is-it-and-how-can-it-help-your-business/) about what DevOps is, and how can it help your business?
1. [ ] New team member: Review [this article](https://www.sam-solutions.com/blog/what-are-the-benefits-of-devops-for-your-business/) about what the benefits of DevOps are for Your business?
1. [ ] New team member: Review [this article](http://blog.icreon.us/maintain/devops-for-business) about how DevOps can make your business better
1. [ ] New team member: Discuss the business impact of using GitLab with your manager
1. [ ] New team member: Submit video: How does using GitLab positively effect a company financially?

**Assessment**
1. [ ] New team member: Take Quiz: [Business Acumen](https://goo.gl/forms/I2sllsXiU96HNL7F2)
1. [ ] Manager: Sign-off of completed section

### Week 4

#### Day 1

##### On the Job Learning

**Goal:** Gain an understanding of how to learn continuously

1. [ ] New team member: Set schedule to read one Sales-related book per month and discuss with your team, suggestions can be found in the [Handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#personal-development)

**Assessment**
1. [ ] Manager: Sign-off of completed section

#### Day 2

##### Ongoing Coaching

**Goal:** Working with your manager to ensure the GitLab SDR stays on track to meet goals

1. [ ] New team member: Create action plan for first month and review with manager
1. [ ] New team member: Submit one email with responses per week to manager for review
1. [ ] Manager: Review coaching sheet for 1:1s with BDR

**Assessment**
1. [ ] Manager: Sign-off of completed section

<!-- end BDR Section -->

<!-- Begin SDR Section (department tasks & bootcamp); Sales & Customer Success sections are below -->

#### For Outbound SDRs Only

1. [ ] People Ops: [Upgrade new team member's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] Manager: Invite to marketing meeting.
1. [ ] Manager: Invite to sales meeting.
1. [ ] Manager: Invite to weekly outbound SDR meeting.
1. [ ] Manager: Schedule weekly 1:1 meeting.
1. [ ] Business Operations (@msnow1): Add to Salesforce.
1. [ ] Business Operations (@msnow1): Add to Discover Org.
1. [ ] Business Operations (@msnow1): Add to Clearbit.
2. [ ] Sales Operations (@francisaquino): Add to Chorus as Listener.
3. [ ] Sales Operations (@francisaquino): Ensure integration between Chorus and Outreach.
1. [ ] Marketing (@jjcordz): Give the new team member an Outreach license.
1. [ ] Marketing (@jjcordz): Add to LinkedIn SalesNav.
1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs.
1. [ ] New team member: In the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit) and [Competition](https://about.gitlab.com/comparison/)
1. [ ] New team member: Familiarize yourself with the [Support](https://about.gitlab.com/handbook/support/) process.

<!-- add `## General Onboarding` and a new line to the first 2 lines of this issue if including a bootcamp -->

## SDR Specific Onboarding

When a GitLab SDR starts they will need to complete a 1-month intensive sales development onboarding program.

**Daily Assessments**

Throughout your onboarding, assessments are given to test your knowledge over the topics learned that day. The assessment results are sent to your team lead upon completion and will be reviewed in your weekly 1:1s throughout the duration of your onboarding.

### Week 1

#### General Onboarding

1. [ ] New team member: Complete the first half of this onboarding issue as part of your general onboarding.

#### SDR Team Culture

**Goal:** Understand what it means to be an SDR at GitLab

1. [ ] New team member: Watch [Welcome to the Sales Development Team](https://youtu.be/ESZtmKUn9QY)
1. [ ] New team member: Watch [The GitLab SDR team Mission, Expectations for Your Performance and Our Vision for Success](https://www.youtube.com/watch?v=h2zd8BYQwhM)
1. [ ] New team member: Review the [SDR Overview](https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#how-to-sdr)
1. [ ] New team member: Review the [Sales Accepted Opportunity Criteria](https://about.gitlab.com/handbook/business-ops/#criteria-for-sales-accepted-opportunity-sao)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Metric Expectations, pages 6-7
1. [ ] New team member: Discuss your understanding of the SDR Team culture, expectations and your goals with your manager
1. [ ] New team member: Watch [this](https://www.youtube.com/watch?v=cRV1219uk_o) video & subscribe to the SDR Chronicles Channel
1. [ ] New team member: Listen to [this](https://youtu.be/fofWiks7OhY) podcast with Ralph Barsi
1. [ ] New team member: Submit video: Describe the culture of the GitLab SDR team, the expectations on you as an SDR at GitLab, and your goals for the role

**Assessment**
1. [ ] New team member: Take Quiz: [Culture, Overview, Expectations](https://goo.gl/forms/R2mXCbyrblNU5xYF2)
1. [ ] Manager: Sign-off of completed section

### Week 2

#### Day 1

##### GitLab Buyer

**Goal:** Understand the people that buy and use GitLab

1. [ ] New team member: Watch [Understanding the GitLab Buyer: Personas and Pain Points](https://youtu.be/-UITZi0mXeU)
1. [ ] New team member: Watch [Persona: Chief Architect/Head of DevOps](https://www.youtube.com/watch?v=qyELotxsQzY)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): pages 115 and 120
1. [ ] New team member: Watch [Persona: Head of IT](https://www.youtube.com/watch?v=LUh5eevH3F4)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): pages 116 and 121
1. [ ] New team member: Watch [Persona: DevOps Director/Lead](https://www.youtube.com/watch?v=5_D4brnjwTg)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): pages 117 and 122
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): pages 118 and 123
1. [ ] New team member: Discuss the four major Personas and their Pain Points with your manager: who are they, what are their common titles, what are their main responsibilities, what are their top 3 pain points
1. [ ] New team member: Submit video: Describe in your own words the components, attributes and pain points of Head of DevOps
1. [ ] New team member: Submit video: Describe in your own words the components, attributes and pain points of Head of IT
1. [ ] New team member: Submit video: Describe in your own words the components, attributes and pain points of DevOps Director

**Assessment**
1. [ ] New team member: Take Quiz: [The GitLab Buyer](https://goo.gl/forms/naq8Jru9dMOJDqzt1)
1. [ ] Manager: Sign-off of completed section

#### Day 2

##### GitLab Industry

**Goal:** Understand the industry in which GitLab competes

1. [ ] New team member: Watch [Understand the Industry where GitLab Competes](https://www.youtube.com/watch?v=qQ0CL3J08lI)
1. [ ] New team member: Watch [What is DevOps?](https://www.youtube.com/watch?v=_I94-tJlovg)
1. [ ] New team member: Review [The State of DevOps 2017](https://www.bmc.com/content/dam/bmc/migration/pdf/Interop+ITX+State+of+DevOps_Final2-002-V1.pdf)
1. [ ] New team member: Review [Understanding the Software Development Lifecycle](https://about.gitlab.com/sdlc/)
1. [ ] New team member: Bookmark the [Glossary of Terms](https://docs.gitlab.com/ce/university/glossary/)
1. [ ] New team member: Watch [Understanding GitLab's Competitors in the Industry](https://www.youtube.com/watch?v=GDqGO5cv1Mk)
1. [ ] New team member: Review [Comparison](https://about.gitlab.com/comparison/)
1. [ ] New team member: Review [Direction](https://about.gitlab.com/direction/#single-application)
1. [ ] New team member: Review and subscribe to these blogs
   1. [ ] [Hacker News](https://news.ycombinator.com/)
   1. [ ] [Martin Fowler](https://martinfowler.com/)
   1. [ ] [New Stack](https://thenewstack.io/)
1. [ ] New team member: Review and subscribe to the [GitLab Blog](https://about.gitlab.com/blog/)
1. [ ] New team member: Review 3 keynotes from [this conference](https://www.youtube.com/playlist?list=PLvk9Yh_MWYuyq_rn0H-AP3ORoQ1QM0s1L)
1. [ ] New team member: Watch the first 30 minutes of the [AWS conference keynote](https://www.youtube.com/watch?v=1IxDLeFQKPk)

**Assessment**
1. [ ] New team member: Take Quiz: [The Industry where GitLab Competes](https://goo.gl/forms/QWLC9jvbd1AErXCt1)
1. [ ] Manager: Sign-off of completed section

#### Days 3 & 4

##### The GitLab Solution

**Goal:** Understand how the GitLab solution solves the pain points for the GitLab buyer

1. [ ] New team member: Watch [The GitLab Value Proposition 2018](https://www.youtube.com/watch?v=6Dsdd1LSf40)
1. [ ] New team member: Watch [Where GitLab Fits in Within Current Tool Stack](https://www.youtube.com/watch?v=YHznYB275Mg&feature=youtu.be)
1. [ ] New team member: Review [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq/)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Value Proposition for Head of DevOps, page 120
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Value Proposition for Head of Engineering, page 121
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Value Proposition for DevOps Engineer, page 122
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Value Proposition Generator, page 12-16
1. [ ] New team member: Playbook: Create Value Propositions for these Personas: Chief Architect, Head of IT, DevOps Director, pages 17-19
1. [ ] New team member: Discuss the Value Propositions created with your manager
1. [ ] New team member: Submit video: The GitLab Value Proposition for a Chief Architect
1. [ ] New team member: Submit video: The GitLab Value Proposition for a Head of IT
1. [ ] New team member: Submit video: The GitLab Value Proposition for a DevOps Director
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): GitLab Ideal Customer Profile, page 22
1. [ ] New team member: Review [Customer Case Studies](https://about.gitlab.com/customers/)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Comcast, Delta and Ticketmaster Customer Stories in, pages 125-129
1. [ ] New team member: Discuss the Comcast, Delta and Ticketmaster customer stories with your manager
1. [ ] New team member: Submit video: Tell the Comcast customer story in your own words
1. [ ] New team member: Submit video: Tell the Delta customer story in your own words
1. [ ] New team member: Submit video: Tell the Ticketmaster customer story in your own words
1. [ ] New team member: Watch [GitLab Sales Training - Security Products](https://www.youtube.com/watch?v=O1XFOlnywDI&feature=youtu.be)
1. [ ] New team member: Watch [Why Auto DevOps Really Matters](https://www.youtube.com/watch?v=56chpCftwJQ&feature=youtu.be)


**Assessment**
1. [ ] New team member: Take Quiz: [The GitLab Solution](https://goo.gl/forms/62BOH6jsy80wk42p1)
1. [ ] Manager: Sign-off of completed section

#### Day 5

##### Messaging

**Goal:** Understand how to connect the GitLab buyer pain points to your calls, email messaging & social strategies

1. [ ] New team member: Read the [GitLab Elevator Pitch](https://about.gitlab.com/handbook/marketing/product-marketing/#elevator-pitch)
1. [ ] New team member: Watch [Messaging that has Worked Well for GitLab](https://www.youtube.com/watch?v=8LDlcvOgn9w)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Research Steps, pages 33-35
1. [ ] New team member: Review [Questions Sales Reps should ask before sending an email](https://about.gitlab.com/handbook/sales-training/#6-questions-sales-reps-should-but-dont-ask-themselves-before-sending-an-email-)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Messaging Research Steps, pages 33-35
1. [ ] New team member: Review [this article & webinar recording](https://www.saleshacker.com/how-to-write-the-perfect-sales-email/) on writing effective emails
1. [ ] New team member: Download [Vidyard](https://www.vidyard.com/govideo/) And review this [example messaging](https://share.vidyard.com/watch/HyoPfVvUJtpduNSXYtbQgP)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Email Builder, pages 64-66
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Social Builder, page 67
1. [ ] New team member: Review [this article](https://blog.hubspot.com/sales/sales-voicemail-tips-that-guarantee-callbacks) on leaving voicemails
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Voicemail Builder, pages 62-63
1. [ ] New team member: Watch [Converting Free to Paid](https://www.youtube.com/watch?v=56eRRl6zFuA&feature=youtu.be)
1. [ ] New team member: Review [this article on moving from CE -> EE](https://about.gitlab.com/handbook/positioning-faq/#were-already-using-gitlab-ce-for-free-why-should-we-upgrade)
1. [ ] New team member: Discuss and practice your voicemail and email messaging with your manager
1. [ ] New team member: Submit a video: How you would construct and email? Give an example
1. [ ] New team member: Submit a video: How you would construct a voicemail? Give an example

**Assessment**
1. [ ] New team member: Send a Vidyard message to your manager and review during your weekly 1:1
1. [ ] New team member: Take Quiz: [GitLab Messaging](https://goo.gl/forms/jzSkMhnOA6jqTnOB3)
1. [ ] Manager: Sign-off of completed section

### Week 3

#### Days 1 & 2

##### Tools

**Goal:** Deep dive into tools that GitLab SDRs use everyday

1. [ ] New team member: Watch Using Salesforce at GitLab - waiting on video
1. [ ] New team member: Bookmark [Salesforce Training Trailhead](https://www.salesforce.com/services/training/overview/)
1. [ ] New team member: Watch Lead/Contact Management - Disposition Status- need link
1. [ ] New team member: Watch [Using Outreach at GitLab](https://www.youtube.com/watch?v=Sg0huunn6vo)
1. [ ] New team member: Bookmark [Outreach University](https://university.outreach.io/)
1. [ ] New team member: Watch [Killer Content & Using Sequences](https://university.outreach.io/live-training/80859)
1. [ ] New team member: Watch [Executing Tasks with Precision](https://university.outreach.io/live-training/80896)
1. [ ] New team member: Watch [Using Discoverorg at GitLab](https://www.youtube.com/watch?v=xbvohNciSsg)
1. [ ] New team member: Read [Uploading lists of contacts into SDFC](https://about.gitlab.com/handbook/business-ops/#list-imports)
1. [ ] New team member: Watch [How to upload/import a list to SFDC from Discoverorg](https://drive.google.com/file/d/1saQqwLuVpy50LfIH0tkXY_dUt4Rybxth/view?usp=sharing)
1. [ ] New team member: Bookmark [Discoverorg Resource Center](https://discoverorg.com/sales-and-marketing-tools-resource-center/)
1. [ ] New team member: Watch [Using Linkedin Sales Navigator at GitLab](https://www.youtube.com/watch?v=-a3eADr0ylc&feature=youtu.be)
1. [ ] New team member: Review [LinkedIn Sales Navigator Tutuorial](https://youtu.be/jxPw_pXqd2s)
1. [ ] New team member: Discuss any questions on tools with manager
1. [ ] New team member: Submit video demonstrating basic understanding of Salesforce
1. [ ] New team member: Submit video demonstrating basic understanding of Discoverorg
1. [ ] New team member: Submit video demonstrating basic understanding of Outreach
1. [ ] New team member: Submit video demonstrating basic understanding of LinkedIn Sales Navigator

**Assessment**
1. [ ] Manager: Sign-off of completed section

#### Days 3 & 4

##### GitLab Sales Skills

**Goal:** Gain an understanding of basic sales skills

1. [ ] New team member: Watch [Best Practices for Opening Calls](https://www.youtube.com/watch?v=jR1RGSBHo5Q&feature=youtu.be)
1. [ ] New team member: Watch [Best Practices for Asking Questions](https://www.youtube.com/watch?v=Lv4nozScm-M)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Asking Questions, pages 92-93
1. [ ] New team member: Watch [Best Practices for Closing for Appointments](https://www.youtube.com/watch?v=FfUfmguwki4&feature=youtu.be)
1. [ ] New team member: Watch [Objection Handling](https://www.youtube.com/watch?v=Q_RnXedP0JI)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Objection Handling, pages 90, 95
1. [ ] New team member: Watch [Objection Handling Techniques](https://youtu.be/yEwunIvRXlY), start at 27 minute mark
1. [ ] New team member: Watch [Overcoming Call Reluctance](https://www.youtube.com/watch?v=M1skw4LXPlk)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Phone Contact Best Practices, pages 83-84
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Pivot Best Practices, pages 85-86
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Dealing with Gatekeepers, page 87
1. [ ] New team member: Review [Account Mapping](http://predictablerevenue.com/blog/mapping-calls-vs-are-we-a-fit-calls-key-differences)
1. [ ] New team member: Watch [Time Management](https://youtu.be/bA_xdjwAfZM)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Touch Pattern Stages, pages 36-42
1. [ ] New team member: Review [How to Create an Opportunity/Handoff Process](https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#how-to-create-an-opportunity)
1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Qualification Stages, pages 43-49
1. [ ] New team member: Discuss the Sales Skills reviewed with your manager and review any questions
1. [ ] New team member: Submit video: review best practices for opening calls
1. [ ] New team member: Submit video: review best practices for asking questions on calls
1. [ ] New team member: Submit video: review best practices for objection handling
1. [ ] New team member: Submit video: review best practices for overcoming call reluctance

**Assessment**
1. [ ] New team member: Take Quiz: [Sales Skills](https://goo.gl/forms/NRdIb3eMoJQSyL2b2)
1. [ ] Manager: Sign-off of completed section

### Week 4

#### Day 1

##### Business Acumen

**Goal:** Gain an understanding of basic business acumen as it applies to the GitLab buyer

1. [ ] New team member: Watch [How to Boost Your Business Acumen](https://www.youtube.com/watch?v=-h6XciVfraI)
1. [ ] New team member: Watch [Business Acumen for Sales Professionals](https://www.youtube.com/watch?v=XRt9RIvxUTA)
1. [ ] New team member: Review [this article](http://content.intland.com/blog/agile/devops/how-does-devops-help-your-business-to-grow)
1. [ ] New team member: Review [this article](https://www.quickstart.com/blog/post/how-devops-can-help-your-business/)
1. [ ] New team member: Review [this article](http://www.zdnet.com/article/devops-what-is-it-and-how-can-it-help-your-business/)
1. [ ] New team member: Review [this article](https://www.sam-solutions.com/blog/what-are-the-benefits-of-devops-for-your-business/)
1. [ ] New team member: Review [this article](http://blog.icreon.us/maintain/devops-for-business)
1. [ ] New team member: Discuss the business impact of using GitLab with your manager
1. [ ] New team member: Submit video: How does using GitLab positively effect a company financially?

**Assessment**
1. [ ] New team member: Take Quiz: [Business Acumen](https://goo.gl/forms/Vcry79o32hqGVYuX2)
1. [ ] Manager: Sign-off of completed section

#### Day 2

##### On the Job Learning

**Goal:** Gain an understanding of how to learn continuously

1. [ ] New team member: Prepare script and call 100 old leads (practice leads)
1. [ ] New team member: Conduct Account Mapping calls: determine 3 major personas in 10 target accounts
1. [ ] New team member: Conduct 3 role play cold calls with other SDRs (one per Persona)
1. [ ] New team member: Conduct 3 role play calls with your manager (one per Persona)
1. [ ] New team member: Set schedule to read one Sales-related book per month and discuss with your team, suggestions can be found in the [Handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#personal-development)
1. [ ] New team member: Conduct Account Mapping calls: determine 3 major personas in 10 target accounts

**Assessment**
1. [ ] Manager: Sign-off of completed section

#### Day 3

##### Ongoing Coaching

**Goal:** Working with your manager to ensure the GitLab SDR stays on track to meet goals

1. [ ] New team member: Review [Playbook](https://drive.google.com/file/d/0Bw0jk96h5OMdUkJEa0lja1Z4STQ/view?ts=5aa02575): Monthly Success Plan, page 81
1. [ ] New team member: Create action plan for first month and review with manager
1. [ ] New team member: Submit one call recording per week to manager for review
1. [ ] New team member: Submit one email with responses per week to manager for review
1. [ ] Manager: Review coaching sheet for 1:1s with SDR
1. [ ] Manager: Review Team Meeting sheet expectations


**Assessment**
1. [ ] Manager: Sign-off of completed section & if all tests were successful order the custom [Super Leaf Coin](https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#super-leaf-coin)


<!-- end SDR Section -->


<!-- Begin Sales Section (department tasks & bootcamp); Customer Success is below -->

#### For Sales Only

1. [ ] Manager: Invite to sales meeting.
1. [ ] Manager: Schedule weekly 1:1 meeting.
1. [ ] People Ops: [Upgrade new team member's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] Business Operations (@msnow1): Add to [Salesforce].
1. [ ] Business Operations (@msnow1): Add to Discover Org - only for positions that do not have support from an SDR.
1. [ ] Business Operations (@msnow1): Add to Clearbit.
2. [ ] Sales Operations (@francisaquino): Add to Chorus as Recorder.
1. [ ] Marketing (@jjcordz): Give the new team member an Outreach license.
1. [ ] Marketing (@jjcordz): Add to LinkedIn SalesNav.
1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs.
1. [ ] New team member: In the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit) and [Competition](https://about.gitlab.com/comparison/)
1. [ ] New team member: Familiarize yourself with the [Support](https://about.gitlab.com/handbook/support/) process.


## Sales Bootcamp

<!-- add `## General Onboarding` and a new line to the 2 first lines of this issue if including a bootcamp -->

When a GitLab salesperson starts they will need to complete an intensive sales bootcamp. Create a weekly study plan to make sure you understand all the theory content and complete all the tests.

### Week 1

**Theory:**
[GitLab University - Beginner](https://docs.gitlab.com/ce/university/#beginner).

**Tests:**

1. [ ] [Version Control Systems Test](http://goo.gl/forms/8H8SNcH70T)
1. [ ] [Intro to Git Test](http://goo.gl/forms/GgWF1T5Ceg)
1. [ ] Create a group on GitLab.com named `glu_yourname`.
1. [ ] Add your direct manager and **Chad Malchow** as master to the group.
1. [ ] Create a project called `About Me`.
   1. [ ] Add a `README.md` file with a list of things that we should know about you.
1. [ ] Create another project called `90 day plan`.
   1. [ ] Add a `README.md` file with a plan of what you are going to do in your first 90 days and cc your manager
1. [ ] Create a **MR** for each of the projects.
   1. [ ] Assign them to your manager.

### Week 2



**Tasks:**

1. [ ] Study the [Sales Handbook](https://about.gitlab.com/handbook/sales/) page.
2. [ ] Become familiar with the [GitLab Operating Metrics](https://about.gitlab.com/handbook/finance/operating-metrics/), especially Incremental Annual Contract Value (IACV).
1. [ ] Review [How to Process an Order](https://about.gitlab.com/handbook/sales/#sales-order-processing) When you are ready to process your first order, please request time with the sales operations team by asking in the #SFDC Slack channel.
1. [ ] Familiarize yourself with our [Qualification Process](https://about.gitlab.com/handbook/sales-qualification-questions/)
1. [ ] Login to [Salesforce.com](http://www.salesforce.com/).
   1. [ ] Change you password when you are prompted to via email.
1. [ ] Familiarize yourself with these business critical Salesforce reports and views.
   1. [ ] [Accounts you Own](https://na34.salesforce.com/001?fcf=00B61000001XPLz):
    Identify expansion opportunities, see who you have neglected and the mix of customers to prospects you are working on.
   1. [ ] With your manager review the book of business or prospective accounts to determine which accounts you should own based on your assigned territories.
   1. [ ] [Your Current Month Pipeline](https://na34.salesforce.com/00O61000001uYbM): View what you are committing to close this month. Ask yourself if each lead is in the right stage to close this month? What is needed to advance the sale to the next stage?
   1. [ ] [Your Total Pipeline](https://na34.salesforce.com/00O61000001uYbR): Overview your pipeline and get insight into focus areas to reach targets. You will also be shown what needs to be closed and where you need to build up your pipeline through new business, expansion or add-on.
   1. [ ] [Your Leads](https://na34.salesforce.com/00Q?fcf=00B610000027qT9&rolodexIndex=-1&page=1): Make sure you are following up on each lead in a timely manner and have a plan on how you qualify or disqualify them.
   1. [ ] [Your Personal Dashboard](https://na34.salesforce.com/01Z61000000J0gx): Understand where you have been, where you are, where are you going and do you have the pipeline to get to where you need to be.
   1. [ ] [Opportunity Pipeline Hygiene](https://na34.salesforce.com/01Z61000000BHGE): Follow this dashboard and understand how to use the data provided to improve your forecasting and increase your win rates.
   1. [ ] [Sales Leadership Dashboard](https://na34.salesforce.com/01Z610000001s2U): Follow this dashboard and visit a weekly to know how the sales team is doing towards our goal and leading indicators.

1. [ ] Engaging with Legal.
   1. [ ] Before sharing any confidential information with any prospective customer, you should always first ensure that there is a valid NDA in place.  If there is not, the template can be found on the [Legal Page](https://about.gitlab.com/handbook/legal)
   1. [ ] The default position is that GitLab's standard terms, found on the [GitLab Website](https://about.gitlab.com/terms/) will govern all customer transactions.  Should a customer request any changes to the standard terms, or present their own contract paper, please submit an issue through the Legal Issue Tracker, by sending an email following the procedures found on the [Legal Page](https://about.gitlab.com/handbook/legal)
   1. [ ] When agreements are completed and ready for signature, you will be responsible for preparing it for signing in [HelloSign](https://about.gitlab.com/handbook/contracts/) and sending it to the proper signor, with authority, as set out in the [Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/)
   1. [ ] All final, fully executed agreements should be filed in SalesForce, in the appropriate customer record.

### Supporting Material

1. [Our Sales Handbook](https://about.gitlab.com/handbook/sales/)
1. [Step-by-step demo script](https://about.gitlab.com/handbook/sales/demo/)
1. [Sales Best Practices Training](https://about.gitlab.com/handbook/sales-training/)
1. [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)
1. [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
1. [Client Demo of GitLab with Job and Haydn](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=ae7b72c61347030e8aa75328ed4b8660)
1. [Recorded Practice Sales Demos](https://drive.google.com/drive/u/0/folders/0B_XVovPbWgADM1M3VUg1ZVJ0UjQ)
1. [GitLab University Google Drive](https://drive.google.com/drive/u/0/folders/0B41DBToSSIG_NlNFLUEwQ2JHSVk)
1. [Getting Technical Support](https://about.gitlab.com/handbook/support/#internal)

<!-- End Sales Bootcamp -->

<!-- Begin Customer Success Section (department tasks & bootcamp) -->
<!-- add `## General Onboarding` and a new line to the 2 first lines of this issue if including a bootcamp -->

<!-- FOR ALL Customer Success GitLab Team Member -->
### For Customer Success Only

1. [ ] Manager: Invite to sales meeting.
1. [ ] Manager: Schedule weekly 1:1 meeting.
1. [ ] People Ops: Upgrade the new team member's GitLab account within the [account-management group](https://gitlab.com/groups/gitlab-com/account-management/-/group_members) to `Master`.
1. [ ] People Ops: [Upgrade new team member's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] Business Operations (@msnow1): Add to Salesforce.
2. [ ] Sales Operations (@francisaquino): Add to Chorus as Listener or Recorder. If Recorder, must get manager approval.
1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs.
1. [ ] New team member: In the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit) and [Competition](https://about.gitlab.com/comparison/)
1. [ ] New team member: Familiarize yourself with the [Support](https://about.gitlab.com/handbook/support/) process.
1. [ ] New team member: Watch all the customer success courses listed in the [sales training handbook](https://about.gitlab.com/handbook/sales-training/#customer-success-cst-courses)

<!-- FOR ALL Customer Success Git Lab Team Members -->
## Customer Success Bootcamp

When a GitLab Customer Success team member starts they will need to complete an intensive bootcamp, divided into sections below.

#### Client Enagement Pre-reqs

**Git Basics**

**Goal:** Become more familiar with git and GitLab basics

1. [ ] [About Version Control](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit)
1. [ ] [Git-ing Started with Git](https://www.youtube.com/watch?v=Ce5nz5n41z4)
1. [ ] [Try Git](https://learngitbranching.js.org/)

**GitLab General Knowledge**

**Goal:** Understand how GitLab is used by our customers

1. [ ] [GitLab Flow](https://www.youtube.com/watch?v=InKNIvky2KE)
1. [ ] [Learn about GitLab namespaces](https://www.youtube.com/watch?v=r0sJgjR2f5A)
1. [ ] All the [GitLab Basics](http://docs.gitlab.com/ce/gitlab-basics/README.html) that you don't feel comfortable with. Pay special attention to:
   1. [ ] [Command line basics](http://docs.gitlab.com/ce/gitlab-basics/command-line-commands.html)
   1. [ ] [Creating SSH keys](http://docs.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html)
   1. [ ] [Creating a branch](http://docs.gitlab.com/ce/gitlab-basics/create-branch.html)
   1. [ ] [Creating a merge request](http://docs.gitlab.com/ce/gitlab-basics/add-merge-request.html)
1. [ ] Take a look at how the different [GitLab versions compare](https://about.gitlab.com/products/)
1. [ ] Learn the differences between [Core and Enterprise](https://about.gitlab.com/pricing/)
1. [ ] Review the various [features of GitLab](https://about.gitlab.com/features/)
1. [ ] [Understanding Gitlab HA and Gitlab GEO](https://about.gitlab.com/high-availability/)
1. [ ] Watch video: GitLab [Security solutions](https://youtu.be/SP0VSH-NqJs)
1. [ ] Watch video: GitLab [HA-GEO infrastructure benefits](https://youtu.be/fji7nvmOHNQ)
1. [ ] Watch video: GitLab [Services](https://youtu.be/FmimIGNR2S4)
1. [ ] Watch video: GitLab custom in-app monitoring for [Value Stream Mapping](https://www.youtube.com/watch?v=oG0VESUOFAI)
1. [ ] Read: GitLab blog highlighting use of [GitLab for Agile](https://about.gitlab.com/2018/03/05/gitlab-for-agile-software-development/)

**Customer Success Department**

1. [ ] Shadow 2-3 calls with a team member in your Customer Success team (SA, TAM or IE).
1. [ ] Shadow at least 1 call in each of the OTHER Customer Success teams
1. [ ] Pair on 2-3 calls with a team member in your Customer Success team (SA, TAM or IE).

**Support Department**

1. [ ] [Review the GitLab Support documentation](https://about.gitlab.com/handbook/support/)
1. [ ] [Review the process for support tickets](https://about.gitlab.com/handbook/support/workflows/shared/zendesk/finding_tickets.html)

### Strategic Selling

1. [ ] Study the [Sales Handbook](https://about.gitlab.com/handbook/sales/)
1. [ ] Learn how to give an [Idea to Production Demo](https://about.gitlab.com/handbook/marketing/product-marketing/demo/)
1. [ ] Our [Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)
1. [ ] Read about [latest GitLab release](https://about.gitlab.com/blog/categories/releases/)
1. [ ] Review the [POC process](https://about.gitlab.com/handbook/sales/POC/)

**Competition**

1. [ ] [Competition](https://about.gitlab.com/comparison/)
   1. [ ] [GitHub](http://github.com)
   1. [ ] [Bitbucket](https://www.atlassian.com/software/bitbucket/features) and [Jira](https://www.atlassian.com/software/jira)
   1. [ ] [Perforce](https://www.perforce.com/)
   1. [ ] [Microsoft Team Foundation Server (TFS)](https://www.visualstudio.com/tfs/)
   1. [ ] [Jenkins](https://jenkins.io/) and [CloudBees](https://www.cloudbees.com/)
   1. [ ] GitLab's production [Pitch Deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit#slide=id.g3092f78080_20_4) to elaborate GitLab's single platform advantage.

**Integrations**

**Goal:** Understand Integrations with GitLab to other common products

1. [ ] Jira issues and Jenkins pipelines integration [Video](https://www.youtube.com/watch?v=Jn-_fyra7xQ)
1. [ ] GitHub repository to GitLab pipelines integration [Video](https://www.youtube.com/watch?v=qgl3F2j-1cI)
1. [ ] Details integrating to [Jira](https://docs.gitlab.com/ee/user/project/integrations/jira.html)
1. [ ] Details integrating to [Jenkins](https://docs.gitlab.com/ee/integration/jenkins.html)
1. [ ] Details integrating to [GitHub](https://docs.gitlab.com/ee/user/project/integrations/github.html)
1. [ ] Authentication via [LDAP/OAuth](https://docs.gitlab.com/ee/integration/oauth_provider.html)

**Professional Services Statements of Work**

**Goal:** Understand How to Create GitLab SOW's

1. [ ] Review GitLab's [professional services offerings](https://about.gitlab.com/services/)
1. [ ] Learn the SOW creation [process](https://about.gitlab.com/handbook/customer-success/implementation-engineering/selling/)

### Technical "deep" dives

1. [ ] [GitLab I2P In-depth with Mark P.](https://youtu.be/fzlDQ2j-jv8)
1. [ ] Perform each of the following [Installation Methods](https://about.gitlab.com/installation/) on your preferred test environment you chose above:
   1. [ ] Install via [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/)
   1. [ ] Populate with some test data: User account, Project, Issue
1. [ ] Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings
1. [ ] Dive into GitLab for Security by reviewing [security offerings and positioning](https://docs.google.com/presentation/d/1lNr9pz7axLlN7uw7Wkwi_FYMuEh4F4QzPaoJLfReGFk/edit#slide=id.g2823c3f9ca_0_9)
<!-- SaaS vs onprem, SaaS status,  -->

**Other Teams**

**Goal:** Get to know how other teams at GitLab help our product and customers

1. [ ] Take a look at the GitLab.com [Team page](https://about.gitlab.com/team/) to find the resident experts in their fields
1. [ ] Understand what's in the pipeline and proposed features at GitLab's [Direction Page](https://about.gitlab.com/direction/)
1. [ ] Learn [who's who in Product](https://about.gitlab.com/handbook/product/categories/)
1. [ ] Practice searching issues and filtering using [labels](https://gitlab.com/gitlab-org/gitlab-foss/labels) to find existing feature proposals and bugs
   - If raising a new issue always provide a relevant label and a link to the relevant ticket in Zendesk
1. [ ] Take a look at the existing [issue templates](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/CONTRIBUTING.md#issue-tracker) to see what is expected

<!-- End General Customer Success Bootcamp -->

<!-- Start Solution Architects Bootcamp for ONLY Solution Architects -->

#### For Solution Architects Only

You will be required to demo to team all demo scenarios specific to customer/prospect requirements in mock style showing business value.  Unless otherwise specified, follow these steps for each scenario outlined below:

1. [ ] Set up a standby demo environment as described in the [Handbook](https://about.gitlab.com/handbook/customer-success/solutions-architects/#demo-readiness). Request project access from project owners as needed.
1. [ ] Start a new Zoom meeting with just yourself
1. [ ] Click record to start recording your session
1. [ ] Share your screen via Zoom and give the presentation
1. [ ] Upload the results to [this Google Drive folder](https://drive.google.com/drive/u/0/folders/1EIN5iZnTWVUvWOsdCNkINIWOBfPGe4Bz) and tag your manager on a comment to this issue with the link to your uploaded video

**Goal:** Final certification as an SA

1. [ ] Sales role play ([demo scenarios](https://docs.google.com/document/d/1kSVUNM4u6KI8M9FxoyiUbHEHAHIi34iiY25NhMxLucc/edit))
1. [ ] Comparison of GitLab editions (key differentiators and business drivers)
1. [ ] Ecosystem and how GitLab interacts/compete with other tools
   - Examples: [GitHub.com](https://www.youtube.com/watch?v=ZdmDJFPNQuI&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP&index=1), [Atlassian](https://www.youtube.com/watch?v=-JRab22h9Dg&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP&index=5), [Integration with JIRA](https://www.youtube.com/watch?v=o7pnh9tY5LY&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP&index=3)
1. [ ] Demo of GitLab CI/CD (deep dive)
   - Follow the steps outlined [on the demo page](https://about.gitlab.com/handbook/marketing/product-marketing/demo/)
   - Examples: [CI/CD demo 1](https://drive.google.com/open?id=0B3MA-pZf8fAYMDFBVEtzOEtueTQ)
1. [ ] Presentation on [Idea to Production](https://about.gitlab.com/handbook/marketing/product-marketing/demo/)
   - Follow the steps outlined [on the demo page](https://about.gitlab.com/handbook/marketing/product-marketing/demo/#idea-to-production)
   - Examples: [I2P Demo from Product](https://drive.google.com/open?id=0B11_uo4faBnbWFlqMkVKMS1SVnc), [SA I2P Demos](https://drive.google.com/drive/folders/0B11_uo4faBnbTXpQWDBLak05QkE)
1. [ ] Interactive demo paired with another SA

<!-- Start Implementation Engineering Bootcamp for ONLY IEs -->

#### For Implementation Engineers only

1. [ ] Shadow/pair with a [Support team member](https://about.gitlab.com/team/) (appropriate to timezone) on 3-5 calls

##### Advanced Technical Topics

1. [ ] Backup omnibus using our [Backup rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
1. [ ] Install GitLab via [Docker](https://docs.gitlab.com/ce/install/docker.html)
1. [ ] Restore backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#restore-a-previously-created-backup)
1. [ ] Starting a [rails console](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#invoking-rake-tasks)
1. [ ] Learn about environment Information and [maintenance checks](http://docs.gitlab.com/ce/raketasks/maintenance.html)
1. [ ] Learn about the [GitLab check](http://docs.gitlab.com/ce/raketasks/check.html) rake command
1. [ ] Learn about GitLab Omnibus commands (`gitlab-ctl`)
   1. [ ] [GitLab Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status)
   1. [ ] [Starting and stopping GitLab services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping)
1. [ ] Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings
1. [ ] Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)

**Custom Integrations**

**Goal:** Understand how developers and customers can produce custom integrations to GitLab

If you want to integrate with GitLab there are three possible paths you can take:
1. [ ] [Utilizing webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html) - If you want to reflect information from GitLab or initiate an action based on a specific activity that occurred on GitLab you can utilize the existing infrastructure of our webhooks. To read about setting up webhooks on GitLab visit this page.
1. [ ] [API integration](https://docs.gitlab.com/ee/api/README.html) - If you're seeking a richer integration with GitLab, the next best option is to utilize our ever-expanding API. The API contains pretty much anything you'll need, however, if there's an API call that is missing we'd be happy to explore it and develop it.
1. [ ] [Project Services](https://docs.gitlab.com/ce/user/project/integrations/project_services.html) - Project services give you the option to build your product into GitLab itself. This is the most advanced and complex method of integration but is by far the richest. It requires you to add your integration to GitLab's code base as a standard contribution. If you're thinking of creating a project service, the steps to follow are similar to any contribution to the GitLab codebase.

**Migrations**

**Goal:** Understand how migrations from other Version Control Systems to Git & GitLab work

1. [ ] [SVN](https://git-scm.com/book/en/v1/Git-and-Other-Systems-Git-and-Subversion)
1. [ ] Learn how to [migrate from SVN to Git](https://docs.gitlab.com/ee/workflow/importing/migrating_from_svn.html)

##### Learn Terraform

1. [ ] Terraform single instance of GitLab into AWS (write your own Terraform).
  - Get AWS login from your manager
1. [ ] Terraform HA configuration into AWS (use existing Terraform).
1. [ ] Terraform HA+GEO configuration into AWS (use existing Terraform).
  - [ ] Setup cloudwatch dashboard for monitoring.
  - [ ] Create and run SAST project.
  - [ ] Setup code quality CI job (with code climate engine).
1. [ ] Terraform single instance into GCP (write your own Terraform).
  - [ ] Configure auto-devops to k8s cluster in GCP (write Terraform to create k8s cluster).
  - [ ] Create and run DAST project (use auto-devops and k8s cluster you created).
  - [ ] Setup DNS for k8s “production” cluster.
  - [ ] Setup auto-monitoring for app deployed to k8s cluster.
1. [ ] Helm deploy new cloud native containers into GCP.
1. [ ] Fix broken instance (mix of SG and misconfiguration scenarios).
  - [ ] Bad DB connection.
  - [ ] Bad Redis connection.
  - [ ] Bad NFS permissions.
  - [ ] Worker not connecting.
  - [ ] Bad certificate.
  - [ ] Can’t upload to registry.
  - [ ] Elasticsearch not indexing.
  - [ ] Repository is corrupt.

<!-- Start Technical Account Managers Bootcamp for ONLY TAMs -->

#### Technical Account Managers only

1. [ ] Shadow/pair with a [Support team member](https://about.gitlab.com/team/) (appropriate to timezone) on 3-5 calls
1. [ ] Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings
1. [ ] Learn about environment Information and [maintenance checks](http://docs.gitlab.com/ce/raketasks/maintenance.html)
1. [ ] Learn about the [GitLab check](http://docs.gitlab.com/ce/raketasks/check.html) rake command
1. [ ] Learn about GitLab Omnibus commands (`gitlab-ctl`)
   1. [ ] [GitLab Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status)
   1. [ ] [Starting and stopping GitLab services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping)
