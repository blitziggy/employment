Welcome to your onboarding issue! We are so excited you're here! As a new GitLab Independent Contractor, you will use GitLab as part of your job, and if you come from a non-technical background, this might be new to you. At times the onboarding issue might feel overwhelming, but if you have any questions along the way, please feel free to ask [People Ops](https://about.gitlab.com/handbook/people-operations/) in [Slack](https://gitlab.slack.com/archives/peopleops), your manager, or anyone else at GitLab in the [#questions channel](https://gitlab.slack.com/archives/questions) on [Slack](https://slack.com/), our chat communication tool. If you have any questions or suggestions about onboarding, please feel free to create an issue on the [People Ops issue tracker](https://gitlab.com/gitlab-com/people-ops/General/issues) or a merge request for [this template](https://gitlab.com/gitlab-com/people-ops/employment/edit/master/.gitlab/issue_templates/onboarding.md) and assign it to People Ops to make this documentation better for the next new team member (you will learn more about how to do that as you progress through this issue!).

The onboarding issue is broken out into what your ("New team member") responsibilities are, what your manager's ("Manager") responsibilities are, and what our People Experience team's ("People Experience") responsibilities are. Just focus on "New Team Member," and don't feel you need to rush through it or complete each and every task on the required day. If you can't move forward with your onboarding tasks because your assigned People Experience team member, your manager, or your buddy haven't checked tasks they are responsible for, don't hesitate to send them a reminder!

Take your time to work through this onboarding issue, and remember to practice the GitLab values of [iteration](https://about.gitlab.com/handbook/values/#iteration) and [efficiency](https://about.gitlab.com/handbook/values/#efficiency) by [taking action](https://about.gitlab.com/handbook/values/#bias-for-action) to fix missing or confusing information in the handbook, and [making a proposal](https://about.gitlab.com/handbook/values/#make-a-proposal) when you have ideas to make it easier to get started at GitLab.

**Note:** To provide timeline guidance, the onboarding issue is broken up by Days 1-5 and Days 6-30 (Weeks 2-4). It is critical that the Morning tasks listed on Days 1-5 are completed as soon as possible and in order. It may take longer than 5 days to complete these tasks, but we hope all team members can focus to complete them within 7-8 days. Do not feel pressured to complete all tasks for one particular day by the end of that day, there is flexibility and we do not want our team members to be overwhelmed. For the tasks in the Days 6-30 section, there is more flexibility to complete them as you complete your first 30 days at GitLab.

**Note:** In addition to general onboarding, you may have Department- or role-specific tasks to complete in parallel. See the section on [Job Specific Tasks](#job-specific-tasks) at the end of this issue to see your tasks (if applicable).


---

### Before Starting at GitLab

<details>
<summary>Manager</summary>

1. [ ] Manager: Select an onboarding buddy on the new employee's team.  Please try to select someone in a similar time zone, and someone who has been at GitLab for at least 3 months.
1. [ ] Manager: Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the onboarding issue.
1. [ ] Manager: The default laptop for the new team member will be an Apple MacBook Pro. If the new team member could benefit from using a Dell with Linux instead, remind the new employee to request it. Details and the appropriate information on laptops are included in the "Welcome" email sent to the new employee from the CES team after the team member's offer is signed, but many are overwhelmed with paperwork and new job excitement, so a friendly reminder might be in order. Refer to the handbook for more information on [approved laptops](https://about.gitlab.com/handbook/business-ops/it-ops-team/#laptop-configurations).
1. [ ] Manager: Schedule a Zoom video call with the new team member for the start of their first day to welcome them to the team and set expectations. Send them an invitation to both their personal email and GitLab email, as it is possible that they will not have created their Zoom account linked to their GitLab email by the time of this scheduled meeting.
1. [ ] Manager: Send an email to the new employee's personal email address welcoming them to GitLab. Provide them with the time and Zoom link to the introduction video call as well as any other information the manager feels will help with a great first day experience.
1. [ ] Manager: Update/add new [vacancy entry, if applicable, or add entry to team page](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#add-blank-entry-to-team-page-) (temporary slug, type, first name + initial, start date, title, and use `../gitlab-logo-extra-whitespace.png` for the picture). For story, add "Joining Month Xth".
1. [ ] Manager: Organize smooth onboarding with clear starting tasks and pathway for new team member.
1. [ ] Calendars & Agenda
   1. [ ] Manager: Invite team member to recurring team meetings.
   1. [ ] Manager: Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.

</details>


<details>
<summary>Buddy</summary>

1. [ ] Buddy: Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/general-onboarding/onboarding-buddies/) to get familiar with your role.
1. [ ] Buddy: Ask Manager for the new team member's personal email by email (not in this issue). Managers are cc'd when the Candidate Experience Team send the new team member a Welcome email to their personal email address once the agreement is signed.
1. [ ] Buddy: Schedule a zoom call for their first week to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc. Send them an invitation to both their personal email and GitLab email, as it is possible that they will not have created their Zoom account linked to their GitLab email by the time of this scheduled meeting.
1. [ ] Buddy: Schedule at least one follow-up chat on their second week. During that chat, offer to help them with updating the team page. Check with the new team member if they will need help setting up their SSH key and installing a Shell ready for using Git locally. You may need to set up an additional meeting for this step.

</details>


<details>
<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/itops : Order the new team member's laptop. If you are not able to ship a laptop to a specific country, let the new team member know and initiate discussion of purchase and expensing. (*NOTE* laptop orders can take up to 2-3+ weeks, especially in remote areas).

</details>


<details>
<summary>People Experience</summary>

1. [ ] People Experience: Check that Manager is accurate and confirm it is `MANAGER_HANDLE`, and People Experience is tackled by `PEOPLE_EXPERIENCE_HANDLE`. Assign this issue to the People Experience team member and the hiring manager (this should be done automatically, but worth checking).
1. [ ] Google account
   1. [ ] People Experience: Log on to the [Google Admin console](https://admin.google.com/gitlab.com/AdminHome?pli=1&fral=1#UserList:org=49bxu3w3zb11yx) and verify `first initial last name[at]gitlab.com` is not conflicting with a current account. Click the + sign to create an account for the new team member. Do not send access to new team member.
   1. [ ] People Experience: Inform the manager that Google account has been created by commenting in this issue with the new email address.
1. [ ] Calendars & Agenda
   1. [ ] People Experience: Add new team member to the next monthly [CEO 101 call](https://about.gitlab.com/culture/gitlab-101/) in the GitLab Team Meetings calendar. Also invite new team member to the "CEO 101 Introductions" meeting that takes place right before it. When prompted to "Edit recurring event", choose "This event". If the new team member is in APAC, please add to the APAC-timed 101 Introductions and Call held at longer intervals than the regular 101 calls. When prompted to "Edit recurring event", choose "This event".
   1. [ ] People Experience: Invite the new team member to the next occurring Security 101 call. When prompted to "Edit recurring event", choose "This event".
   1. [ ] People Experience: Invite the new team member to the next Onboarding Office Hours.  When prompted to "edit recurring event", choose "this event"
1. [ ] People Experience: Invite the new team member's GitLab email address to the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members) as a `Developer`.
1. [ ] People Experience: Invite the new team member's GitLab email address to the [gitlab-org group](https://gitlab.com/groups/gitlab-org/-/group_members) as a `Developer`.
1. [ ] People Experience: Modify the onboarding issue due date for 30 days after the new hire's start date (I.E. January 1 start date would make the due date February 1). 
1. [ ] People Experience: Ensure that job-specific onboarding tasks are included after Days 5-30 and delete any remaining job-specific tasks that do not apply.
1. [ ] People Experience: Consider scheduling the new team member's welcome [onboarding email](https://gitlab.com/gitlab-com/people-group/employment/blob/master/.gitlab/issue_templates/onboarding_email.md) ahead of time (see People Experience task # 2 in `Day 1: Accounts and Paperwork` below for details). Remember to edit out the `Okta` section of the template, as independent contractors do not have Okta.

</details>





---

### Day 1: Accounts and Access

<details>
<summary>People Experience</summary>

1. [x] People Experience: Schedule or send GSuite GitLab email account information to new team member's personal email address.
1. [x] People Experience: Prepare a brief welcome email that sets expectations for the new team member's first day using the [onboarding email template](https://gitlab.com/gitlab-com/people-group/employment/blob/master/.gitlab/issue_templates/onboarding_email.md). Send to the new team member's personal email address and copy their GitLab email address and People Experience. The welcome email needs to arrive in the morning of the new team member's start date before they begin to guide them through onboarding. You may either [schedule the email ahead of time](https://support.google.com/mail/answer/9214606?co=GENIE.Platform%3DDesktop&hl=en)or send it early in the morning of their start date. Remember to edit out the `Okta` section of the template, as independent contractors do not have Okta.
1. [x] People Experience: Request signature of Code of Conduct document from HelloSign.
1. [ ] People Experience: In 1Password, mark new team member as Active/Confirmed from Pending.

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Create a new Access Request issue. This is to request anything NOT provided by [baseline entitlements for all GitLab team members](https://about.gitlab.com/handbook/engineering/security/#baseline-entitlements-all-gitlab-team-members). For example, not everyone gets a Salesforce login, this is where you would request a login for Salesforce if you need to.
    - Go to [Access Requests](https://gitlab.com/gitlab-com/access-requests) to see if there is already a role-based AR template for your new team member's role
         * **Note: Pre-configured and pre-approved role-based Access Request (AR) templates have been created for several roles in Engineering, IT Ops, People Operations, Sales, and Security. Please use these pre-approved AR templates if one exists for your new team member's role.**
    - For all other roles that do not have a role-specific AR template, open a [Single Person Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request).
1. [ ] Manager: Set new GitLab team member's GitLab group and project-level permissions as needed.
1. [ ] Manager: Introduce your new team member to your team at your next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.

</details>




<details>
<summary>Accounts Payable</summary>

1. [ ] Accounts Payable (@tsaunders):  Send the independent contractor an onboarding request in Tipalti so they can create an account and supply their tax information to ensure we supply them with a 1099 at the end of the year. 

</details>





---


### Day 1 - Morning : Getting Started : Accounts and Paperwork

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Assign the onboarding issue to yourself by adding yourself to the current team members currently assigned by clicking Edit in the Assignees section (right-hand column) and enter your GitLab handle.
##### Security Tasks
1. [ ] New team member: Watch the recorded [New Hire Security Orientation](https://www.youtube.com/watch?v=kNc9f-LH6pU) training.
1. [ ] New team member: Complete the [survey/feedback](https://docs.google.com/forms/d/1_jUbA961Mj44paTfiI2VFTMsElAdjEqFsLrfPKfoW04) form for the Security Orientation training.
1. [ ] New team member: Read our [Security Practices](https://about.gitlab.com/handbook/security) page, especially our Password Policy Guidelines.
##### 1Password
1. [ ] New team member: Install 1Password. GitLab provides all team members with an account to 1Password. Some teams use 1Password for shared notes and some shared accounts.
   1. [ ] New team member: Register on 1Password by clicking on the link in the ["Link to 1Password Sign-Up"](https://docs.google.com/document/d/15BdqpNhSi_5bc2b9KXo_w5f0gC-3gmuw-aIgzRcbnbk/edit) Google Doc. You can find this (and other) shared Google Docs by using [`Search for files shared with gitlab.com`](https://support.google.com/a/answer/3187967?hl=en) in Google Drive.
   1. [ ] New team member: Install the 1Password app on your computer, and link it to your team account, as described on the [security practices page](https://about.gitlab.com/handbook/security/#adding-the-gitlab-team-to-a-1password-app). Please let People Experience or your manager know if you need any assistance.
   1. [ ] New team member: Read 1Password's [getting started guides](https://support.1password.com/explore/extension/) and the [Mac app](https://support.1password.com/getting-started-mac/) to understand its functionality.
   1. [ ] New team member: Ensure that your 1Password master key is unique and random. Start learning it!
   1. [ ] New team member: Before saving passwords in 1Password. Remember to save your passwords in your [*Private* vault](https://support.1password.com/1password-com-items/) (only accessible to you) rather than the *Team* vault (accessible to others at GitLab).
   1. [ ] New team member: Create a secure password for your laptop lock screen.

##### 2FA (Two-Factor Authentication)
1. [ ] New team member: GitLab requires you to enable 2FA (2-Step Verification, also known as two-factor authentication), because it adds an extra layer of security to your account. You sign in with something you know (your password) and something you have (a code you can copy from your Virtual Multi-Factor authentication Device, like FreeOTP, 1Password or Google Authenticator). Please make sure that time is set automatically on your device (ie. on Android: "Settings" > "Date & Time" > "Set automatically"). If you have problems with 2FA just let IT Ops know in the #it_help Slack channel! We can disable it for you to then set it up again. If the problem persists, then we can direct you to one of our super admins. Reach out proactively if you're struggling - it's better than getting locked out and needing to wait for IT Ops to help you get back online.
1. [ ] New team member: Do the next 3 steps Today. Actually -- why not RIGHT NOW. This absolutely must be done within 48 hours or you will be locked out and need to wait for IT Ops to get around to helping you get reconnected.
   1. [ ] New team member: Enable 2FA on your GitLab email account (Gmail/GSuite) (this should have been an option to do when you first received the invitation to the account). Google does not support 2FA in some countries ([e.g. Nigeria](https://productforums.google.com/forum/#!topic/gmail/3_wcd8tAqdc) ); if that is the case for you, reach out to IT Ops in the #it_help Slack channel to get the initial authentication code sent to a phone in a supported country, after which 2FA will work as normal. Please comment in this issue when this is completed.
   1. [ ] New team member: If you authenticated your GitLab.com account with Google, GitHub, etc. you should either disconnect them or make sure they use two-factor authentication.
   1. [ ] New team member: Enable [two-factor authentication](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) on your GitLab.com account.
   1. [ ] New team member: If you had a GitLab.com account before joining GitLab, remove all SSH keys that may give access to your account from machines or systems at previous employers or other organizations that should not have access to GitLab IP.


</details>

### Day 1 - Afternoon : Getting Started : Your Role

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Spend the second half of your first day starting on your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-base onboarding issue, as applicable).

</details>

---

### Day 2 - Morning : General Tasks

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Particularly if you're new to working in a 100% remote environment, read over our [Guide for starting a new remote role](https://about.gitlab.com/company/culture/all-remote/getting-started/). You'll find other tips for operating (and thriving!) in an all-remote setting within the [All-Remote section of our handbook](https://about.gitlab.com/company/culture/all-remote/). Take some inspiration from our team members on how they structure their remote working day, read this help blog post [A day in the life of remote worker](https://about.gitlab.com/blog/2019/06/18/day-in-the-life-remote-worker/)
1. [ ] New team member: The first month at a remote company can be hard. Take a look at [3 Things I Learned in My First Month at GitLab](https://about.gitlab.com/2016/11/02/three-things-i-learned-in-my-first-month-at-gitlab/) for some insight from a GitLab team member.
1. [ ] New team member: Onboarding at GitLab and our ongoing workplace methodologies are highly unique. Acclimate by reading this [guide on adopting a self-service and self-learning mentality](https://about.gitlab.com/company/culture/all-remote/self-service/).
1. [ ] New team member: GitLab values are a living document. In many instances, they have been documented, refined, and revised based on lessons learned (and scars earned) in the course of doing business. Familiarize yourself with our [Values page](/handbook/values/).
   1. [ ] Take and pass our [Values Certification](https://docs.google.com/forms/d/e/1FAIpQLSfuIrMYDMS14OZo8zqMnkk6yiKDhzcnhH7f82aHTOMWHS5uvg/viewform) to become certified in our GitLab Values. 
1. [ ] New team member: Clear, considerate communication is especially important at an all-remote company. Familiarize yourself with our [Communication page](https://about.gitlab.com/handbook/communication/) in the handbook, particularly the [Internal Communication](https://about.gitlab.com/handbook/communication/#internal-communication) and [asynchronous communication](https://about.gitlab.com/company/culture/all-remote/management/#asynchronous) sections, as they will help you to better communicate with your coworkers.
   1. [ ] Take and pass our [Communication Certification](https://docs.google.com/forms/d/e/1FAIpQLScyyIQDdC3oN-H6-IJJM1QlZNHaGPI0jESb9ogAfkQlMzKgwQ/viewform) to become certified in our GitLab Communication. 
1. [ ] New team Member: Read our handbook section on [Google Calendar](https://about.gitlab.com/handbook/tools-and-tips/#google-calendar)
1. [ ] New team Member: Set your Google Calendar [default event duration](https://calendar.google.com/calendar/r/settings) to use `speedy meetings`
1. [ ] New team Member: Consider adding the [GitLab team member Meetup](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV85cWZnajRuMm9nZHMydmlhMDFrb3ZoaGpub0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) calendar to your calendars.
1. [ ] New team Member: Verify you have access to view the GitLab Team Meetings calendar, and add the calendar. Go to your calendar, left sidebar, go to Other calendars, press the + sign, Subscribe to calendar, and enter in the search field `gitlab.com_6ekbk8ffqnkus3qpj9o26rqejg@group.calendar.google.com`. Reach out to a People Experience Associate if you have any questions.
1. [ ] New team member: Set up [Calendly](https://about.gitlab.com/handbook/tools-and-tips/#calendly). Calendly is a calendar tool that allows individuals to select open meeting slots in order to speak with others outside of GitLab. For GitLab team members, feel free to [schedule directly in Google Calendar](https://about.gitlab.com/handbook/communication/#scheduling-meetings). When you are setting up Calendly, there is no specific GitLab account. With the free version you will only be allowed one meeting time, but if you need to upgrade to a Pro account, you can do so and expense it per [spending company money](https://about.gitlab.com/handbook/spending-company-money).
1. [ ] New team member: [Set your working hours & availability](https://support.google.com/calendar/answer/7638168?hl=en) in your Google Calendar.
1. [ ] New team member: Link your GitLab email address to an easily recognizable photo of yourself on [Gravatar](https://en.gravatar.com/). It is company policy to use a photo, and not an avatar, a stock photo, or something with sunglasses for your GitLab accounts, as we have a lot of GitLab team members and our brains are comfortable with recognizing people; let's use them.
1. [ ] Slack
   1. [ ] New team member: Using your @gitlab.com email, register on Slack by following this [invitation link](https://join.slack.com/t/gitlab/signup). Read the next suggestions on how to choose a username first.
   1. [ ] New team member: Pick your [Slack username](https://gitlab.slack.com/account/settings#username) to be the same as your GitLab email handle, for consistency and ease of use.
   1. [ ] New team member: Fill in your [Slack profile](https://gitlab.slack.com/account/profile), as we use Slack profiles as our Team Directory to stay in touch with other team members. The fields include:
      1. [ ] Photo (if you set up a Gravatar for your email address Slack will pull it automatically at registration or a few hours after)
      1. [ ] What I Do (can be your job title or something informative about your role)
      1. [ ] Phone Number including country code
      1. [ ] Time Zone (useful for other GitLab team members to see when you're available)
      1. [ ] GitLab.com profile
      1. [ ] Calendly link
      1. [ ] Job Description link from GitLab.com
      1. [ ] City and country
      1. [ ] If you'd like to, your Personal Email Address i.e. not your GitLab email address (this is optional)
      1. [ ] Consider changing your ["display name"](https://get.slack.help/hc/en-us/articles/216360827-Change-your-display-name) if you prefer to be addressed by a nickname
      1. [ ] Consider creating and adding your own [README](https://about.gitlab.com/handbook/marketing/website/#creating-and-publishing-your-gitlab-readme). This may take some time to create so please feel free to return to this task and complete it at a later date. 
   1. [ ] New team member: Read our handbook section on [communication via Slack](https://about.gitlab.com/handbook/communication/#slack), paying particular attention to the item concerning `Please avoid using @here or @channel unless this is about something urgent and important`.
   1. [ ] New team member: Introduce yourself in the Slack [#new_labbers](https://gitlab.slack.com/messages/new_labbers/) channel, where you can ask any questions you have and get to know other new team members! Make sure your Slack profile has a **photo** - it makes easier for other GitLab team members to remember you! Also consider adding a photo with your introduction message to give your new team members a glimpse into your world (scroll through previous messages in the channel for inspiration).
   1. [ ] New team member: Complete the 8 short Slack lessons found at [Slack 101](https://slack.com/resources/slack-101).
   1. [ ] New team member: Check out some of GitLab's [key Slack channels](https://about.gitlab.com/handbook/communication/#key-slack-channels) to get started. You can also reference the Slack help center for [instructions on browsing all available channels](https://slack.com/help/articles/205239967-Join-a-channel); we have thousands!
1. [ ] New team member: [View](https://drive.google.com/drive/folders/0B4eFM43gu7VPUXBUa251RHFrUnM) and save your [Traveler Insurance ID card](https://about.gitlab.com/handbook/benefits/#general-benefits).
1. [ ] New team member: Set up [Zoom account](https://about.gitlab.com/handbook/tools-and-tips/#zoom). Your Zoom account will be set up via Okta for SSO Login, so login using the Zoom SSO tile.
   1. [ ] New team member: Fill in your [profile](https://zoom.us/profile) with your Name, Job Title and Profile Picture. Since Zoom's job title field isn't displayed during calls, it is recommended to add your job title as part of your last name in the last name field. For example, if your name is `John Appleseed` and your role is `Engineer`, you can write first name: `John` and last name: `Appleseed - Engineer`.
   1. [ ] New team member: Consider setting your default recording view to "Gallery view". To do this:
      1. Go to zoom.us and log in.
      1. Click the Settings tab on the left, then the Recording tab on the right.
      1. Make sure you have `Record gallery view with shared screen` selected, and unselect `Record active speaker with shared screen` and `Record active speaker, gallery view and shared screen separately`. Remember to save.
   1. [ ] New team member: Set up the Zoom Scheduler plugin for [Chrome](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle/related?hl=en) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/zoom-new-scheduler/) - this will allow you to automatically add Zoom links to Google Calendar events.
   1. [ ] Or, if you're browser agnostic and you'd prefer not to use a plugin, there's [Zoom for Google Calendar](https://gsuite.google.com/marketplace/app/zoom_for_google_calendar/364750910244). Using the app integration you'll have video conferencing info natively in your event, rather than text/links in the description.
1. [ ] New team member: Please read the [anti-harassment policy](https://about.gitlab.com/handbook/anti-harassment/) and in particular any country specific requirements that relate to your location.  Remember to complete the harassment prevention training via the invite sent to you in an email from WILL Learning.

</details>

### Day 2 - Afternoon : Your Role

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Spend the second half of your second day working on your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-base onboarding issue, as applicable).

</details>

---

### Day 3 - Morning : Security

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Make sure your notifications for GitLab.com aren't sent to a personal email. Send them to your `@gitlab.com` email. GitLab.com is viewable to anyone with an account, and we ask you to set up your GitLab.com account with your GitLab email for additional security.
1. [ ] New team member: Consider enabling notifications about your own activity (helpful for visibility in later Git steps). To enable this, go to Settings -> Notifications -> Check the box before "Receive notifications about your own activity"
1. [ ] New team member: If you are already in possession of a company owned Apple laptop for use in your new role at Gitlab, please create or update the Apple ID, using your GitLab email address. 
1. [ ] New team member: Encrypt your hard drive. On Macs this may be with FileVault (see [security practices](https://about.gitlab.com/handbook/security) for help and more details).
1. [ ] New team member: Leave a comment in this issue with a screenshot verifying that your hard drive is encrypted (on Mac, `Command` + `Shift` + `3` for the entire screen or `Command` + `Shift` + `4` to decide which area you'd like to screenshot). To do this, find the `Attach a file` link at the bottom of this issue and press Comment. Please be sure that the screenshot includes your machine's serial number as well as evidence of disk encryption for correlation.  Depending on the operating system you are using, the steps to retrieve this information may be different.  Please review the [Full Disk Encryption](https://about.gitlab.com/handbook/business-ops/it-ops-team/#full-disk-encryption) page in the handbook for the appropriate steps to create the screenshot with the necessary information. Please note that GitLab's [Internal Acceptable Use Policy](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/#unacceptable-system-and-network-activities) prohibits the use of tools that capture and share screenshots to hosted third-party sites so please make sure that you do not leverage such a tool to complete this step.
1. [ ] New team member: Please add your serial code for your GitLab laptop here: [GitLab Notebook information](https://docs.google.com/forms/d/e/1FAIpQLSfk54_BgOj9h_PuH6NnJNF2hGf22Db8edvFCkJyiO-iDCxmjA/viewform?usp=sf_link). You will need the evidence of full disk encryption from the previous step.
1. [ ] New team member: For contractors, it may be useful to save an invoice template for future use. An invoice template can be found in Google Docs by searching `Invoice Template` or [here](https://docs.google.com/spreadsheets/d/1CxJMQ06GK_DCqihVaZ0PXxNhumQYzgG--nw_ibPV0XA/edit#gid=0).
1. [ ] New team member: Review and sign the [Code of Business Conduct & Ethics Acknowledgment Form](https://about.gitlab.com/handbook/people-operations/code-of-conduct/#code-of-business-conduct--ethics-acknowledgment-form). This document was sent to your GitLab email inbox from HelloSign requesting your signature. Please comment in this issue and ping your assigned People Experience team member if you have not received it.
1. [ ] New team member: Don't forget to comply with the contract you signed, and make sure you understand [Intellectual Property](https://about.gitlab.com/handbook/people-operations/code-of-conduct/#intellectual-property-and-protecting-ip).
1. [ ] New team member: Check out the [People Operations handbook page](https://about.gitlab.com/handbook/people-group/) and learn more about the People team and everything under our purview.
1. [ ] New team member: Set-up and familiarize yourself with our apps: [Gmail](https://mail.google.com/), [Google Calendar](https://www.google.com/calendar/), [Slack](https://gitlab.slack.com/messages/general/) and [Google Drive](https://www.google.com/drive/) where you can [download](https://tools.google.com/dlpage/drive/index.html?hl=en) to work offline.
  1. [ ] New team member: Consider setting up an email signature. [Here is an example you can use](https://about.gitlab.com/handbook/tools-and-tips/#sts=Email%20signature). Feel free to customize it how you'd like.
  1. [ ] New team member: Be aware your Google Calendar (tied to your GitLab Google account) is internally viewable by default. If you have private meetings often, you might want to [change this](https://support.google.com/calendar/answer/34580?co=GENIE.Platform%3DDesktop&hl=en) in your calendar settings. However, even private meetings are viewable by your manager and those who manage them.
  1. [ ] New team member: Review [GitLab's internal acceptable use policy](https://about.gitlab.com/handbook/people-operations/acceptable-use-policy/)
1. [ ] New team member: Introduce yourself to your Breakout Group after the company call. Please tell the group about where you were before GitLab, why you wanted to join our team, where you are located, and what you like to do for fun outside of work.
1. [ ] New team member: Review the [Tools and Tips section of the handbook](https://about.gitlab.com/handbook/tools-and-tips/). Many of the tools used by GitLab, including tips on how to use them, care found there.
1. [ ] New team member: Update your LinkedIn profile to reflect your new position with GitLab.
1. [ ] New team member: We use TripActions for company travel; please [create your account here](https://tripactions.com/signup ). Head to the [travel page](https://about.gitlab.com/handbook/travel/#setting-up-your-tripactions-account) for further instructions on how to setup your account.
1. [ ] New team member: If you think you might have questions getting your work station set up, try to sign up within 2 weeks for [Gitlab IT Onboarding/101](https://calendar.google.com/event?action=TEMPLATE&tmeid=M3VpajRiNTdmYXM3ZnViZDI1MGVnaHFvdTRfMjAxOTEyMDNUMTcwMDAwWiBtZGlzYWJhdGlub0BnaXRsYWIuY29t&tmsrc=mdisabatino%40gitlab.com&scp=ALL). This is an optional meeting for you to ask questions. We run this live troubleshooting session weekly, so you can always drop into a later meeting.
1. [ ] New team member: If you had a GitLab.com account before joining GitLab, remove all SSH keys that may give access to your account from machines or systems at previous employers or other organizations that should not have access to GitLab IP.

</details>

### Day 3 - Afternoon : Your Role

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Spend the second half of this day working on your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-base onboarding issue, as applicable).

</details>

---

### Day 4 - Morning : Social

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Schedule at least 5 calls in your first week of 30 mins with at least 5 different colleagues to get to know the GitLab team. You can enter your colleagues' names below by clicking the pencil icon at the top right of this issue (under `New Issue`) and editing this section. We encourage you to schedule calls with people who are not in your department or geographical area, so that you can learn more about other teams and cultures. You can view GitLab team members by role and team on the [team structure chart](https://about.gitlab.com/team/chart/). Please feel free to reach out to People Experience or your manager if you are unsure of who to schedule with. Try to schedule 1 call each day for your first week, but feel free to spread them out over your first month, or even schedule more! It is generally not recommended to do all the coffee chats in one day. You can also join the [#donut-be-strangers](https://gitlab.slack.com/messages/C613ZTNEL/) channel in Slack, which will automatically set you up on one random [coffee break call](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) every other week. There's no need to ask people if it's okay to schedule a call first – just go ahead and schedule a meeting using the "Find a time" feature in Google Calendar and introduce yourself in the invitation.
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] optional call with an engineer to get going with Git
1. [ ] New team member: Check the [GitLab Contribute handbook page](https://about.gitlab.com/company/culture/contribute/) and [project page](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/README.md) to get familiar with the GitLab Contribute event.
1. [ ] New team member: If applicable, make sure to register for the next GitLab Contribute and make travel arrangements (using TripActions) within 2 weeks of starting. You will receive an invite to do so over email.
1. [ ] New team member: Fill in your [GitLab.com profile](https://gitlab.com/profile), including: Organization = "GitLab", Bio = "(title)". Given our value of [transparency](https://about.gitlab.com/handbook/values/#transparency), ensure your do *not* use a [private profile](https://docs.gitlab.com/ee/user/profile/#private-profile), i.e. make sure that checkbox under **Private profile** is unchecked.
1. [ ] New team member: Connect with or follow GitLab's social media sites: [LinkedIn](https://www.linkedin.com/company/gitlab-com), [Twitter](https://twitter.com/gitlab), [Facebook](https://www.facebook.com/gitlab), and [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg).
1. [ ] New team member: Accept the invitation to be a Manager of the GitLab Unfiltered YouTube channel. If you do not see an invitation in your Inbox, please check the [Pending Invitations](https://myaccount.google.com/brandaccounts) section of your GSuite account.
1. [ ] New team member: You should receive an email inviting you to Moo. Moo is the system we use to place [business cards](https://about.gitlab.com/handbook/people-group/#business-cards). Follow handbook directions for more detailed information.

</details>

### Day 4 - Afternoon : Your Role

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Spend the second half of this day working on your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-base onboarding issue, as applicable).

</details>

---

### Day 5 - Morning : Git

<details>
<summary>New Team Member</summary>

For those new to Git and GitLab, it's important to get familiar with the below and [bookmark these links](https://about.gitlab.com/company/culture/all-remote/self-service/). You're not expected to become a GitLab master in a day, but to understand that the below is a key reference as you acclimate.

1. [ ] New team member: Read this informative blog post titled [GitLab 101 - A Primer for the Non-Technical](https://about.gitlab.com/2019/08/02/gitlab-for-the-non-technical/).
1. [ ] New team member (may be optional): If you have never used Git before, we recommend you skip this step and use the WebIDE to complete any Git-related onboarding tasks. The WebIDE doesn't require you to set or use and command-line tools. If you plan to use Git on your local machine then you'll need to [add an SSH key to your GitLab profile](https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account). The SSH key provides security and allows you to authenticate to the GitLab remote server without supplying your username or password each time. Please note that following best practices, you should always favor ED25519 SSH keys, since they are more secure and have better performance over the other types.

##### Technical Git Information
1. [ ] New team member: Become familiar with how GitLab works by watching the [videos for git newbies](https://about.gitlab.com/training/#git) and familiarizing yourself with other available training content.
1. [ ] New team member: Become aware of the [materials used by our professional services team](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/offerings/#training--education) to train our customers.
1. [ ] New team member: Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] New team member: Become familiar with how Gitlab works by learning our [GitLab Basics](http://doc.gitlab.com/ce/gitlab-basics/README.html).
1. [ ] New team member: Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] New team member: Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally.  The rest of the team is happy to assist in teaching you git.
1. [ ] New team member: If you need any general help with Git, ask in the [`#questions`](https://gitlab.slack.com/archives/questions) channel in Slack. For questions with Merge Requests, ask in the [`#mr-buddies`](https://gitlab.slack.com/archives/mr-buddies) channel. With specific questions using Git in the terminal, subscribe to the [`#git-help`](https://gitlab.slack.com/archives/git-help) channel on Slack and feel free to ask any questions you might have.
1. [ ] New team member: Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.
1. [ ] New team member: Consider requesting a Zoom video call with your buddy or any other GitLab team member to complete any local git training.
1. [ ] New team member: Reach out to your onboarding buddy, or any other GitLab team member to show you how to do some advanced git tasks locally. For example, configure and/or [solve a merge conflict](https://about.gitlab.com/handbook/git-page-update/#how-to-solve-a-broken-pipeline-in-a-merge-request).
##### Issues and Issue Trackers
1. [ ] New team member: Learn how to use [issue trackers](https://docs.gitlab.com/ee/user/project/issues/). We use GitLab Issues to raise awareness, discuss, and propose solutions for various issues related to any aspect of our business. The most common issues are created in the following projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/issues) - Issues related to our website
   * [GitLab Project](https://gitlab.com/gitlab-org/gitlab/issues) - Customer requests and public issues related to GitLab product
##### Merge Requests
1. [ ] New team member: When you are first starting at GitLab and creating new merge requests, you can assign the MRs to your manager. Going forward, it's best practice to assign the MR to the relevant department; for example, if you are making an edit to the People Experience section of the handbook, please assign it to someone within People Experience. If you're unsure of who to assign a merge request to or if you have any questions or problems, feel free to reach out to your manager or onboarding buddy. You can also ask in the [`#questions`](https://gitlab.slack.com/archives/questions) channel in Slack! When you join GitLab your GitLab.com account will be added to the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members) as a Developer. Please see this [blog post discussing how we assign permissions](https://about.gitlab.com/2014/11/26/keeping-your-code-protected/), as well as a [permission levels chart](https://docs.gitlab.com/ee/user/permissions.html) to find out more about what each level can do.
1. [ ] New team member: Review best practices for issues and merge requests. It is important that we make iterations quickly and efficiently. Do not wait until you are done before creating an issue or merge request. Making issues and merge requests quickly and in small iterations allows the information to be quickly updated in the handbook and available for review by everyone. It also reduces the risk of changes becoming lost or out of date. `WIP` (Work in Progress) is what you prepend to a merge request in GitLab in order to prevent your work from being merged if it still needs more work. When you are working on a big issue, create a merge request after the very first commit and give it a title, starting with `WIP`, so you can iterate on it. For more information regarding issues, merge requests, and overall best practices regarding communication, please review [the communication guidelines](https://about.gitlab.com/handbook/communication/).
1. [ ] New team member: Make an improvement to the [handbook](https://about.gitlab.com/handbook/) or [onboarding issue](https://gitlab.com/gitlab-com/people-group/employment/-/blob/master/.gitlab/issue_templates/onboarding.md), assign the merge request (MR) to your onboarding buddy (if they have merge rights; if not, assign to your manager), ping them in the #new_labbers slack channel, and link the MR url in this onboarding issue. When you are making changes in the handbook, you are making changes in the www-gitlab-com project. If your manager does not have access to merge your MR based on the permissions described above, then please reference the specific [members page](https://gitlab.com/gitlab-com/www-gitlab-com/project_members?page=3&sort=access_level_desc) for the www-gitlab-com project. Here at GitLab, everyone contributes to our way of working in this way. Being new, you may be unsure if your idea for a change is good or not, and that's OK! Your merge request starts the discussion, so don't be afraid to offer your perspective as a new team member.

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Check that new team member has successfully added themselves to the team page.

</details>

### Day 5 - Afternoon : Your Role

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Spend the second half of this day working on your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-base onboarding issue, as applicable).

</details>

---

### Weeks 2 - 4 : Explore

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Be sure to spend equal time on this critical onboarding issue, as well as your role-based onboarding and training.
1. [ ] New team member: Read the team [handbook](https://about.gitlab.com/handbook/). A core value of GitLab is documentation. Therefore, everything that we do, we have documented in the handbook. This is NOT a traditional company handbook of rules and regulations. It is a living document that is public to the world and constantly evolving. Absolutely anyone, including you, can suggest improvements or changes to the handbook! It's [encouraged](https://about.gitlab.com/handbook/handbook-usage/)! The handbook may seem overwhelming, but don't let it scare you. To simplify navigating the handbook, here are some suggested steps. Feel free to take a wrong turn at any time to learn more about whatever you are interested in.
   - [ ] Read [about](https://about.gitlab.com/about/) the company, and [how we use GitLab to build GitLab](https://about.gitlab.com/2015/07/07/how-we-use-gitlab-to-build-gitlab/). It is important to understand our [culture](https://about.gitlab.com/culture/), and how the organization was started. If you have any questions about company products you can always check out our [features](https://about.gitlab.com/features/#compare) and [products](https://about.gitlab.com/products/).
   - [ ] Make sure to read over our [values](https://about.gitlab.com/handbook/values) to answer any questions you might have about what to do and when. GitLab values should be a part of the everyday work process for all GitLab team members.
   - [ ] If you have questions about what is in the handbook check out the [handbook usage](https://about.gitlab.com/handbook/handbook-usage) as well as your own department's sub-page of the handbook (which can be found at the top of the handbook).
   - [ ] GitLab's Value Framework is foundational to GitLab's go-to-market messaging. We want to ensure all GitLab team members (even those outside of sales) are aware of what it is, why it is so important to our continued growth and success, and what it means to our customers, partners, and GitLab team members. Learn more in [this virtual session recording](http://bit.ly/37FqLC4) and the [Command of the Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) handbook page.
   - [ ] Since we are a global organization understand your benefits might be different than other team members at the company based on which [contract](https://about.gitlab.com/handbook/contracts/) you signed. It is important to understand **your** [benefits](https://about.gitlab.com/handbook/benefits/). If you have questions please reach out to People Experience.
   - [ ] [No ask time off policy](https://about.gitlab.com/handbook/paid-time-off): GitLab truly does value a work-life balance, and encourages team members to have a flexible schedule and take vacations. If you feel uncomfortable about taking time off, or are not sure how much time to take off throughout the year, feel free to speak with your manager or People Experience. We will be happy to reinforce this policy! Please note the additional steps that might need to be taken if you are scheduled for [on call](https://about.gitlab.com/handbook/on-call).
   - [ ] As part of onboarding, you received an email asking if you needed any equipment. Throughout your time at GitLab you might need additional equipment, would like to learn how to code, or want to visit a team member! Make sure you know what you can [spend company money](https://about.gitlab.com/handbook/spending-company-money) on.
   - [ ] The handbook also describes [incentives](https://about.gitlab.com/handbook/incentives) such as sales, dinners, or bonuses, that can apply to all GitLab team members. We'd love it if you [referred great talent](https://about.gitlab.com/handbook/incentives/#referral-bonuses)!
   - [ ] Even if you come from a technical background, it is important to understand the [proper workflow](https://about.gitlab.com/handbook/communication/#everything-starts-with-a-merge-request) since this is how we are all able to operate remotely in a successful way. If you come from a non-technical background, and would like more clarification on what this workflow means, feel free to ask People Experience, your manager, or anyone else at the company.
1. [ ] New team member: Take a look at GitLab's [Diversity & Inclusion page](https://about.gitlab.com/company/culture/inclusion/) and the [project](https://gitlab.com/gitlab-com/diversity-and-inclusion/issues). Optionally, you can contribute by collaborating or creating a new issue.
1. [ ] New team member: Take a look at GitLab's [public customer list](https://about.gitlab.com/customers/) to learn about our list of advertised customers.
1. [ ] New team member: Take a look at GitLab's [product tier use-cases](https://about.gitlab.com/handbook/ceo/pricing/per-tier-use-cases/) and [product marketing tiers](https://about.gitlab.com/handbook/marketing/product-marketing/tiers/) to learn about GitLab product capabilities, tiers, and the motivation behind each tier.
1. [ ] New team member: While we [avoid acronyms](https://about.gitlab.com/handbook/product/#how-to-submit-a-new-issue) typically so that Everyone Can Contribute, there are times that the easiest way to achieve mutually exclusive, ubiquitous language (referred to as [MECEFU terms](https://about.gitlab.com/handbook/communication/#mecefu-terms)) is through acronyms. When that is the case, you should define the acronym in context first before it's use. For example, "All Three Letter Acronyms (TLA) should be defined first to avoid confusion."
1. [ ] New team member: Read about GitLab [stock options](https://about.gitlab.com/handbook/stock-options/). If you received stock options as part of your contract, your options will be approved by the Board of Directors at their quarterly board meetings. After your grant has been approved by the Board you will receive a grant notice by email.
1. [ ] New team member: Bookmark [GitLab Handbook ChangeLog](https://about.gitlab.com/handbook/CHANGELOG.html) as our handbook is constantly growing and changing this is a useful link to stay up to date with all recent changes!
1. [ ] New team member: Set a calendar reminder to answer the GitLab quiz questions in the first 2 weeks of working in the company. If the answers are not accurate, you may retest: [GitLab Quiz](https://about.gitlab.com/handbook/questions/). Please schedule a call with your manager to discuss.
1. [ ] New team member: Explore how to best setup your inbox using [GitLab filters](https://www.youtube.com/watch?v=YOgm-vZVqng)
1. [ ] New team member: After you have been at GitLab for 30 days, kindly review [the NPS section](https://about.gitlab.com/handbook/people-group/#nps-surveys) in the handbook and complete this [survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform). Kindly do make time to complete it, it is vitally important for us to continue getting feedback and iterate on onboarding.
1. [ ] New team member: Help spread the word about life at GitLab. Share your initial feedback by leaving a review on [Glassdoor](https://www.glassdoor.com/Overview/Working-at-GitLab-EI_IE1296544.11,17.htm) or [Comparably](https://www.comparably.com/companies/gitlab).

</details>

---