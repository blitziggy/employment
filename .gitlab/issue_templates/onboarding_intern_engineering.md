Welcome to your onboarding issue! We are so excited you're here! As a new GitLab team member, you will use GitLab as part of your job, and if you come from a non-technical background, this might be new to you. At times the onboarding issue might feel overwhelming, but if you have any questions along the way, please feel free to ask on Slack, our chat communication tool. You can find the most appropriate channel for your issue [here](https://about.gitlab.com/handbook/communication/chat/)

If you have any questions or suggestions about onboarding, please feel free to create an issue on the [People Ops issue tracker](https://gitlab.com/gitlab-com/people-group/General/issues) or a merge request for [this template](https://gitlab.com/gitlab-com/people-group/employment/edit/master/.gitlab/issue_templates/onboarding.md) and assign it to a People Operations Specialist or Manager to make this documentation better for the next new team member (you will learn more about how to do that as you progress through this issue!).

The onboarding issue is broken out into what your ("New team member") responsibilities are, what your manager's ("Manager") responsibilities are, and what our People Ops team's ("People Ops") responsibilities are. Just focus on "New Team Member," and don't feel you need to rush through it or complete each and every task on the required day. If you can't move forward with your onboarding tasks because People Ops, your manager, your mentor, or your buddy haven't checked tasks they are responsible for, don't hesitate to send them a reminder!

Take your time to work through this onboarding issue, and remember to practice the GitLab values of [iteration](https://about.gitlab.com/handbook/values/#iteration) and [efficiency](https://about.gitlab.com/handbook/values/#efficiency) by [taking action](https://about.gitlab.com/handbook/values/#bias-for-action) to fix missing or confusing information in the handbook, and [making a proposal](https://about.gitlab.com/handbook/values/#make-a-proposal) when you have ideas to make it easier to get started at GitLab.

**Note:**
In addition to general onboarding, you may have Department- or role-specific tasks to complete once you are done with your Day 1-5 tasks. See the section on [Job Specific Tasks](#job-specific-tasks) after the Day 5 section to see your tasks (if applicable).

---

### Before Starting at GitLab

<details>
<summary>Manager</summary>

1. [ ] Manager: Select an the onboarding buddy.  
1. [ ] Manager: Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the onboarding issue.
1. [ ] Manager: Select the mentor for the internship: this mentor will be providing extensive guidance throughout the internship.
1. [ ] Manager: The default laptop for the new team member will be an Apple MacBook Pro. If the new team member could benefit from using a Dell with Linux instead, remind the new employee to request it. Details and the appropriate information on laptops are included in the "Welcome" email sent to the new employee from the CES team after the team member's offer is signed, but many are overwhelmed with paperwork and new job excitement so a friendly reminder might be in order. Refer to the handbook for more information on [approved laptops](https://about.gitlab.com/handbook/business-ops/it-ops-team/#laptop-configurations).
1. [ ] Manager: Schedule a video call using Zoom with the new team member at the start of their first day to welcome them to the team and set expectations.
1. [ ] Manager: Send an email to the new employee's personal email address welcoming them to GitLab. Provide them with the time and Zoom link to the introduction video call as well as any other information the manager feels will help with a great first day experience.
1. [ ] Manager: Update [vacancy entry or add entry to team page](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#add-blank-entry-to-team-page-) (temporary slug, type, first name + initial, start date, title, and use `../gitlab-logo-extra-whitespace.png` for the picture). For story, add "Joining Month Xth". 
1. [ ] Manager: Organize smooth onboarding with clear starting tasks / pathway for new team member.
1. [ ] Calendars & Agenda
   1. [ ] Manager: Invite team member to recurring team meetings.
   1. [ ] Manager: Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.

</details>


<details>
<summary>Buddy</summary>

1. [ ] Buddy: Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/general-onboarding/onboarding-buddies/) to get familiar with your role. 
2. [ ] Buddy: Review the handbook page about the [Engineering internship pilot] (https://about.gitlab.com/handbook/engineering/internships/)
1. [ ] Buddy: Schedule a zoom call for their first week to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc.
1. [ ] Buddy: Schedule at least one follow-up chat on their second week. During that chat, offer to help them with updating the team page. Check with the new team member if they will need help setting up their SSH key and installing a Shell ready for using Git locally. You may need to set up an additional meeting for this step.
</details>

<details>
<summary>Mentor</summary>

1. [ ] Mentor: Review the handbook page about the [Engineering internship pilot] (https://about.gitlab.com/handbook/engineering/internships/)
1. [ ] Mentor: Follow the training on mentoring an intern. Aligned with: https://gitlab.com/gitlab-com/engineering-internships/issues/9
1. [ ] Mentor: Schedule a zoom call for their first day to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc.
1. [ ] Mentor: Make sure to schedule regular (bi-weekly/weekly) calls to help the intern with questions and removing anything that blocks them. 
</details>


<details>
<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/itops : Order the new team member's laptop. If you are not able to ship a laptop to a specific country, let the new team member know and initiate discussion of purchase and expensing. (*NOTE* laptop orders can take up to 2-3+ weeks, especially in remote areas)

</details>


<details>
<summary>People Ops</summary>

1. [ ] People Ops: Check that Manager is accurate and confirm it is `__MANAGER_HANDLE__`, and People Ops is tackled by `__PEOPLE_EXPERIENCE_HANDLE__`. Assign this issue to the People Ops team member and the hiring manager (this should be done automatically, but worth checking).
1. [ ] Google account
   1. [ ] People Ops: Log on to the [Google Admin console](https://admin.google.com/gitlab.com/AdminHome?pli=1&fral=1#UserList:org=49bxu3w3zb11yx) and verify `first initial last name[at]gitlab.com` is not conflicting with a current account. Click the + sign to create an account for the new team member. Do not send access to new team member.
   1. [ ] People Ops: Inform the manager that Google account has been created by commenting in this issue with the new email address.
1. [ ] People Ops Information
   1. [ ] People Ops: Find the new team member's profile in [BambooHR](https://gitlab.bamboohr.com/home) and input [relevant data](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#adding-a-new-team-member-to-bamboohr) into the GitLab team member's profile including and up to Stock options on this handbook page.
   1. [ ] People Ops: Check if new team member was referred and by whom in [Greenhouse](https://app2.greenhouse.io/dashboard). If it's not marked at the top of their Greenhouse profile, go to "Application" on the left hand sidebar under the job they were hired for, then click "View Job Post" and look in their application questions to see if they mentioned a referrer. Make sure the source for the candidate is labelled as referred and by the appropriate person.
   1. [ ] People Ops: If the new team member was referred, add a note under "Notes" in the team member's BambooHR profile with the name of who referred them. Also [add the referral bonus](https://about.gitlab.com/handbook/incentives/#referral-bonuses) for the referrer in BambooHR. Please verify based on the handbook what the amount should be depending on if the hire is in a low location area or underrepresented group.
   1. [ ] People Ops: If the new team member listed any exceptions to the IP agreement in their contract, check the applicable box under the Jobs tab. Email the VP of Engineering and VP of Product with the information to get approval.
   1. [ ] People Ops: If the new team member has any additional documents besides their contract, file them in their BambooHR profile under "Documents". Example documents are NDA+IP agreements, W9s, and approvals for exceptions to the IP Agreement. Set the uploaded documents to "shared" so that the team member can also view them.
   1. [ ] People Ops: If applicable, add a time off accrual plan for employees located in GitLab LTD (UK), GitLab GmbH (Germany), GitLab B.V. (Belgium), GitLab Inc (China), and GitLab B.V.(Netherlands). Click on "Time Off", hover your cursor over the "Employee Accruals" box, and you should see "Accrual Options" appear at the bottom. Click on it and select the appropriate policy from the dropdown. Click Save.
   1. [ ] People Ops: Enable "self-service" in BambooHR by clicking the orange gear at the top right, hovering over "BambooHR Access Level" and selecting "Employee Self-Service."
   1. [ ] People Ops (Analyst): 
        *  Audit the entry in BambooHR and mark complete in the "Payroll Change Report" in BambooHR. 
        *  Add new team member to the comp calc.
        *  Update access levels for Managers and Contractors, if applicable, as follows:
           * For Employees who are Managers of people: "Managers"
           * For Contractors (independent or corp-to-corp): "Contractor Self-Service"
           * For Contractors who are Managers of people: "Multiple Access Levels": "Contractor Self-Service" and "Managers"
1. [ ] Calendars & Agenda
   1. [ ] People Ops: If applicable, invite the new team member to the APAC/EMEA Company Call in the GitLab Team Meetings calendar. When prompted, choose "edit all events".
   1. [ ] People Ops: Add new team member to the next monthly [GitLab 101 call](https://about.gitlab.com/culture/gitlab-101/) in the GitLab Team Meetings calendar. Also invite new team member to the "GitLab 101 Introductions" meeting that takes place right before it. When prompted to "Edit recurring event", choose "This event". If the new team member is in APAC, please add to the APAC-timed 101 Introductions and Call held at longer intervals than the regular 101 calls. When prompted to "Edit recurring event", choose "This event".
   1. [ ] People Ops: Invite the new team member to the next occurring Security 101 call. When prompted to "Edit recurring event", choose "This event".
   1. [ ] People Ops: Invite the new team member to the next Onboarding Office Hours.  When prompted to "edit recurring event", choose "this event"
1. [ ] People Ops: Invite the new team member's GitLab email address to the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members) as a `Developer`.
1. [ ] People Ops: Invite the new team member's GitLab email address to the [gitlab-org group](https://gitlab.com/groups/gitlab-org/-/group_members) as a `Developer`.

</details>


<!-- include: before_starting_country -->



---

### Day 1: Accounts and Paperwork

<details>
<summary>People Ops</summary>

1. [ ] People Ops: Schedule or send GSuite GitLab email account information to new team member's personal email address.
1. [ ] People Ops: Schedule or send brief welcome email to their personal email address that sets expectations for the first day using the [onboarding email template](https://gitlab.com/gitlab-com/people-group/employment/blob/master/.gitlab/issue_templates/onboarding_email.md). Send to new team member's personal email address and copy their GitLab email address and People Ops.
1. [ ] People Ops: Move the scanned photo of ID to the Verification Folder in BambooHR.  Important: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Ops: Send [New Hire Swag email](https://gitlab.com/gitlab-com/people-group/employment/blob/master/.gitlab/issue_templates/swag_email.md) to new team member.
1. [ ] People Ops: Add new user as a Manager to the [GitLab Unfiltered YouTube account](https://myaccount.google.com/u/3/brandaccounts/103404719100215843993/view?pli=1) by clicking "Manage Permissions" and entering their GitLab email address.
1. [ ] People Ops: Invite team member to [Moo](https://www.moo.com/m4b/account/login) by having the People Ops Specialist DRI [update the GitLab : Moo spreadsheet](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#onboarding-issue-tasks) for our Moo rep.
1. [ ] People Ops: In 1Password, mark new team member as Active/Confirmed from Pending.

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New team member: Assign the onboarding issue to yourself.
#### Security Tasks
1. [ ] New team member: Watch the recorded [New Hire Security Orientation](https://www.youtube.com/watch?v=kNc9f-LH6pU) training.
1. [ ] New team member: Complete the [survey/feedback](https://docs.google.com/forms/d/1_jUbA961Mj44paTfiI2VFTMsElAdjEqFsLrfPKfoW04) form for the Security Orientation training.
1. [ ] New team member: Read our [Security Practices](https://about.gitlab.com/handbook/security) page, especially our Password Policy Guidelines.
#### 1Password
1. [ ] New team member: Install 1Password. GitLab provides all team members with an account to 1Password. Some teams use 1Password for shared notes and some shared accounts, but these are in the process of being incorporated into Okta.
   1. [ ] New team member: Register on 1Password by clicking on the link in the ["Link to 1Password Sign-Up"](https://docs.google.com/document/d/15BdqpNhSi_5bc2b9KXo_w5f0gC-3gmuw-aIgzRcbnbk/edit) Google Doc. You can find this (and other) shared Google Docs by using [`Search for files shared with gitlab.com`](https://support.google.com/a/answer/3187967?hl=en) in Google Drive.
   1. [ ] New team member: Install the 1Password app on your computer, and link it to your team account, as described on the [security practices page](https://about.gitlab.com/handbook/security/#adding-the-gitlab-team-to-a-1password-app). Please let People Ops or your manager know if you need any assistance.
   1. [ ] New team member: Read 1Password's [getting started guides](https://support.1password.com/explore/extension/) and the [Mac app](https://support.1password.com/getting-started-mac/) to understand its functionality.
   1. [ ] New team member: Ensure that your 1Password master key is unique and random. Start learning it!
   1. [ ] New team member: Before saving passwords in 1Password, understand why some will be in 1Password versus Okta. For passwords that cannot be saved in Okta, remember to save your passwords in your [*Private* vault](https://support.1password.com/1password-com-items/) (only accessible to you) rather than the *Team* vault (accessible to others at GitLab).
   1. [ ] New team member: Create a login item in your 1Password private vault for your laptop login. Reset your laptop password to the random one generated by 1Password and start learning it!
#### Okta
1. [ ] New team member: Set up your Okta Account. GitLab uses Okta as its primary portal for all SaaS Applications, and you should already have an activation email in both your Gmail and Personal Accounts. Okta will populate your Dashboard with many of the Applications you will need, but some of these will require activation. Read and Review the [Okta Handbook](https://about.gitlab.com/handbook/business-ops/okta/) page for more information.
   1. [ ] New team member: Change your account on GitLab.com to use your @gitlab.com email account if you had a GitLab account before joining the company.
   1. [ ] New team member: Reset your Gitlab password via Okta and make sure you use Okta to log into your GitLab account.
   1. [ ] New team member: Reset your Google password via Okta and set up 2FA for your Google account immediately.
   1. [ ] New team member: If you created any accounts while onboarding before being added to Okta, add these passwords to the relevant Okta Application.
   1. [ ] New team member: Reset any additional existing passwords in line with the security training & guidelines.
   1. [ ] New Team member: Confirm in the comment box that you are using 1Password and Okta in accordance with our Security Practices.
#### 2FA (Two-Factor Authentication)
1. [ ] New team member: GitLab requires you to enable 2FA (2-Step Verification, also known as two-factor authentication), because it adds an extra layer of security to your account. You sign in with something you know (your password) and something you have (a code you can copy from your Virtual Multi-Factor authentication Device, like FreeOTP, 1Password or Google Authenticator). Please make sure that time is set automatically on your device (ie. on Android: "Settings" > "Date & Time" > "Set automatically"). If you have problems with 2FA just let IT Ops know in the #it_help Slack channel! We can disable it for you to then set it up again. If the problem persists, then we can direct you to one of our super admins. Reach out proactively if you're struggling - it's better than getting locked out and needing to wait for IT Ops to help you get back online.
1. [ ] New team member: Do the next 3 steps Today. Actually -- why not RIGHT NOW. This absolutely must be done within 48 hours or you will be locked out and need to wait for IT Ops to get around to helping you get reconnected.
   1. [ ] New team member: Enable 2FA on your GitLab email account (Gmail/GSuite) (this should have been an option to do when you first received the invitation to the account). Google does not support 2FA in some countries ([e.g. Nigeria](https://productforums.google.com/forum/#!topic/gmail/3_wcd8tAqdc) ); if that is the case for you, reach out to IT Ops in the #it_help Slack channel to get the initial authentication code sent to a phone in a supported country, after which 2FA will work as normal. Please comment in this issue when this is completed.
   1. [ ] New team member: If you authenticated your GitLab.com account with Google, GitHub, etc. you should either disconnect them or make sure they use two-factor authentication.
   1. [ ] New team member: Enable [two-factor authentication](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) on your GitLab.com account.
   1. [ ] New team member: If you had a GitLab.com account before joining GitLab, remove all SSH keys that may give access to your account from machines or systems at previous employers or other organizations that should not have access to GitLab IP.
#### BambooHR
1. [ ] New team member: BambooHR is our HRIS (Human Resource Information System) for all team members. We have self-service enabled so that at any time you can access your account to see your employment information and documentation. As part of onboarding, please make sure to update everything that is applicable to you. The information from BambooHR is used to feed other systems, so it is important that it is filled out before you start.
   1. [ ] New team member: Access BambooHR from your Okta account dashboard.
   1. [ ] New team member: Please upload a scanned photo of your ID/Passport and Work Permit/Visa in BambooHR (My Info => Documents => Employee Uploads). If your ID documents have already been uploaded by People Ops (check the Verification Docs folder), please disregard this task. If no document(s) are there, please upload and notify People Ops by commenting at the bottom of this issue, tagging your People Ops Specialist assigned to your issue by entering their GitLab handle and your message.
   1. [ ] New team member: On the Personal tab of the My Info page, enter
      1. [ ] Birth Date: This field is mandatory and is confidential, viewable only to People Operations Admins.
      1. [ ] Preferred Name: Only fill out this field if you prefer to be known by a name other than your legal first name. Ex. If your legal first name is Emily but you prefer to be known as Emma, please enter Emma in the Preferred Name field. If you go by Emily, leave the preferred field blank.
      1. [ ] National Identification Number
      1. [ ] Gender
      1. [ ] Marital Status
      1. [ ] Address
      1. [ ] Nationality
      1. [ ] Phone numbers (make sure to add the country code, i.e. all numbers should start with `+`)
   1. [ ] New team member: On the Job tab, enter your ethnicity. If you would prefer not to answer, please leave it blank.
   1. [ ] New team member: On the Emergency tab, enter in all emergency contact information.
   1. [ ] New team member: On the Training tab, click the title of the role appropriate Sexual Harassment Prevention training. You only need to complete one training based on your role (Manager or Employee) and location (California or non-California).  Follow the url to the vendor's training system (WILL Interactive) and click 'Sign Up Now'.  Create an account using your GitLab email and create a new password.  Complete the training within 30 days of hire and upload the completion certificate to your Employee Uploads folder in the Documents section of your BambooHR profile.
   1. [ ] New team member (only for GitLab IT BV Contractors): On the More tab, click Bank Information and fill in your bank details. Note that for all other GitLab team members, please follow the instructions as per the on-boarding issue.


</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Create a new Access Request issue. This is to request anything NOT provided by [baseline entitlements for all GitLab team members](https://about.gitlab.com/handbook/engineering/security/#baseline-entitlements-all-gitlab-team-members). For example, not everyone gets a Salesforce login, this is where you would request a login for Salesforce if you need to.
    - Go to [Access Requests](https://gitlab.com/gitlab-com/access-requests) to see if there is already a role-based AR template for your new team member's role 
         * **Note: Pre-configured and pre-approved role-based Access Request (AR) templates have been created for several roles in Engineering, IT Ops, People Ops, Sales, and Security. Please use these pre-approved AR templates if one exists for your new team member's role.**
    - For all other roles that do not have a role-specific AR template, open a [Single Person Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request).
1. [ ] Manager: Set new GitLab team member's GitLab group and project-level permissions as-needed.
1. [ ] Manager: Introduce your new team member to your team at your next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.

</details>

<details>
<summary>Payroll</summary>

1. [ ] Payroll (@nprecilla): If an applicable policy exists for the employee type and entity, [invite team member](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#add-expensify) to an account in [Expensify](https://www.expensify.com/signin). If contractor, search to see if there is an `iiPay` policy in the currency of the team member's compensation, and invite to that policy if it exists. If none currently exists, create a new policy and the new team member to that policy.

</details>


---


<!-- include: country_tasks -->


---


<!-- include: entity_tasks -->


---


### Day 1: General Tasks

<details>
<summary>New Team Member</summary>

1. [ ] New team member: The first month at a remote company can be hard. Take a look at [3 Things I Learned in My First Month at GitLab](https://about.gitlab.com/2016/11/02/three-things-i-learned-in-my-first-month-at-gitlab/) for some insight from a GitLab team member.
1. [ ] New team member: Onboarding at GitLab and our ongoing workplace methodologies are highly unique. Acclimate by reading this [guide on adopting a self-service and self-learning mentality](https://about.gitlab.com/company/culture/all-remote/self-service/).
1. [ ] New team member: Clear, considerate communication is especially important at an all-remote company. Familiarize yourself with our [Communication page](https://about.gitlab.com/handbook/communication/) in the handbook, particularly the [Internal Communication](https://about.gitlab.com/handbook/communication/#internal-communication) and [asynchronous communication](https://about.gitlab.com/company/culture/all-remote/management/#asynchronous) sections, as they will help you to better communicate with your coworkers.
1. [ ] New team Member: Read our handbook section on [Google Calendar](https://about.gitlab.com/handbook/tools-and-tips/#google-calendar)
1. [ ] New team Member: Set your Google Calendar [default event duration](https://calendar.google.com/calendar/r/settings) to use `speedy meetings`
1. [ ] New team Member: Consider adding the [GitLab team member Meetup](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV85cWZnajRuMm9nZHMydmlhMDFrb3ZoaGpub0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) calendar to your calendars.
1. [ ] New team Member: Verify you have access to view the GitLab Team Meetings calendar, and add the calendar. Go to your calendar, left side bar, go to Other calendars, press the + sign, Subscribe to calendar, and enter in the search field `gitlab.com_6ekbk8ffqnkus3qpj9o26rqejg@group.calendar.google.com`. Reach out to a People Operations Specialist if you have any questions.
1. [ ] New team member: Set up [Calendly](https://about.gitlab.com/handbook/tools-and-tips/#calendly). Calendly is a calendar tool that allows individuals to select open meeting slots in order to speak with others outside of GitLab. For GitLab team members, feel free to [schedule directly in Google Calendar](https://about.gitlab.com/handbook/communication/#scheduling-meetings). When you are setting up Calendly, there is no specific GitLab account. With the free version you will only be allowed one meeting time, but if you need to upgrade to a Pro account, you can do so and expense it per [spending company money](https://about.gitlab.com/handbook/spending-company-money).
1. [ ] New team member: [Set your working hours & availability](https://support.google.com/calendar/answer/7638168?hl=en) in your Google Calendar.
1. [ ] New team member: Link your GitLab email address to an easily recognizable photo of yourself on [Gravatar](https://en.gravatar.com/). It is company policy to use a photo, and not an avatar, a stock photo, or something with sunglasses for your GitLab accounts, as we have a lot of GitLab team members and our brains are comfortable with recognizing people; let's use them.
1. [ ] Slack
   1. [ ] New team member: Using your @gitlab.com email, register on Slack by following this [invitation link](https://join.slack.com/t/gitlab/signup). Read the next suggestions on how to choose a username first.
   1. [ ] New team member: Pick your [Slack username](https://gitlab.slack.com/account/settings#username) to be the same as your GitLab email handle, for consistency and ease of use.
   1. [ ] New team member: Fill in your [Slack profile](https://gitlab.slack.com/account/profile), as we use Slack profiles as our Team Directory to stay in touch with other team members. The fields include:
      1. [ ] Photo (if you set up a Gravatar for your email address Slack will pull it automatically at registration or a few hours after)
      1. [ ] What I Do (can be your job title or something informative about your role)
      1. [ ] Phone Number including country code
      1. [ ] Time Zone (useful for other GitLab team members to see when you're available)
      1. [ ] GitLab.com profile
      1. [ ] Calendly link
      1. [ ] Job Description link from GitLab.com
      1. [ ] City and country
      1. [ ] If you'd like to, your Personal Email Address i.e. not your GitLab email address (this is optional)
      1. [ ] Consider changing your ["display name"](https://get.slack.help/hc/en-us/articles/216360827-Change-your-display-name) if you prefer to be addressed by a nickname
   1. [ ] New team member: Read our handbook section on [communication via Slack](https://about.gitlab.com/handbook/communication/#slack), paying particular attention to the item concerning `Please avoid using @here or @channel unless this is about something urgent and important`.
   1. [ ] New team member: Introduce yourself in the Slack [#new_labbers](https://gitlab.slack.com/messages/new_labbers/) channel, where you can ask any questions you have and get to know other new team members! Make sure your Slack profile has a **photo** - it makes easier for other GitLab team members to remember you! Also consider adding a photo with your introduction message to give your new team members a glimpse into your world (scroll through previous messages in the channel for inspiration).
   1. [ ] New team member: Complete the [Slack course using Foundry app](https://slackfoundry.builtbyslack.com/#install) which covers how to use Slack and best practices. Instead of Step 1, use the "Jump to..." search prompt to search for "Slack Foundry" and click it.  Then start from the `Step 2 "Start a direct message with Slack Foundry."` because Foundry is already added to GitLab Slack team.
1. [ ] New team member: [View](https://drive.google.com/drive/folders/0B4eFM43gu7VPUXBUa251RHFrUnM) and save your [Traveler Insurance ID card](https://about.gitlab.com/handbook/benefits/#general-benefits).
1. [ ] New team member: Set up [Zoom account](https://about.gitlab.com/handbook/tools-and-tips/#zoom). Your Zoom account will be set up via Okta for SSO Login, so login using the Zoom SSO tile.
   1. [ ] New team member: Fill in your [profile](https://zoom.us/profile) with your Name, Job Title and Profile Picture. Since Zoom doesn't have a job title field, it is recommended to add your job title as part of your last name in the last name field. For example, if your name is `John Appleseed` and your role is `Engineer`, you can write first name: `John` and last name: `Appleseed - Engineer`.
   1. [ ] New team member: Consider setting your default recording view to "Gallery view". To do this:
      1. Go to zoom.us and log in.
      1. Click the Settings tab on the left, then the Recording tab on the right.
      1. Make sure you have `Record gallery view with shared screen` selected, and unselect `Record active speaker with shared screen` and `Record active speaker, gallery view and shared screen separately`. Remember to save.
   1. [ ] New team member: Set up the Zoom Scheduler plugin for [Chrome](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle/related?hl=en) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/zoom-new-scheduler/) - this will allow you to automatically add Zoom links to Google Calendar events.
   1. [ ] Or, if you're browser agnostic and you'd prefer not to use a plugin, there's [Zoom for Google Calendar](https://gsuite.google.com/marketplace/app/zoom_for_google_calendar/364750910244). Using the app integration you'll have video conferencing info natively in your event, rather than text/links in the description.
1. [ ] New team member: Please read the [anti-harassment policy](https://about.gitlab.com/handbook/anti-harassment/) and in particular any country specific requirements that relate to your location.  Remember to complete the harassment prevention training via the invite sent to you in an email from WILL Learning.
1. [ ] New team member: Don't forget to comply with the contract you signed, and make sure you understand [Intellectual Property](https://about.gitlab.com/handbook/people-operations/code-of-conduct/#intellectual-property-and-protecting-ip).
1. [ ] New team member: Check out the [People Operations handbook page](https://about.gitlab.com/handbook/people-group/) and learn more about the People team and everything under our purview.
1. [ ] New team member: Set-up and familiarize yourself with our apps: [Gmail](https://mail.google.com/), [Google Calendar](https://www.google.com/calendar/), [Slack](https://gitlab.slack.com/messages/general/) and [Google Drive](https://www.google.com/drive/) where you can [download](https://tools.google.com/dlpage/drive/index.html?hl=en) to work offline.
  1. [ ] New team member: Consider setting up an email signature. [Here is an example you can use](https://about.gitlab.com/handbook/tools-and-tips/#sts=Email%20signature). Feel free to customize it how you'd like.
  1. [ ] New team member: Be aware your Google Calendar (tied to your GitLab Google account) is internally viewable by default. If you have private meetings often, you might want to [change this](https://support.google.com/calendar/answer/34580?co=GENIE.Platform%3DDesktop&hl=en) in your calendar settings. However, even private meetings are viewable by your manager and those who manage them.
  1. [ ] New team member: Review [GitLab's internal acceptable use policy](https://about.gitlab.com/handbook/people-operations/acceptable-use-policy/)
1. [ ] New team member: Introduce yourself to your Breakout Group after the company call. Please tell the group about where you were before GitLab, why you wanted to join our team, where you are located, and what you like to do for fun outside of work. 
1. [ ] New team member: If outside the US, fill in the [expression of wishes form](https://docs.google.com/document/d/1bBX6Mn5JhYuQpCXgM4mkx1BbTit59l0hD2WQiY7Or9E/edit?usp=sharing). To do this, first copy the template to your Google Drive (File -> Make a copy), enter your information, sign electronically. To sign the document, use a free document signing program like [smallpdf](https://smallpdf.com/sign-pdf) or [HelloSign](https://app.hellosign.com/); or you can print it, sign and digitize.  On a Mac you can electronically sign via exporting the document to PDF and then using [Preview](https://support.apple.com/guide/preview/fill-out-and-sign-pdf-forms-prvw35725/mac).  Sign, save as a pdf and upload to the Employee Uploads folder in BambooHR.
1. [ ] New team member: Review the [Tools and Tips section of the handbook](https://about.gitlab.com/handbook/tools-and-tips/). Many of the tools used by GitLab, including tips on how to use them, care found there.
1. [ ] New team member: Update your LinkedIn profile to reflect your new position with GitLab.
1. [ ] New team member: We use TripActions for company travel; please [create your account here](https://tripactions.com/signup ). Head to the [travel page](https://about.gitlab.com/handbook/travel/#setting-up-your-tripactions-account) for further instructions on how to setup your account.
1. [ ] New team member: If you think you might have questions getting your workstation setup, try to sign up within 2 weeks for [Gitlab IT Onboarding/101](https://calendar.google.com/event?action=TEMPLATE&tmeid=M3VpajRiNTdmYXM3ZnViZDI1MGVnaHFvdTRfMjAxOTEyMDNUMTcwMDAwWiBtZGlzYWJhdGlub0BnaXRsYWIuY29t&tmsrc=mdisabatino%40gitlab.com&scp=ALL). This is an optional meeting for you to ask questions. We run this live-troubleshooting session weekly, so you can always drop into a later meeting.

</details>



---

### Day 2: Security

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Make sure your notifications for GitLab.com aren't sent to a personal email. Send them to your `@gitlab.com` email. GitLab.com is viewable to anyone with an account, and we ask you to set up your GitLab.com account with your GitLab email for additional security.
1. [ ] New team member: Consider enabling notifications about your own activity (helpful for visibility in later Git steps). To enable this, go to Settings -> Notifications -> Check the box before "Receive notifications about your own activity"
1. [ ] New team member: If you are already in possesion of a company owned Apple laptop for use in your new role at Gitlab, please create or update the Apple ID, using your GitLab email address. If an app is critical to your work, it can be reimbursed through Expensify. If your laptop is of a different make, please assign any computer or store ID to your GitLab email address.
1. [ ] New team member: Encrypt your hard drive. On Macs this may be with FileVault (see [security practices](https://about.gitlab.com/handbook/security) for help and more details).
1. [ ] New team member: Leave a comment in this issue with a screenshot verifying that your hard drive is encrypted (on Mac, `Command` + `Shift` + `3` for the entire screen or `Command` + `Shift` + `4` to decide which area you'd like to screenshot). To do this, find the `Attach a file` link at the bottom of this issue and press Comment. Please be sure that the screenshot includes your machine's serial number as well as evidence of disk encryption for correlation.  Depending on the operating system you are using, the steps to retrieve this information may be different.  Please review the [Full Disk Encryption](https://about.gitlab.com/handbook/business-ops/it-ops-team/#full-disk-encryption) page in the handbook for the appropriate steps to create the screen shot with the necessary information.
1. [ ] New team member: Please add your serial code for your GitLab laptop here: [GitLab Notebook information](https://docs.google.com/forms/d/e/1FAIpQLSfk54_BgOj9h_PuH6NnJNF2hGf22Db8edvFCkJyiO-iDCxmjA/viewform?usp=sf_link). You will need the evidence of full disk encryption from the previous step.
1. [ ] New team member: For contractors, it may be useful to save an invoice template for future use. An invoice template can be found in Google Docs by searching `Invoice Template` or [here] (https://docs.google.com/spreadsheets/d/1CxJMQ06GK_DCqihVaZ0PXxNhumQYzgG--nw_ibPV0XA/edit#gid=0). 
1. [ ] New team member: Review and sign the [Code of Business Conduct & Ethics Acknowledgment Form](https://about.gitlab.com/handbook/people-operations/code-of-conduct/#code-of-business-conduct--ethics-acknowledgment-form). To do this, first copy the template to your Google Drive (File -> Make a copy), enter your information, sign electronically. To sign the document, you can print it, sign and digitalize, or use some free document sign like [smallpdf](https://smallpdf.com/sign-pdf). After having signed save as a pdf and upload to the Employee Uploads folder in BambooHR. Comment on this issue when complete.
1. [ ] New team member: If you had a GitLab.com account before joining GitLab, remove all SSH keys that may give access to your account from machines or systems at previous employers or other organizations that should not have access to GitLab IP.
 

</details>

---

### Day 3: Social & Project setup

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Fill in your [GitLab.com profile](https://gitlab.com/profile), including: Organization = "GitLab", Bio = "(title)". Given our value of [transparency](https://about.gitlab.com/handbook/values/#transparency), ensure your do *not* use a [private profile](https://docs.gitlab.com/ee/user/profile/#private-profile), i.e. make sure that checkbox under **Private profile** is unchecked. 
1. [ ] New team member: Connect with or follow GitLab's social media sites: [LinkedIn](https://www.linkedin.com/company/gitlab-com), [Twitter](https://twitter.com/gitlab), [Facebook](https://www.facebook.com/gitlab), and [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg).
1. [ ] New team member: Accept the invitation to be a Manager of the GitLab Unfiltered YouTube channel. If you do not see an invitation in your Inbox, please check the [Pending Invitations](https://myaccount.google.com/brandaccounts) section of your GSuite account.
1. [ ] New team member: You should receive an email inviting you to Moo. Moo is the system we use to place [business cards](https://about.gitlab.com/handbook/people-group/frequent-requests/#business-cards). Follow handbook directions for more detailed information.

</details>

---

### Day 4: Git & GDK

<details>
<summary>New Team Member</summary>

Git may be overwhelming at first. Please do not hesitate to reach out to a more experienced GitLab team member if you need assistance.  In addition, we host onboarding office hours every Thursday at 12:00 EST/EDT.
1. [ ] New team member: Read this informative blog post titled [GitLab 101 - A Primer for the Non-Technical](https://about.gitlab.com/2019/08/02/gitlab-for-the-non-technical/).
1. [ ] New team member: Become familiar with how GitLab works by watching the [videos for git newbies](https://about.gitlab.com/training/#git) and familiarizing yourself with other available training content. We have [materials used by our professional services team](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/offerings/#training--education) to train our customers, as well [on-demand training tracks](https://about.gitlab.com/training/) on different topics.
1. [ ] New team member: Update your slug, full name, photo, social media handles and story on the team page by following the instructions in [the handbook](https://about.gitlab.com/handbook/git-page-update/#11-add-yourself-to-the-team-page), or follow this video: [Adding Self to GitLab Team Page](https://www.youtube.com/watch?v=axF0qPqBEl0).  Please make sure to include your nickname when you do so if you prefer to be called anything other than your first name.  You can always reach out to your buddy, manager, or anyone in the company if you get stuck! For errors in the MR, feel free to post the MR link in [#mr-buddies](https://about.gitlab.com/handbook/general-onboarding/mr-buddies/) in Slack to get some help.
1. [ ] New team member: If you have a pet, please also consider adding them to the [team pets page](https://about.gitlab.com/team-pets/) by following the instructions on the [same page](https://about.gitlab.com/handbook/git-page-update/#11-add-yourself-to-the-team-page). Please ensure to add your first and last name.
1. [ ] New team member: Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] New team member: Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab. 
1. [ ] New team member: Learn how to use [issue trackers](https://docs.gitlab.com/ee/user/project/issues/). We use GitLab Issues to raise awareness, discuss, and propose solutions for various issues related to any aspect of our business. The most common issues are created in the following projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/issues) - Issues related to our website
   * [GitLab Project](https://gitlab.com/gitlab-org/gitlab/issues) - Customer requests and public issues related to GitLab product
   * [GitLab HQ](https://dev.gitlab.org/gitlab/gitlabhq) - Internal issues related to documentation and gitlab.com usage
   * [GitLab Organization](https://gitlab.com/gitlab-com/organization) - Issues related to the organization, consider making them confidential
   * Internal [GitLab EE](https://dev.gitlab.org/gitlab/gitlab-ee) - Internal issues relate to Enterprise Edition.
1. [ ] New team member: When you are first starting at GitLab and creating new merge requests, you can assign the MRs to your manager. Going forward, it's best practice to assign the MR to the relevant department; for example, if you are making an edit to the People Ops section of the handbook, please assign it to someone within People Ops. If you're unsure of who to assign a merge request to or if you have any questions or problems, feel free to reach out to your manager or onboarding buddy. You can also ask in the [`#questions`](https://gitlab.slack.com/archives/questions) channel in Slack! When you join GitLab your GitLab.com account will be added to the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members) as a Developer. Please see this [blog post discussing how we assign permissions](https://about.gitlab.com/2014/11/26/keeping-your-code-protected/), as well as a [permission levels chart](https://docs.gitlab.com/ee/user/permissions.html) to find out more about what each level can do.
1. [ ] New team member: Review best practices for issues and merge requests. It is important that we make iterations quickly and efficiently. Do not wait until you are done before creating an issue or merge request. Making issues and merge requests quickly and in small iterations allows the information to be quickly updated in the handbook and available for review by everyone. It also reduces the risk of changes becoming lost or out of date. `WIP` (Work in Progress) is what you prepend to a merge request in GitLab in order to prevent your work from being merged if it still needs more work. When you are working on a big issue, create a merge request after the very first commit and give it a title, starting with `WIP`, so you can iterate on it. For more information regarding issues, merge requests, and overall best practices regarding communication, please review [the communication guidelines](https://about.gitlab.com/handbook/communication/).
1. [ ] New team member: Accept the invitation to be a Manager of the GitLab Unfiltered YouTube channel. If you do not see an invitation in your Inbox, please check the [Pending Invitations](https://myaccount.google.com/brandaccounts) section of your GSuite account.
1. [ ] New team member: Make an improvement to the [handbook](https://about.gitlab.com/handbook/), assign the merge request (MR) to your onboarding buddy (if they have merge rights; if not, assign to your manager), ping them in the #new_labbers slack channel, and link the MR url in this onboarding issue. When you are making changes in the handbook, you are making changes in the www-gitlab-com project. If your manager does not have access to merge your MR based on the permissions described above, then please reference the specific [members page](https://gitlab.com/gitlab-com/www-gitlab-com/project_members?page=3&sort=access_level_desc) for the www-gitlab-com project. Here at GitLab, everyone contributes to our way of working in this way. Being new, you may be unsure if your idea for a change is good or not, and that's OK! Your merge request starts the discussion, so don't be afraid to offer your perspective as a new team member.
1. [ ] New team member: Become familiar with how Gitlab works by learning our [GitLab Basics](http://doc.gitlab.com/ce/gitlab-basics/README.html).
1. [ ] New team member: Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally.  The rest of the team is happy to assist in teaching you git.
1. [ ] New team member: If you need any general help with Git, ask in the [`#questions`](https://gitlab.slack.com/archives/questions) channel in Slack. For questions with Merge Requests, ask in the [`#mr-buddies`](https://gitlab.slack.com/archives/mr-buddies) channel. With specific questions using Git in the terminal, subscribe to the [`#git-help`](https://gitlab.slack.com/archives/git-help) channel on Slack and feel free to ask any questions you might have.
1. [ ] New team member: Consider requesting a Zoom video call with your buddy or any other GitLab team member to complete any local git training.
1. [ ] New team member: Reach out to your onboarding buddy, or any other GitLab team member to show you how to do some advanced git tasks locally. For example, configure and/or solve a merge conflict.
1. [ ] New team member: Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Check that new team member has successfully added themselves to the team page.
1. [ ] Manager: Share the new team member's Calendly link in team Slack channels or syncronous meetings that happen within your department. 

</details>

---

### Day 5: Setup for development environment



### Day 6 to 30: Explore

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Schedule at least 5 calls in your first week of 30 mins with at least 5 different colleagues to get to know the GitLab team. You can enter your colleagues' names below by clicking the pencil icon at the top right of this issue (under `New Issue`) and editing this section. We encourage you to schedule calls with people who are not in your department or geographical area, so that you can learn more about other teams and cultures. You can view GitLab team members by role and team on the [team structure chart](https://about.gitlab.com/team/chart/). Please feel free to reach out to People Ops or your manager if you are unsure of who to schedule with. Try to schedule 1 call each day for your first week, but feel free to spread them out over your first month, or even schedule more! It is generally not recommended to do all the coffee chats in one day. You can also join the [#donut-be-strangers](https://gitlab.slack.com/messages/C613ZTNEL/) channel in Slack, which will automatically set you up on one random [coffee break call](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) every other week. There's no need to ask people if it's okay to schedule a call first – just go ahead and schedule a meeting using the "Find a time" feature in Google Calendar and introduce yourself in the invitation.
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] optional call with an engineer to get going with Git 
1. [ ] New team member: Read the team [handbook](https://about.gitlab.com/handbook/). A core value of GitLab is documentation. Therefore, everything that we do, we have documented in the handbook. This is NOT a traditional company handbook of rules and regulations. It is a living document that is public to the world and constantly evolving. Absolutely anyone, including you, can suggest improvements or changes to the handbook! It's [encouraged](https://about.gitlab.com/handbook/handbook-usage/)! The handbook may seem overwhelming, but don't let it scare you. To simplify navigating the handbook, here are some suggested steps. Feel free to take a wrong turn at any time to learn more about whatever you are interested in.
   - [ ] Read [about](https://about.gitlab.com/about/) the company, and [how we use GitLab to build GitLab](https://about.gitlab.com/2015/07/07/how-we-use-gitlab-to-build-gitlab/). It is important to understand our [culture](https://about.gitlab.com/culture/), and how the organization was started. If you have any questions about company products you can always check out our [features](https://about.gitlab.com/features/#compare) and [products](https://about.gitlab.com/products/).
   - [ ] Make sure to read over our [values](https://about.gitlab.com/handbook/values) to answer any questions you might have about what to do and when. GitLab values should be a part of the everyday work process for all GitLab team members.
   - [ ] Particularly if you're new to working in a 100% remote environment, read over our [Guide for starting a new remote role](https://about.gitlab.com/company/culture/all-remote/getting-started/). You'll find other tips for operating (and thriving!) in an all-remote setting within the [All-Remote section of our handbook](https://about.gitlab.com/company/culture/all-remote/).
   - [ ] If you have questions about what is in the handbook check out the [handbook usage](https://about.gitlab.com/handbook/handbook-usage) as well as your own department's sub-page of the handbook (which can be found at the top of the handbook).
   - [ ] GitLab's Value Framework is foundational to GitLab's go-to-market messaging. We want to ensure all GitLab team members (even those outside of sales) are aware of what it is, why it is so important to our continued growth and success, and what it means to our customers, partners, and GitLab team members. Learn more in [this virtual session recording](http://bit.ly/37FqLC4) and the [Command of the Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) handbook page.
   - [ ] Since we are a global organization understand your benefits might be different than other team members at the company based on which [contract](https://about.gitlab.com/handbook/contracts/) you signed. It is important to understand **your** [benefits](https://about.gitlab.com/handbook/benefits/). If you have questions please reach out to People Ops.
   - [ ] [No ask time off policy](https://about.gitlab.com/handbook/paid-time-off): GitLab truly does value a work-life balance, and encourages team members to have a flexible schedule and take vacations. If you feel uncomfortable about taking time off, or are not sure how much time to take off throughout the year, feel free to speak with your manager or People Ops. We will be happy to reinforce this policy! Please note the additional steps that might need to be taken if you are scheduled for [on call](https://about.gitlab.com/handbook/on-call).
   - [ ] As part of onboarding, you received an email asking if you needed any equipment. Throughout your time at GitLab you might need additional equipment, would like to learn how to code, or want to visit a team member! Make sure you know what you can [spend company money](https://about.gitlab.com/handbook/spending-company-money) on.
   - [ ] The handbook also describes [incentives](https://about.gitlab.com/handbook/incentives) such as sales, dinners, or bonuses, that can apply to all GitLab team members. We'd love it if you [referred great talent](https://about.gitlab.com/handbook/incentives/#referral-bonuses)!
   - [ ] Even if you come from a technical background, it is important to understand the [proper workflow](https://about.gitlab.com/handbook/communication/#everything-starts-with-a-merge-request) since this is how we are all able to operate remotely in a successful way. If you come from a non-technical background, and would like more clarification on what this workflow means, feel free to ask People Ops, your manager, or anyone else at the company.
1. [ ] New team member: Take a look at GitLab's [Diversity & Inclusion page](https://about.gitlab.com/company/culture/inclusion/) and the [project](https://gitlab.com/gitlab-com/diversity-and-inclusion/issues). Optionally, you can contribute by collaborating or creating a new issue.
1. [ ] New team member: Take a look at GitLab's [public customer list](https://about.gitlab.com/customers/) to learn about our list of advertised customers.
1. [ ] New team member: Take a look at GitLab's [product tier use-cases](https://about.gitlab.com/handbook/ceo/pricing/per-tier-use-cases/) and [product marketing tiers](https://about.gitlab.com/handbook/marketing/product-marketing/tiers/) to learn about GitLab product capabilities, tiers, and the motiviation behind each tier.
1. [ ] New team member: While we [avoid acronyms](https://about.gitlab.com/handbook/product/#how-to-submit-a-new-issue) typically so that Everyone Can Contribute, there are times that the easiest way to achieve mutually exclusive, ubiquitous language (referred to as [MECEFU terms](https://about.gitlab.com/handbook/communication/#mecefu-terms)) is through acronyms. When that is the case, you should define the acronym in context first before it's use. For example, "All Three Letter Acryonyms (TLA) should be defined first to avoid confusion."
1. [ ] New team member: Read about GitLab [stock options](https://about.gitlab.com/handbook/stock-options/). If you received stock options as part of your contract, your options will be approved by the Board of Directors at their quarterly board meetings. After your grant has been approved by the Board you will receive a grant notice by email.
1. [ ] New team member: Bookmark [GitLab Handbook ChangeLog](https://about.gitlab.com/handbook/CHANGELOG.html) as our handbook is constantly growing and changing this is a useful link to stay up to date with all recent changes!
1. [ ] New team member: Set a calendar reminder to answer the GitLab quiz questions in the first 2 weeks of working in the company. If the answers are not accurate, you may retest: [GitLab Quiz](https://about.gitlab.com/handbook/questions/). Please schedule a call with your manager to discuss.
1. [ ] New team member: Explore how to best setup your inbox using [GitLab filters](https://www.youtube.com/watch?v=YOgm-vZVqng)
1. [ ] New team member: After you have been at GitLab for 30 days, kindly review [the NPS section](https://about.gitlab.com/handbook/people-group/#nps-surveys) in the handbook and complete this [survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform). Kindly do make time to complete it, it is vitally important for us to continute getting feedback and iterate on onboarding. 
1. [ ] New team member: Help spread the word about life at GitLab. Share your initial feedback by leaving a review on [Glassdoor](https://www.glassdoor.com/Overview/Working-at-GitLab-EI_IE1296544.11,17.htm) or [Comparably](https://www.comparably.com/companies/gitlab).  

</details>

---

<!-- include: division_tasks -->

---

</details>

#### Production And Database Engineering

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] New team member: Join the ['#eng-week-in-review' channel](https://app.slack.com/client/T02592416/CJWA4E9UG)

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Create a new [Production Engineering onboarding checklist](https://gitlab.com/gitlab-com/gl-infra/infrastructure/blob/master/.gitlab/issue_templates/onboarding_template.md).

</details>

#### Database Engineering

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Familiarize yourself with the [Database Team](https://about.gitlab.com/handbook/engineering/infrastructure/database/) handbook page.
1. [ ] New team member: Make sure you can log in to the staging and production databases using `sudo gitlab-psql gitlabhq_production`.
1. [ ] New team member: Join the `#database` channel on Slack.
1. [ ] New team member: Join the ['#eng-week-in-review' channel](https://app.slack.com/client/T02592416/CJWA4E9UG)

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager and new team member: Make sure all the (relevant) steps for Production Engineering are covered.

#### Frontend Engineers

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Familiarize yourself with the [frontend development guidelines](https://docs.gitlab.com/ce/development/fe_guide/).
1. [ ] New team member: Familiarize yourself with the [Vue documentation](https://vuejs.org/v2/guide/).
1. [ ] New team member: Familiarize yourself with [GitLab UI](https://gitlab-org.gitlab.io/gitlab-ui).
1. [ ] New team member: Familiarize yourself with [GitLab's Design System](http://design.gitlab.com).
1. [ ] Your specific Frontend Team Buddy is: XXX
1. [ ] New team member: Join the [#frontend](https://gitlab.slack.com/messages/frontend/) channel on Slack.
1. [ ] New team member: Join the ['#eng-week-in-review' channel](https://app.slack.com/client/T02592416/CJWA4E9UG)
1. [ ] New Team Member: Start off with these issues and check in with your buddy every second day on your progress, what you have done and your final MR.
1. [ ] New Team Member: Put on the agenda in your first week a small FE intro. What have you done before specifically? Favorite Frameworks? Your Code Editor? etc.
1. [ ] New Team Member: Check in with your respective manager on which areas you will start working so that you can take a look at the functionality and read the documentation about it.

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Grant new team member edit access to the frontend calendar
1. [ ] Manager: Add new team member to [@gl-frontend](https://gitlab.com/groups/gl-frontend/-/group_members) group

</details>

<details>
<summary>FE Buddy</summary>

1. [ ] FE Buddy: Schedule an introduction [coffee break call](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) in advance for the first 2 days.
1. [ ] FE Buddy: Do an intro session about the GDK and specifically about our FE Tools (for example best way to run Tests, etc.).
1. [ ] FE Buddy: Show one of your current deliverables and explain based on that: What you have actually done, How did you find the actual relevant files, reproducing steps, How our process works, How to submit an MR, When to involve reviewers etc.
1. [ ] FE Buddy: Search for 3 easy issues to start with (if none found, create one based on existing code) which is a simple refactoring, removal of ESLint exceptions, replace Modals or Icons.

</details>

#### Professional Services Engineer

<details>
<summary>New team member</summary>

1. [ ] New team member: Shadow/pair with a [Support team member](https://about.gitlab.com/team/) (appropriate to timezone) on 3-5 calls
1. [ ] New team member: Join the ['#eng-week-in-review' channel](https://app.slack.com/client/T02592416/CJWA4E9UG)

**Advanced Technical Topics**
1. [ ] New team member: Backup omnibus using our [Backup rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
1. [ ] New team member: Install GitLab via [Docker](https://docs.gitlab.com/ce/install/docker.html)
1. [ ] New team member: Restore backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#restore-a-previously-created-backup)
1. [ ] New team member: Starting a [rails console](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#invoking-rake-tasks)
1. [ ] New team member: Learn about environment Information and [maintenance checks](http://docs.gitlab.com/ce/raketasks/maintenance.html)
1. [ ] New team member: Learn about the [GitLab check](http://docs.gitlab.com/ce/raketasks/check.html) rake command
1. [ ] New team member: Learn about GitLab Omnibus commands (`gitlab-ctl`)
   1. [ ] [GitLab Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status)
   1. [ ] [Starting and stopping GitLab services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping)
1. [ ] New team member: Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)

**Custom Integrations**
**Goal:** Understand how developers and customers can produce custom integrations to GitLab. If you want to integrate with GitLab there are three possible paths you can take:
1. [ ] New team member: [Utilizing webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html) - If you want to reflect information from GitLab or initiate an action based on a specific activity that occurred on GitLab you can utilize the existing infrastructure of our webhooks. To read about setting up webhooks on GitLab visit this page.
1. [ ] New team member: [API integration](https://docs.gitlab.com/ee/api/README.html) - If you're seeking a richer integration with GitLab, the next best option is to utilize our ever-expanding API. The API contains pretty much anything you'll need, however, if there's an API call that is missing we'd be happy to explore it and develop it.
1. [ ] New team member: [Project Services](https://docs.gitlab.com/ce/user/project/integrations/project_services.html) - Project services give you the option to build your product into GitLab itself. This is the most advanced and complex method of integration but is by far the richest. It requires you to add your integration to GitLab's code base as a standard contribution. If you're thinking of creating a project service, the steps to follow are similar to any contribution to the GitLab codebase.

**Migrations**
**Goal:** Understand how migrations from other Version Control Systems to Git & GitLab work.

1. [ ] New team member: [SVN](https://git-scm.com/book/en/v1/Git-and-Other-Systems-Git-and-Subversion)
1. [ ] New team member: Learn how to [migrate from SVN to Git](https://docs.gitlab.com/ee/workflow/importing/migrating_from_svn.html)

**Learn Terraform**
1. [ ] New team member: Terraform single instance of GitLab into AWS (write your own Terraform).
   - Get AWS login from your manager
1. [ ] New team member: Terraform HA configuration into AWS (use existing Terraform).
1. [ ] New team member: Terraform HA+GEO configuration into AWS (use existing Terraform).
   - [ ] Setup cloudwatch dashboard for monitoring.
   - [ ] Create and run SAST project.
   - [ ] Setup code quality CI job (with code climate engine).
1. [ ] New team member: Terraform single instance into GCP (write your own Terraform).
   - [ ] Configure auto-devops to k8s cluster in GCP (write Terraform to create k8s cluster).
   - [ ] Create and run DAST project (use auto-devops and k8s cluster you created).
   - [ ] Setup DNS for k8s “production” cluster.
   - [ ] Setup auto-monitoring for app deployed to k8s cluster.
1. [ ] New team member: Helm deploy new cloud native containers into GCP.
1. [ ] New team member: Fix broken instance (mix of SG and misconfiguration scenarios).
   - [ ] Bad DB connection.
   - [ ] Bad Redis connection.
   - [ ] Bad NFS permissions.
   - [ ] Worker not connecting.
   - [ ] Bad certificate.
   - [ ] Can’t upload to registry.
   - [ ] Elasticsearch not indexing.
   - [ ] Repository is corrupt.

</details>
