### FOR MARKETING ONLY

* [ ] Marketing Ops: Remove from Tweetdeck.

<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions
      * [ ]   Salesforce - @Astahn (whoever is available): Immediately freeze the user if you cannot permanently deactivate the user. There may be a few reasons why you cannot deactivate the user (lead routing, etc).
      * [ ]   Salesforce - @Astahn: REASSIGN RECORDS in Salesforce: Export all leads, accounts, contacts, and open opportunities OWNED by the former team member and save as a flat file or googlesheets. Name the file `LASTNAME-FIRSTNAME-OBJECT-YYYY-MM-DD` (Object will be LEADS, ACCOUNTS, CONTACTS, or OPPORTUNITIES). Then reassign all leads, accounts, contacts, and open opportunities (do Not Reassign CLOSED WON/LOST OPPS!) to their Manager.
      * [ ]   Salesforce - @Astahn: REMOVE FROM PICKLISTS: remove from SDR or BDR picklists on ACCOUNT and OPPORTUNITY objects and if applicable, replace the former SDR/BDR to new SDR/BDR on both the account and opportunity objects. Please do not replace the SDR/BDR CLOSED WON/LOST OPPS and only replace opportunities with a new SDR/BDR if the opportunity is not yet qualified.
      * [ ]   Chorus - @Astahn: Disable user and deactivate license.
      * [ ]   Outreach - @jburton: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
      * [ ]   DiscoverOrg - @jburton: First, remove from DiscoverOrg Permission Set. Second, deactivate the user in DiscoverOrg.com.
      * [ ]   Marketo - @jburton: Remove from Workflow Campaigns, pick lists, and SFDC assignment sync.
      * [ ]   LeanData - @bethpeterson: Remove from any lead routing rules and round robin groups.
      * [ ]   LinkedIn Sales Navigator - @jburton: Disable user, remove from team license.
      * [ ]   Drift - @jburton: IF User: Disable access; IF Admin: Reset password.
      * [ ]   1Password - @Astahn: Rotate any shared login credentials (SFDC, Google Analytics, Adwords, Disqus, Facebook, LinkedIn, Zendesk).
