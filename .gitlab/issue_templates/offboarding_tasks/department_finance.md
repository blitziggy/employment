<summary>Finance</summary>

1. [ ] Finance: Remove from Comerica, Rabobank or any other account (as user or viewer only if in Finance).
1. [ ] Finance: Remove from Netsuite [QuickBooks users](https://about.gitlab.com/handbook/hiring/) (Finance only).

<summary>Manager</summary>

1. [ ] Manager: Remove from sales meeting.