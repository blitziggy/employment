#### Customer Success

<details>
<summary>Manager</summary>

1. [ ] Manager: Create a new issue in the [Customer Success Onboarding Issue Tracker](https://gitlab.com/gitlab-com/customer-success/team-onboarding/issues) using the `customer_success_onboarding.md` template. Notify the new member that this will be their Customer Success training/bootcamp.
1. [ ] Manager: Invite to sales meeting.
1. [ ] Manager: Schedule weekly 1:1 meeting.
1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs.

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New team member: In the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit) and [Competition](https://about.gitlab.com/comparison/).
1. [ ] New team member: Watch all the customer success courses listed in the [sales training handbook](https://about.gitlab.com/handbook/sales-training/#customer-success-cst-courses).
1. [ ] New team member: Review the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).

</details>


<details>
<summary>Sales Operation</summary>

1. [ ] Sales Operation (@Astahn): Add to Salesforce.
1. [ ] Sales Operation (@Astahn): When adding to Salesforce ensure that they are added to the appropriate group that either grants or restricts access to Pub Sec Data
1. [ ] Sales Operations (@Astahn): Add to Chorus as Listener or Recorder. If Recorder, must get manager approval.

</details>