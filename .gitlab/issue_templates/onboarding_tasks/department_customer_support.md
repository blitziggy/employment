#### For Support Only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Open a new [support onboarding boot camp issue](https://gitlab.com/gitlab-com/support/support-training/issues/new?issue) using the appropriate issue template for [Support Engineer](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Support%20Engineering%20Bootcamp.md) or [Support Agent](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Service%20Support%20Agent%20Bootcamp.md), and provide the link in a comment below this onboarding checklist.

</details>
