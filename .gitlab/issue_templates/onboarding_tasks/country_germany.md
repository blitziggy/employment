### For employees in Germany only

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `6-month Probationary Period until YYYY-MM-DD (6 months after hire date)` 
   * Effective Date: `6 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `6 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.
1. [ ] People Experience: Prepare the GitLab GmbH Employee Questionnaire, found in HelloSign as a template. Go to HelloSign - Templates - `GitLab GmbH Employee Questionnaire` - Use Template. Enter the requested information referencing their BambooHR profile. Stage for signature to the team member's personal email and to yourself as the second signatory for people ops . 
1. [ ] People Experience: Once the questionnaire has been completed and signed by the new team member, file the document in the new team member's BambooHR profile under Documents => Payroll Forms.
1. [ ] People Experience: Send a copy of the signed employment contract and completed employee questionnaire, both encrypted to RPI. You can find their contact information in the PeopleOps vault in 1Password in the Entity & Co-Employer HR Contacts note under Germany (RPI) name Ursula.
1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>
