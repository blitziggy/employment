### For team members in Canada only

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Please read the [Canada Corp Benefits](https://about.gitlab.com/handbook/benefits/canada-corp-benefits/) handbook page.

</details>

<details>
<summary>Compensation & Benefits</summary>

1. [ ] People Ops Analyst (@mwilkins): Add team member to benefits platform, [Collage](https://www.collage.co/).

</details>

<details>
<summary>Payroll</summary>

1. [ ] Payroll (@hdevlin): Add new team member to Canada payroll and send invitation to team member. 
1. [ ] Payroll (@hdevlin): Communicate with PeopleOps Analyst that team member has been successfully added.

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>
