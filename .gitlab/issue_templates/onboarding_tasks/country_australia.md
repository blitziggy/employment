### For employees in Australia only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please provide the following to payroll@gitlab.com: [Tax file number declaration](https://www.ato.gov.au/uploadedFiles/Content/IND/Downloads/TFN_declaration_form_N3092.pdf) and Superannuation information.
1. [ ] New team member: Add your bank details in BambooHR (this must be done by day 1 or latest day 2).

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `6-month Probationary Period until YYYY-MM-DD (6 months after hire date)` 
   * Effective Date: `6 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `6 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.

</details>
