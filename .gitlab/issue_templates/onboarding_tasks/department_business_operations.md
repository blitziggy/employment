#### Business Operations 

<details>
<summary>Manager</summary>

1. [ ] Invite to meetings: All Roles: Monthly Finance, Weekly Finance
    *  [ ]  BSA Team: Invite to BSA sync, Daily Iterative
    *  [ ]  BSS Team: Invite to BSA sync, Daily Iterative
    *  [ ]  Data Team: 
    *  [ ]  IT Team: Invite to IT Ops, IT Help
    *  [ ]  Procurement Team:

</details>

<details>
<summary>New Team Member</summary>

1. [ ] Join Slack channels: #business-operations, #bzo-team-lounge
    * [ ]  If you are a BSA: #bzo-bs, #finance and reach out in #bzo-bs to the other BSAs to ask which channels you should join based on your area of focus
    * [ ]  If you are on the Data Team:
    * [ ]  If you are on the IT Team: #it-ops, it_help, #okta
    * [ ]  If you are in Procurement: #procurement
    > If you are in IT, skip the next task since this will be opened for you.
1. Open an Access Request for access to Google Groups, Slack Groups and GitLab Groups:
    * [ ]  Open an [Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request) and ask to be added to:
       * [ ]  Google Groups: `Business Operations`, `BZO`, `bizops`
       * [ ]  Slack Groups: `Business Operations`, `BZO`, `bizops`
       * [ ]  GitLab Group: [business ops](https://gitlab.com/groups/gitlab-com/business-ops/-/group_members), developer role
       * [ ]  BSAs additionally request access to:
            * [ ] Google group: `BSAs`, member
            * [ ] Slack group: `BSA`, member
            * [ ] GitLab group: [BSA](https://gitlab.com/groups/gitlab-com/business-ops/bizops-bsa/-/group_members), developer role
       * [ ]  Procurement additionally request access to:
            * [ ] Google group: `Procurement`, member
            * [ ] Slack group: `Procurement`, member
            * [ ] GitLab group: [Procurement](https://gitlab.com/groups/gitlab-com/business-ops/procurement/-/group_members), developer role

</details>