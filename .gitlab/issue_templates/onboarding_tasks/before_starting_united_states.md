### Before Starting at GitLab: Employees in the US only

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Send the [I9 email](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/i9_email.md) to the new team members' personal email approximately 1 week before their start date.
1. [ ] People Experience: Enter new team member's designated Section 2 Agent into Fragomen and send invite.
1. [ ] People Experience: Run the completed I-9 through e-verify within the Fragomen platform. 
   1. [ ] People Experience: After the case has been submitted to e-verify, refer to the e-verify tab in Fragomen to see if the case has been resolved or needs further action. All cases will need a photo match. To do this click on the name of the individual, select complete photo match, and submit by following the instructions in the platform. If you have any questions refer to page 130 in the Fragomen user guide.  
1. [ ] People Experience: Once the I9 is complete and audited, and the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the I9 process is completed and that the employment is authorized. Comment on Day 1 at the earliest.

</details>
