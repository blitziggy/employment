### For employees in the UK only

<details>
<summary>People Experience</summary>

Before Start Date
1. [ ] People Experience: A few days before new team member's start date, send new team member the [UK email](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/uk_email.md) with New Starter Checklist link, information about P45 and notification that UK payroll form will be sent from HelloSign.
1. [ ] People Experience: At the same time you send the the email in step 1, using the HelloSign, send the new team member the UK payroll form. To do this go to Template >> Use template >> `GitLab Ltd Uk Payroll Form` On the drop-down menu on the right, choose `use template`. Fill in as much information as possible from BambooHR and stage in HelloSign for signature using their personal email.
1. [ ] People Experience: Once the payroll form has been completed and signed by the new team member file the document in BambooHR under Documents/Payroll Forms. 
1. [ ] People Experience: If a New Starter Checklist was completed, save it in the same folder. If new team member has a P45, please save it there as well.
1. [ ] People Experience: When the Payroll form and P45 or New Starter Checklist are complete, send as encrypted attachments to Vistra (Payroll Manager, Nicola) along with an encrypted copy of the team member's signed contract. The signed contract is found in the Contracts & Changes folder in the team member's BambooHR profile. This information should not be sent later than Day 1 of a new hire's start date. Do not cc the team as this are senstive information. Once acknowledged, delete your email.

After Start Date
1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has two entries marked as `Probation Period` with date as the hire date and `probation period ends` with the end date as exactly 3 months from the hire date.
1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>



<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Read through the [AXA PPP Brochure](https://drive.google.com/a/gitlab.com/file/d/0Bwy71gCp1WgtUXcxeFBaM0MyT00/view?usp=sharing) and let People Ops know if you would like to join the medical insurance scheme. You can find some more information on the [benefits section](https://about.gitlab.com/handbook/benefits/ltd-benefits-uk/#medical-insurance) also. This does not currently include dental or optical care. Please also note that this is a P11d taxable benefit.
1. [ ] New Team Member: Please read through the auto-enrolment personal pension details which you can find on the [pensions section](https://about.gitlab.com/handbook/benefits/ltd-benefits-uk/#pension-introduction) of the benefits page.

</details>

