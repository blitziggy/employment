#### For Recruiting Only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Read each of the subpages of the [hiring section in the handbook](https://about.gitlab.com/handbook/hiring/).
1. [ ] New team member: Read through the [recruiting onboarding guide](https://gitlab.com/gitlab-com/people-ops/recruiting/tree/master/recruiting-onboarding).
1. [ ] New team member: Turn on new issue notifications in the [Referrals Project](https://gitlab.com/gl-recruiting/referrals)
1. [ ] New team member: Create an account for [TimeZone.io](https://timezon.io) and add yourself to the People Group by clicking our [join link](https://timezone.io/join/c1661b415cadb3d6). Afterwards, please confirm that you added yourself to our [TimeZone team page] (https://timezone.io/team/peopleops). It will help you to have a quick overview of all the different timezones your People Group colleagues are located in.
1. [ ] New team member: (CES only) Turn on new issue notifications in the [CES Service Desk Project](https://gitlab.com/gl-recruiting/ces-service-desk)
1. [ ] New team member: (CES only) Turn on new issue notifications in the [Interview Reimbursement Project](https://gitlab.com/interview-reimbursement/ap-ces)
1. [ ] New team member: (Operations only) Turn on new issue notifications in the [Recruiting Operations Service Desk Project](https://gitlab.com/gl-recruiting/operations)
1. [ ] New team member: [Setup Greenhouse/Calendly Integration](https://support.greenhouse.io/hc/en-us/articles/360029359472-Enable-Calendly-Integration?mkt_tok=eyJpIjoiTTJSbFpHSXhNVFk0WVRrMSIsInQiOiJqaUZsdnNTblBLVVQ4WVdocWViTVBpU1hKOXVoK3pXZnpRQXBPZHpoSTQzaUpJUUdaeW1FTmRzcXp1OHN6eGp0XC9IRmJyNjJ1QjVsSmI2a28zbFBIdWloamI2dFdienZBNDNzK25acmR4eFU2SGlvcVllYUxvSHl2NEc3Ym9OK2UifQ%3D%3D)
1. [ ] New team member: [Setup Greenhouse/LinkedIn Integration] https://support.greenhouse.io/hc/en-us/articles/115005678103-Enable-LinkedIn-Recruiter-System-Connect-RSC-
1. [ ] New team member: (Sourcing only) Read more about how we do Sourcing through our handbook- https://about.gitlab.com/handbook/hiring/sourcing/ 
1. [ ] New team member: Change your LinkedIn account email to your GitLab email address.
1. [ ] New team member: Read through our the details of our [Diversity Sourcing iniative](https://gitlab.com/gitlab-com/people-ops/recruiting/issues/139). See what role we're current focused on in `related issues`.
1. [ ] New team member: (CES only) Edit the settings for the "interview calendar" in google calendar. Under "General notifications", make sure you receive email notifications for all events other than "new event" and "daily agenda".
1. [ ] New team member: (CES only) Add your recruiters and Manager to send emails on your behalf.  In GH - Click on Hi "Name" in the top right hand side - Account Settings - Email Permission - Allow only specific people to send emails from my address - and then add Candidate Experience Manager and your recruiters. Your recruiters should do the same for you.
1. [ ] New team member: Consider branding your email signature with [these best practices.](https://gitlab.com/gitlab-com/people-ops/recruiting/blob/master/recruiting-email-signatures.md) 

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Add team member to Greenhouse as `Job Admin: Recruiting`.
1. [ ] Manager: Create Baseline Entitlements Access Request [New Access Request ](https://gitlab.com/gitlab-com/access-requests/issues/new) using the Recruiting Role Based Entitlements template, manager approval is not needed for baseline entitlements when done as a part of on-boarding.
1. [ ] Manager: Add team member to the shared Google Calendar `Interview Calendar` and give read/write access.
1. [ ] Manager: Add team member to the Recruiting and People Ops team meetings as well as applicable team meetings and candidate sync meetings.
1. [ ] Manager: Give team member a Recruiter seat in our LinkedIn account.
1. [ ] Manager: (for CES and Operations only) Add team member to Sterling Talent Solutions account (global and US systems).

</details>
