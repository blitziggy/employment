#### For People Experience Only

<details>
<summary>Manager</summary>

1. [ ] Manager: Add team member to the following private Slack channels (as applicable):
   * `#people-group-confidential` 
   * `#people-ops` 
   * `#people-exp`
   * `#people-exp_ces` 
   * `#peopleops_totalrewards`
   * `#international-exp`
   * `#onboarding-survey`
   * `#offboardings`
1. [ ] Manager: Add team member to the following public Slack channels (as applicable):
   * `#peopleops`
   * `#country-hiring-guidelines`
   * `#group-conversations`
   * `#peopleops-alerts`
1. [ ] Manager: Add team member to the following Slack groups (as applicable):
   * `peopleops_spec`
   * `people_exp`

</details>




<details>
<summary>People Ops</summary>

1. [ ] People Ops (@ewegscheider): Add team member to Greenhouse as "Job Admin: People Ops".
1. [ ] People Ops: Ping `@tknudsen` or `@shaynes13` in this issue to add team member to the calendar invite for the PeopleOps team meetup at Contribute March 2020.

</details>

<details>
<summary>New Team Member</summary>

1. You will be added to the email alias for your team. This can result in a high volume inbox, consider reviewing how your fellow team members are responding to the queries we as a team receive. No need to respond to any of these queries during week 1 or 2.
1. [ ] New Team Member: Add yourself to the Timezone.io tool via [this link](https://timezone.io/join/c1661b415cadb3d6) to keep track of where the People Group members are located.

</details>
